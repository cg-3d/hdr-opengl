################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/imgui/imgui.cpp \
../src/imgui/imgui_demo.cpp \
../src/imgui/imgui_draw.cpp \
../src/imgui/imgui_impl_glfw.cpp \
../src/imgui/imgui_impl_opengl3.cpp \
../src/imgui/imgui_widgets.cpp \
../src/imgui/toggle_button.cpp 

OBJS += \
./src/imgui/imgui.o \
./src/imgui/imgui_demo.o \
./src/imgui/imgui_draw.o \
./src/imgui/imgui_impl_glfw.o \
./src/imgui/imgui_impl_opengl3.o \
./src/imgui/imgui_widgets.o \
./src/imgui/toggle_button.o 

CPP_DEPS += \
./src/imgui/imgui.d \
./src/imgui/imgui_demo.d \
./src/imgui/imgui_draw.d \
./src/imgui/imgui_impl_glfw.d \
./src/imgui/imgui_impl_opengl3.d \
./src/imgui/imgui_widgets.d \
./src/imgui/toggle_button.d 


# Each subdirectory must supply rules for building sources it contributes
src/imgui/%.o: ../src/imgui/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O3 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


