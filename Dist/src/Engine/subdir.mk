################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Engine/Camera.cpp \
../src/Engine/FramebuffersTracker.cpp \
../src/Engine/Image.cpp \
../src/Engine/Material.cpp \
../src/Engine/Mesh.cpp \
../src/Engine/Model.cpp \
../src/Engine/Scene.cpp \
../src/Engine/Shader.cpp \
../src/Engine/cubemaps.cpp \
../src/Engine/engine-utils.cpp \
../src/Engine/input-callbacks.cpp \
../src/Engine/linear-algebra.cpp 

OBJS += \
./src/Engine/Camera.o \
./src/Engine/FramebuffersTracker.o \
./src/Engine/Image.o \
./src/Engine/Material.o \
./src/Engine/Mesh.o \
./src/Engine/Model.o \
./src/Engine/Scene.o \
./src/Engine/Shader.o \
./src/Engine/cubemaps.o \
./src/Engine/engine-utils.o \
./src/Engine/input-callbacks.o \
./src/Engine/linear-algebra.o 

CPP_DEPS += \
./src/Engine/Camera.d \
./src/Engine/FramebuffersTracker.d \
./src/Engine/Image.d \
./src/Engine/Material.d \
./src/Engine/Mesh.d \
./src/Engine/Model.d \
./src/Engine/Scene.d \
./src/Engine/Shader.d \
./src/Engine/cubemaps.d \
./src/Engine/engine-utils.d \
./src/Engine/input-callbacks.d \
./src/Engine/linear-algebra.d 


# Each subdirectory must supply rules for building sources it contributes
src/Engine/%.o: ../src/Engine/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O3 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


