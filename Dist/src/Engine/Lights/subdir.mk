################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Engine/Lights/DirectionalLight.cpp \
../src/Engine/Lights/Light.cpp \
../src/Engine/Lights/PointLight.cpp \
../src/Engine/Lights/SpotLight.cpp 

OBJS += \
./src/Engine/Lights/DirectionalLight.o \
./src/Engine/Lights/Light.o \
./src/Engine/Lights/PointLight.o \
./src/Engine/Lights/SpotLight.o 

CPP_DEPS += \
./src/Engine/Lights/DirectionalLight.d \
./src/Engine/Lights/Light.d \
./src/Engine/Lights/PointLight.d \
./src/Engine/Lights/SpotLight.d 


# Each subdirectory must supply rules for building sources it contributes
src/Engine/Lights/%.o: ../src/Engine/Lights/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -std=c++0x -O3 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


