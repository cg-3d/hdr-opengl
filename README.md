# HDR-OpenGL

An HDR rendering demo in C++ using OpenGL.  
This is a university assignment project.

This project uses:
 * OpenGL 3+
 * GLSL 3.30
 * GLFW3: [https://github.com/glfw/glfw](https://github.com/glfw/glfw)
 * Assimp: [https://github.com/assimp/assimp](https://github.com/assimp/assimp)
 * ImGui: [https://github.com/ocornut/imgui](https://github.com/ocornut/imgui)
 * Stb image: [https://github.com/nothings/stb]([https://github.com/glfw/glfw](https://github.com/glfw/glfw))


 The `AppImage` folder contains a runnable AppImage built on ubuntu 18.04. If the app image cannot be run on your system you need to build the project on your own (thus installing all the required dependencies), then an AppImage
 can be created with the content of the `build` directory.