#!/bin/bash
cd "$(dirname ${BASH_SOURCE[0]})"

# Clear AppDir/usr/bin
rm -r AppDir/usr/bin/*

# Copy resources to AppDir/usr/bin
cp ../Dist/engine_0.8-Test-OpenGL AppDir/usr/bin
mkdir AppDir/usr/bin/src
cp -r ../src/shaders AppDir/usr/bin/src
mkdir AppDir/usr/bin/src/Scenes
mkdir AppDir/usr/bin/src/Scenes/BPLightsScene
mkdir AppDir/usr/bin/src/Scenes/HDRScene
cp -r ../src/Scenes/BPLightsScene/shaders AppDir/usr/bin/src/Scenes/BPLightsScene
cp -r ../src/Scenes/HDRScene/shaders AppDir/usr/bin/src/Scenes/HDRScene
cp -r ../models AppDir/usr/bin
cp -r ../textures AppDir/usr/bin
cp -r ../skybox AppDir/usr/bin


