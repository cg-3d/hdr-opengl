#ifndef ENGINE_LINEAR_ALGEBRA_H_
#define ENGINE_LINEAR_ALGEBRA_H_

#include <glm/glm.hpp>

namespace engLinAlg{

glm::mat2x3 calculateTangentAndBitangent( glm::vec3 p1 , glm::vec3 p2 , glm::vec3 p3 ,
										  glm::vec2 uv1 , glm::vec2 uv2 , glm::vec2 uv3 );

}


#endif /* ENGINE_LINEAR_ALGEBRA_H_ */
