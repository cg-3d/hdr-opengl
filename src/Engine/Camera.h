#ifndef CAMERA_H_
#define CAMERA_H_

#include <glm/glm.hpp>

class Camera {

public:
	Camera( const glm::vec3 position , const glm::vec3 target , const glm::vec3 up );
	Camera( const glm::vec3 target );
	Camera();

	glm::mat4 getViewMatrix();

	const glm::vec3& getPosition() const;
	const glm::vec3& getTarget() const;
	const glm::vec3& getUp() const;
	const glm::vec3& getFront() const;
	const glm::vec3& getWorldUp() const;
	bool isTargetLockedOnMove() const;
	const float getYaw() const;
	const float getPitch() const;
	float getCameraOrbitRadius() const;

	void setTargetLockedOnMove( bool locked );

	void moveTo( const glm::vec3 position );
	void moveBy( const glm::vec3 movement );
	void orbitBy( const float radians );
	void moveLook( const float pitch , const float yaw );

	void setCameraOrbitRadius( float cameraOrbitRadius );

	void printStatus();

private:
	glm::vec3 position;
	glm::vec3 target;
	glm::vec3 up;
	glm::vec3 front;
	glm::vec3 worldUp;

	float pitch;
	float yaw;

	bool lockTargetOnMove;
	float cameraOrbitRadius;
};


#endif /* CAMERA_H_ */
