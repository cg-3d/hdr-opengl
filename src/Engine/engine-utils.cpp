#include <string>
#include <iostream>
#include <glad/glad.h>

#include "../stb_image/stb_image.h"

#include "engine-utils.h"



unsigned int engUtils::textureFromFile(const char *path, bool gamma){
	std::string filename = std::string(path);

    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    //stbi_set_flip_vertically_on_load( true );
    unsigned char *data = stbi_load(filename.c_str(), &width, &height, &nrComponents, 0);
    if (data)
    {

    	std::cout << "NrComponents: " << nrComponents << std::endl;

        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        // Use SRGB space if gamma is true
        GLenum internalFormat;
        if( gamma ){
        	if( format == GL_RGB )
        		internalFormat = GL_SRGB;
        	if( format == GL_RGBA )
        		internalFormat = GL_SRGB_ALPHA;
        }else{
        	internalFormat = format;
        }

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, internalFormat , width, height, 0, format , GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}

/**
 * Creates a framebuffer with a texture color buffer attached
 * as GL_COLOR_ATTACHMENT0.
 * The (texture) color buffer is built using the specified
 * 		internal format : (GL_RGB , GL_RGBA16F , ... )
 * 		format : (GL_RGB , GL_RGBA , ... )
 * 	 	data format : (GL_UNSIGNED_BYTE , GL_FLOAT , ... )
 */
void engUtils::createFrameTextureBuffer( unsigned int *fbo , unsigned int *texcb ,
							   	   	     int width , int height ,
										 GLint colorInternalFormat , GLenum colorFormat , GLenum colorDataFormat ){

	// Framebuffer
	glGenFramebuffers( 1 , fbo );
	glBindFramebuffer( GL_FRAMEBUFFER , *fbo );

	// Texture Color buffer
	glGenTextures( 1 , texcb );
	glBindTexture( GL_TEXTURE_2D , *texcb );
	glTexImage2D( GL_TEXTURE_2D , 0 , colorInternalFormat , width , height , 0 , colorFormat , colorDataFormat , NULL );

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture( GL_TEXTURE_2D , *texcb );
	glFramebufferTexture2D( GL_FRAMEBUFFER , GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D , *texcb , 0 );

	auto fboStatus = glCheckFramebufferStatus( GL_FRAMEBUFFER );
	if( fboStatus != GL_FRAMEBUFFER_COMPLETE )
			std::cout << "ERROR::FRAMEBUFFER::FT Framebuffer is not complete: "
					  << fboStatus << std::endl;

	glBindFramebuffer( GL_FRAMEBUFFER , 0 );
	glBindTexture( GL_TEXTURE_2D , 0 );
	glBindRenderbuffer( GL_RENDERBUFFER , 0 );
}

void engUtils::createFrameTextureRenderBuffer( unsigned int *fbo , unsigned int *texcb , unsigned int *rb ,
											   int width , int height  ,
											   GLint colorInternalFormat ,
											   GLenum colorFormat ,
											   GLenum colorDataFormat ,
											   GLenum renderFormat ,
											   GLenum renderAttachment
										     )
{

	//Framebuffer and texture color buffer
	engUtils::createFrameTextureBuffer( fbo , texcb ,
							  	  	  	width , height ,
										colorInternalFormat ,  colorFormat , colorDataFormat );

	glBindFramebuffer( GL_FRAMEBUFFER , *fbo ); // Rebind the fbo because all utilities restore to default

	// Render buffer
	glGenRenderbuffers( 1 , rb );
	glBindRenderbuffer( GL_RENDERBUFFER , *rb );
	glRenderbufferStorage( GL_RENDERBUFFER , renderFormat , width , height );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER , renderAttachment , GL_RENDERBUFFER , *rb );

	auto fboStatus = glCheckFramebufferStatus( GL_FRAMEBUFFER );
		if( fboStatus != GL_FRAMEBUFFER_COMPLETE )
				std::cout << "ERROR::FRAMEBUFFER::FTR Framebuffer is not complete: "
						  << fboStatus << std::endl;

	glBindFramebuffer( GL_FRAMEBUFFER , 0 );
	glBindTexture( GL_TEXTURE_2D , 0 );
	glBindRenderbuffer( GL_RENDERBUFFER , 0 );

}

