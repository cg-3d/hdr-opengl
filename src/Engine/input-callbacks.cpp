#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stddef.h>
#include <iostream>

#include <string>
#include <map>
#include <iterator>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include "Shader.h"
#include "Camera.h"

#include "engine-utils.h"
#include "../engine-modules.h"

#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_glfw.h"
#include "../imgui/imgui_impl_opengl3.h"

#include "../utilities/utilities.h"

#include "input-callbacks.h"

namespace incalls{

// Screen : track window size
int screenWidth = 800 , screenHeight = 600;

// Scene view : track scene view size
int sceneViewWidth = (int)(screenWidth * 3 / 4.0f);
int sceneViewHeight = screenHeight;

// Track if the scene window is active or not
bool sceneViewActive = false;

// Delta time correction
float deltaTime = 0.0f;
float lastFrame = 0.0f;

//Camera
Camera *camera = NULL;
float cameraSpeed = 0.2f;
float orbitSpeed = 1.0f; //degrees

//Framebuffer
//unsigned int *framebuffer = NULL;
//unsigned int *textureColorbuffer = NULL;
//unsigned int *renderbuffer = NULL;

//Projection matrix
glm::mat4 *projection = NULL;
//Projection matrix shaders uniform to update
std::map<Shader *, std::string> projectionUniforms = std::map<Shader *, std::string>();

MouseStatus mouse = {
	0.05f ,
	400 ,
	300 ,
	true
};

// Framebuffers tracker in anonymous namespace = private
namespace {
	FramebuffersTracker tracker = FramebuffersTracker();
}

/**
 * Gives access to the tracker by const reference.
 */
const FramebuffersTracker &fbTracker(){
	return tracker;
}

//FramebuffersTracker tracker = FramebuffersTracker();

/**
 * Binds a camera as the camera managed by the
 * input callbacks.
 */
void bindCamera( Camera *cam ){
	camera = cam;
}

/**
 * Set the projection matrix to update when resizing
 */
void bindProjectionMatrix( glm::mat4 *pMatrix ){
	projection = pMatrix;
}

void bindProjectionUniform( Shader *shader , std::string name ){
	projectionUniforms.insert( std::pair<Shader *, std::string>( shader , name ) );
}

void unbindProjectionUniform( Shader *shader ){
	projectionUniforms.erase( shader );
}

/**
 * Set the screen width and height,
 * and also set the sceneView size accordingly;
 */
void setScreenSize( int width , int height ){
	screenWidth = width;
	screenHeight = height;
	sceneViewWidth = (int)( width * 3 / 4.0f );
	sceneViewHeight = height;
}

/**
 * Sets the camera speed.
 */
void setCameraSpeed( float speed ){
	cameraSpeed = speed;
}

/**
 * Returns cameraSpeed with delta time correction.
 */
float getCameraSpeed(){
	return cameraSpeed * deltaTime;
}

/**
 * Sets the camera orbit speed.
 */
void setCameraOrbitSpeed( float speed ){
	orbitSpeed = speed;
}

/**
 * Return orbitSpeed with delta time correction.
 */
float getCameraOrbitSpeed(){
	return orbitSpeed * deltaTime;
}

/**
 * Updates the current delta time stored.
 */
void updateDeltaTime(){
	float currentFrame = glfwGetTime();
	deltaTime = currentFrame - lastFrame;
	lastFrame = currentFrame;
}

/**
 * The callback to call on window resizing
 */
void framebuffer_size_callback( GLFWwindow* window , int width , int height ){


	setScreenSize( width , height );

//	std::cout << "BEFORE REGEN: " << std::endl;
//	tracker.dumpFramebuffers();

	tracker.resizeFramebuffers( incalls::sceneViewWidth , incalls::sceneViewHeight );

//	std::cout << "AFTER REGEN: " << std::endl;
//	tracker.dumpFramebuffers();

	//Change OpenGL viewport accordingly
	glViewport( 0 , 0 , sceneViewWidth , sceneViewHeight );

	// Update the projection matrix
	*projection = glm::perspective( glm::radians( 45.0f ) ,
								    sceneViewWidth / (float) sceneViewHeight ,
									1.0f , 500.0f );

	//Update the shaders projection matrices
	std::map<Shader *, std::string >::iterator it;
	for( it = projectionUniforms.begin(); it != projectionUniforms.end(); ++it ){
		it->first->setMatrix4fv( it->second.c_str() , *projection );
	}

}

void key_callback( GLFWwindow* window , int key , int scancode , int action , int mods ){

	// Bind ESC to window close
//	if( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS ){
//		std::cout << "ESC Pressed. Closing" << std::endl;
//		glfwSetWindowShouldClose( window , true );
//	}

	// L toggles camera lock
	if( key == GLFW_KEY_L && action == GLFW_PRESS ){
		if( ImGui::IsWindowFocused() ){
			camera->setTargetLockedOnMove( !camera->isTargetLockedOnMove() );
			std::cout << "Toggled camera lock: " << ( camera->isTargetLockedOnMove() ? "true" : "false" ) << std::endl;
		}
	}

	// The ESC key allows to exit from the scene view
	if( key == GLFW_KEY_ESCAPE && action == GLFW_PRESS ){
		if( ImGui::IsWindowFocused() ){
			glfwSetCursorPosCallback( window , NULL ); // Unbind the
			ImGui::SetWindowFocus( NULL ); // Unfocus the scene view window
			glfwSetInputMode( window , GLFW_CURSOR , GLFW_CURSOR_NORMAL ); // Enable the cursor
			sceneViewActive = false;
		}
	}
}

void mouse_callback( GLFWwindow* window , double xpos , double ypos ){

	if( mouse.firstMouse ){
		mouse.lastX = xpos;
		mouse.lastY = ypos;
		mouse.firstMouse = false;
	}

	float xOffset = xpos - mouse.lastX;
	float yOffset = ypos - mouse.lastY;

	mouse.lastX = xpos;
	mouse.lastY = ypos;

	xOffset *= mouse.sensitivity;
	yOffset *= mouse.sensitivity;

	camera->moveLook( xOffset , yOffset );
	//camera->printStatus();
}

void mouse_button_callback( GLFWwindow *window , int button , int action , int mods ){

	// Enter the scene view when clicked
	if( button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE ){
		if( ImGui::IsWindowFocused() && !sceneViewActive ){ // If the scene window has been focused and the scene view is not active, activate the scene view
			glfwSetCursorPosCallback( window , incalls::mouse_callback ); // Bind the mouse callback
			glfwSetInputMode( window , GLFW_CURSOR , GLFW_CURSOR_DISABLED ); // Disable the cursor
			sceneViewActive = true;
			incalls::mouse.firstMouse = true;
		}else{
			// If the scene window has been unfocused but the scene view is still active refocus the scene window
			if( !ImGui::IsWindowFocused() && sceneViewActive ){
				ImGui::SetWindowFocus();
			}
		}
	}

}

}
