#ifndef IMAGE_H_
#define IMAGE_H_

#include <string>

/**
 * Image Utility class
 */
class Image{

public:
	int width;
	int height;
	int nrChannels;
	unsigned char* data;

	Image( std::string path );
	~Image();

	bool loaded();
	void freeImageData();

};

#endif /* IMAGE_H_ */
