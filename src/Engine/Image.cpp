#include <iostream>
#include <fstream>
#include <sstream>

#include "../stb_image/stb_image.h"

#include "Image.h"

Image::Image( std::string path ){
	//stbi_set_flip_vertically_on_load(true);
	this->data = stbi_load( path.c_str() , &this->width , &this->height , &this->nrChannels , 0 );
}

Image::~Image( void ){
	this->freeImageData();
}

bool Image::loaded(){
	if( this->data )
		return true ;
	return false;
}

void Image::freeImageData(){
	if( this->data && this->data != NULL ){
		stbi_image_free( this->data );
		this->data = NULL;
	}
}
