
#include <vector>
#include <map>
#include <algorithm>

#include <iterator>
#include <iostream>

#include <glad/glad.h>

#include "../utilities/utilities.h"

#include "FramebuffersTracker.h"

FramebuffersTracker::FramebuffersTracker(){

	 framebuffers = std::map< unsigned int * ,
			  	  	  	  	  std::pair<
							  	  std::vector<Texture2DColorbuffer> ,
								  std::vector<Renderbuffer>
			  	  	  	  	  >
	 	 	 	 	 >();

	 drawbuffers = std::map< unsigned int * , std::vector<GLenum>>();
}

FramebuffersTracker::~FramebuffersTracker(){
}

void FramebuffersTracker::bindFramebufferTex2DColorbuffer( unsigned int *fbo ,
														   FramebuffersTracker::Texture2DColorbuffer texcb )
const
{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>
	   ::iterator fbIt  = framebuffers.find( fbo );

	if( fbIt == framebuffers.end() ){ // The FB is not already being tracked
		framebuffers.insert(
			std::pair< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>(
			   fbo , // Framebuffer
			   std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>(
				   { texcb } , // Colorbuffers
				   {} 		   // No render buffers
			   )
			)
		);
	} else {

		std::vector<Texture2DColorbuffer>::iterator cbIt;
		for( cbIt = fbIt->second.first.begin() ; cbIt != fbIt->second.first.end() ; ++cbIt ){
			if( cbIt->cbo == texcb.cbo ){
				return; // The colorbuffer is already being tracked, do nothing
			}
		}

		// Track the colorbuffer
		fbIt->second.first.push_back( texcb );
	}
}

void FramebuffersTracker::bindFramebufferRenderbuffer( unsigned int *fbo ,
														   FramebuffersTracker::Renderbuffer rb )
const
{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>
	   ::iterator fbIt  = framebuffers.find( fbo );

	if( fbIt == framebuffers.end() ){ // The FB is not already being tracked
		framebuffers.insert(
			std::pair< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>(
			   fbo , // Framebuffer
			   std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>(
				   {} , 	// No colorbuffer
				   { rb } 	// Renderbuffer
			   )
			)
		);
	} else {

		std::vector<Renderbuffer>::iterator rbIt;
		for( rbIt = fbIt->second.second.begin() ; rbIt != fbIt->second.second.end() ; ++rbIt ){
			if( rbIt->rbo == rb.rbo ){
				return; // The renderbuffer is already being tracked, do nothing
			}
		}

		// Track the renderbuffer
		fbIt->second.second.push_back( rb );
	}
}

void FramebuffersTracker::bindFramebufferTex2DColorbufferRenderbuffer( unsigned int *fbo ,
																	   FramebuffersTracker::Texture2DColorbuffer texcb ,
																	   FramebuffersTracker::Renderbuffer rb )
const
{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>
	   ::iterator fbIt  = framebuffers.find( fbo );

	if( fbIt == framebuffers.end() ){ // The FB is not already being tracked
		framebuffers.insert(
			std::pair< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>(
			   fbo , // Framebuffer
			   std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>(
				   { texcb } , 	// Colorbuffer
				   { rb } 		// Renderbuffer
			   )
			)
		);
	} else {

		// Texture color buffer check
		std::vector<Texture2DColorbuffer>::iterator cbIt;
		for( cbIt = fbIt->second.first.begin() ; cbIt != fbIt->second.first.end() ; ++cbIt ){
			if( cbIt->cbo == texcb.cbo ){
				break;
			}
		}

		if( cbIt == fbIt->second.first.end() ){ // The colorbuffer is not being tracked add it
			fbIt->second.first.push_back( texcb );
		}

		// Renderbuffer check
		std::vector<Renderbuffer>::iterator rbIt;
		for( rbIt = fbIt->second.second.begin() ; rbIt != fbIt->second.second.end() ; ++rbIt ){
			if( rbIt->rbo == rb.rbo ){
				break;
			}
		}

		if( rbIt == fbIt->second.second.end() ){ // The renderbuffer is not being tracked add it
			fbIt->second.second.push_back( rb );
		}
	}
}

/**
 * Attach a GL_TEXTURE_2D colorbuffer to this tracker.
 */
void FramebuffersTracker::attachFramebufferTex2DColorbuffer( unsigned int *fbo ,
														     unsigned int *cb ,
															 GLenum attachment ,
															 GLint internalFormat ,
															 GLenum format ,
															 GLenum dataFormat ,
															 GLint level ,
															 GLenum magFilter ,
															 GLenum minFilter
									  	  	  	  	  	   )
const
{

	FramebuffersTracker::Texture2DColorbuffer texcb = {
		cb ,
		attachment ,
		internalFormat ,
		format ,
		dataFormat ,
		level ,
		magFilter ,
		minFilter
	};


	bindFramebufferTex2DColorbuffer( fbo , texcb );
}

/**
 * Attach a renderbuffer to this tracker.
 */
void FramebuffersTracker::attachFramebufferRenderbuffer( unsigned int *fbo ,
									    				 unsigned int *rbo ,
														 GLenum attachment ,
														 GLenum format
									  	               )
const
{

	FramebuffersTracker::Renderbuffer rb = { rbo , attachment , format };
	bindFramebufferRenderbuffer( fbo , rb );
}

/**
	 * Attach a GL_TEXTURE_2D colorbuffer and a
	 * renderbuffer to this tracker.
	 */
void FramebuffersTracker::attachFramebufferTex2DColorbufferRenderbuffer( unsigned int *fbo ,
																		 unsigned int *cb ,
																		 GLenum cbAttachment ,
																		 GLint cbInternalFormat ,
																		 GLenum cbFormat ,
																		 GLenum cbDataFormat ,
																		 GLint cbLevel ,
																		 GLenum texMagFilter ,
																		 GLenum texMinFilter ,
																		 unsigned int *rbo ,
																		 GLenum rbAttachment ,
																		 GLenum rbFormat
												  	  	  	  	  	   )
const
{

	FramebuffersTracker::Texture2DColorbuffer texcb = {
		cb ,
		cbAttachment ,
		cbInternalFormat ,
		cbFormat ,
		cbDataFormat ,
		cbLevel ,
		texMagFilter ,
		texMinFilter
	};

	FramebuffersTracker::Renderbuffer rb = { rbo , rbAttachment , rbFormat };

	bindFramebufferTex2DColorbufferRenderbuffer( fbo , texcb , rb );
}

void FramebuffersTracker::detachFramebuffer( unsigned int *fbo ) const {
	framebuffers.erase( fbo );
	drawbuffers.erase( fbo );
}

void FramebuffersTracker::detachFramebufferColorbuffer( unsigned int* fbo , unsigned int *cbo )
const
{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer> > >
	   ::iterator fbIt = framebuffers.find( fbo );

	std::map< unsigned int * , std::vector<GLenum>>
	   ::iterator dbIt = drawbuffers.find( fbo );

	if( fbIt != framebuffers.end() ){
		std::vector<Texture2DColorbuffer>::iterator cbIt;
		for( cbIt = fbIt->second.first.begin(); cbIt != fbIt->second.first.end() ; ++cbIt ){
			if( cbIt->cbo == cbo ){
				if( dbIt != drawbuffers.end() ){ // remove attachment from drawbuffer call
					dbIt->second.erase(
						std::remove( dbIt->second.begin() , dbIt->second.end() ,
									 cbIt->attachment )
					);

					// remove from drawbuffers if the attachment vector is empty
					if( dbIt->second.empty() ){
						drawbuffers.erase( fbo );
					}
				}

				fbIt->second.first.erase( cbIt );
				break;
			}
		}
	}
}

void FramebuffersTracker::detachFramebufferRenderbuffer( unsigned int* fbo , unsigned int *rbo )
const
{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer> > >
	   ::iterator fbIt = framebuffers.find( fbo );

	if( fbIt != framebuffers.end() ){
		std::vector<Renderbuffer>::iterator rbIt;
		for( rbIt = fbIt->second.second.begin(); rbIt != fbIt->second.second.end() ; ++rbIt ){
			if( rbIt->rbo == rbo ){
				fbIt->second.second.erase( rbIt );
				break;
			}
		}
	}
}

void FramebuffersTracker::addDrawBuffer( unsigned int *fbo , GLenum attachment ) const{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer> > >
	   ::iterator fbIt = framebuffers.find( fbo );

	// Fraembuffer is not tracked
	if( fbIt == framebuffers.end() ){
		std::cout << "ERROR::FRAMEBUFFERTRACKER:: addDrawBuffer() on a non-tracked framebuffer. Skipped ..." << std::endl;
		return;
	}

	std::vector<Texture2DColorbuffer>
	   ::iterator cbIt = fbIt->second.first.begin();

	std::map< unsigned int *, std::vector<GLenum>>
	   ::iterator dbIt = drawbuffers.find( fbo );

	while( cbIt != fbIt->second.first.end() ){
		if( cbIt->attachment == attachment ){
			if( dbIt != drawbuffers.end() )
				dbIt->second.push_back( attachment );
			else
				drawbuffers.insert( std::pair< unsigned int * , std::vector<GLenum>>(
						fbo , { attachment }
				) );

			return;
		}
		++cbIt;
	}

	std::cout << "WARNING:FRAMEBUFFERTRACKER:: addDrawBuffer() adding non-tracked attachment. Skipped ... " << std::endl;
}

void FramebuffersTracker::resizeFramebuffer( unsigned int *fbo ,
											 std::vector<Texture2DColorbuffer> texcbs ,
											 std::vector<Renderbuffer> rbos ,
											 int width ,
											 int height )
const
{

	// Regen framebuffer
	glDeleteFramebuffers( 1 , fbo );
	glGenFramebuffers( 1 , fbo );
	glBindFramebuffer( GL_FRAMEBUFFER , *fbo );

	// Regen textures
	for( unsigned int i=0 ; i < texcbs.size() ; ++i ){
		glDeleteTextures( 1 , texcbs[i].cbo );
		//glActiveTexture( GL_TEXTURE0 );
		glGenTextures( 1 , texcbs[i].cbo );
		glBindTexture( GL_TEXTURE_2D , *(texcbs[i].cbo) );
		glTexImage2D( GL_TEXTURE_2D ,
					  texcbs[i].level ,
					  texcbs[i].internalFormat ,
					  width , height ,
					  0 ,
					  texcbs[i].format ,
					  texcbs[i].dataFormat ,
					  NULL );
		glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , texcbs[i].magFilter );
		glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , texcbs[i].minFilter );

		glFramebufferTexture2D( GL_FRAMEBUFFER ,
				                texcbs[i].attachment ,
								GL_TEXTURE_2D ,
								*(texcbs[i].cbo) ,
								texcbs[i].level );
	}

	// Call glDrawbuffers if needed
	std::map<unsigned int *, std::vector<GLenum>>
	   ::iterator dbIt = drawbuffers.find( fbo );
	if( dbIt != drawbuffers.end() ){
		glDrawBuffers( dbIt->second.size() , &dbIt->second[0] );
	}

	// Regen renderbuffers
	for( unsigned int i=0 ; i < rbos.size() ; ++i ){
		glDeleteRenderbuffers( 1 , rbos[i].rbo );
		glGenRenderbuffers( 1 , rbos[i].rbo );
		glBindRenderbuffer( GL_RENDERBUFFER , *(rbos[i].rbo) );
		glRenderbufferStorage( GL_RENDERBUFFER ,
							   rbos[i].format ,
							   width , height );
		glFramebufferRenderbuffer( GL_FRAMEBUFFER ,
								   rbos[i].attachment ,
								   GL_RENDERBUFFER ,
								   *(rbos[i].rbo) );
	}

	auto fboStatus = glCheckFramebufferStatus( GL_FRAMEBUFFER );
	if( fboStatus != GL_FRAMEBUFFER_COMPLETE )
			std::cout << "ERROR::FRAMEBUFFER Framebuffer is not complete: "
					  << fboStatus << std::endl;

	glBindFramebuffer( GL_FRAMEBUFFER , 0 );

}

void FramebuffersTracker::resizeFramebuffer( unsigned int *fbo , int width , int height )
const
{

	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>
	   ::iterator fbIt = framebuffers.find( fbo );

	if( fbIt != framebuffers.end() ){

		resizeFramebuffer( fbIt->first ,				// FBO
						   fbIt->second.first , 		// Texture colorbuffers array
						   fbIt->second.second ,		// Renderbuffers array
						   width , height );
	}
}

void FramebuffersTracker::resizeFramebuffers( int width , int height )
const
{
	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>
	   ::iterator fbIt = framebuffers.begin();

	while( fbIt != framebuffers.end() ){
		resizeFramebuffer( fbIt->first , fbIt->second.first , fbIt->second.second ,
						   width , height );
		++fbIt;
	}
}

void FramebuffersTracker::dumpFramebuffers(){

	std::map< unsigned int * , std::pair< std::vector<Texture2DColorbuffer> , std::vector<Renderbuffer>>>
	   ::iterator fbIt = framebuffers.begin();

	std::cout << "# Tracked framebuffers: " << framebuffers.size() << std::endl;
	while( fbIt != framebuffers.end() ){

		std::cout << "--" << std::endl;

		std::cout << "Framebuffer: " << *fbIt->first
				  << " | # colorbuffers: " << fbIt->second.first.size()
				  << " | # renderbuffers: " << fbIt->second.second.size()
				  << std::endl;

		std::cout << "\tColorbuffers = [ " << std::endl;
		for( unsigned int i=0 ; i < fbIt->second.first.size() ; ++i ){
			std::cout << "\t\t" << *(fbIt->second.first[i].cbo) << " , "
								<< fbIt->second.first[i].attachment << " , "
								<< fbIt->second.first[i].internalFormat << " , "
								<< fbIt->second.first[i].format << " , "
								<< fbIt->second.first[i].dataFormat << " , "
								<< fbIt->second.first[i].level << " , "
								<< fbIt->second.first[i].magFilter << " , "
								<< fbIt->second.first[i].minFilter
								<< std::endl;
		}
		std::cout << "]" << std::endl;

		std::cout << "\tRenderbuffers = [ " << std::endl;
		for( unsigned int i=0 ; i < fbIt->second.second.size() ; ++i ){
			std::cout << "\t\t" << *(fbIt->second.second[i].rbo) << " , "
								<< fbIt->second.second[i].attachment << " , "
								<< fbIt->second.second[i].format
								<< std::endl;
		}
		std::cout << "]" << std::endl;
		std::cout << "--" << std::endl;
		++fbIt;
	}

}
