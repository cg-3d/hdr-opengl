#ifndef ENGINE_LIGHTS_SPOTLIGHT_H_
#define ENGINE_LIGHTS_SPOTLIGHT_H_

#include <glm/glm.hpp>
#include "Light.h"


class SpotLight : public Light{

public:

	glm::vec3 position;
	glm::vec3 direction;

	float c, l, q;

	float cutoff , outerCutoff;

	SpotLight();
	SpotLight( glm::vec3 pos , glm::vec3 direction );

	SpotLight( glm::vec3 pos , glm::vec3 direction ,
			   float c , float l , float q );

	SpotLight( glm::vec3 pos , glm::vec3 direction ,
			   float cutoff, float outerCutoff );

	SpotLight( glm::vec3 pos , glm::vec3 direction ,
			   float c , float l , float q ,
			   float cutoff, float outerCutoff ,
			   glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular );

	void setShaderUniform( Shader &shader , std::string lightVarName ) const;
};



#endif /* ENGINE_LIGHTS_SPOTLIGHT_H_*/
