#include <glm/glm.hpp>

#include "PointLight.h"

PointLight::PointLight() :
		Light() ,
		position( glm::vec3( 0.0f ) ) ,
		c( 1.0f ) , l( 1.0f ) , q( 1.0f )
{ }

PointLight::PointLight( glm::vec3 position ) :
		Light() ,
		position( position ) ,
		c( 1.0f ) , l( 1.0f ) , q( 1.0f )
{ }

PointLight::PointLight( glm::vec3 position , float c , float l , float q) :
		Light() ,
		position( position ) ,
		c( c ) , l( l ) , q( q )
{ }

PointLight::PointLight( glm::vec3 pos , float c , float l , float q ,
						glm::vec3 ambient , glm::vec3 diffuse ,
						glm::vec3 specular ) :
		Light( ambient , specular , diffuse ) ,
		position( pos ) ,
		c( c ) , l( l ) , q( q )
{ }

void PointLight::setShaderUniform( Shader &shader , std::string lightVarName ) const {
	Light::setShaderUniform( shader , lightVarName );
	shader.setVec3( lightVarName + ".position" , this->position );
	shader.setFloat( lightVarName + ".c" , this->c );
	shader.setFloat( lightVarName + ".l" , this->l );
	shader.setFloat( lightVarName + ".q" , this->q );
	shader.setFloat( lightVarName + ".type" , LightTypes::POINT );
}
