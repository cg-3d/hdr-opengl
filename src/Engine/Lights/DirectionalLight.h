#ifndef ENGINE_LIGHTS_DIRECTIONALLIGHT_H_
#define ENGINE_LIGHTS_DIRECTIONALLIGHT_H_

#include <glm/glm.hpp>

#include "Light.h"

class DirectionalLight : public Light{

public:
	//Direction
	glm::vec3 direction;

	/**
	 * Defaults direction = vec3( 0.0 , -1.0 , 0.0 )
	 * facing down directional light
	 */
	DirectionalLight();
	DirectionalLight( glm::vec3 direction );

	DirectionalLight( glm::vec3 direction ,
					  glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular );

	void setShaderUniform( Shader &shader , std::string lightVarName ) const;
};



#endif /* ENGINE_LIGHTS_DIRECTIONALLIGHT_H_ */
