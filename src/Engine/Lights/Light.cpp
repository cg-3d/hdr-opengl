#include <glm/glm.hpp>
#include <string>

#include "Light.h"

Light::Light() :
	ambient( glm::vec3( 0.1f) ) ,
	diffuse( glm::vec3( 0.5f ) ) ,
	specular( glm::vec3( 1.0f ) ) ,
	intensity( 1.0f )
{ }

Light::Light( glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular ) :
	ambient( ambient ) ,
	diffuse( diffuse ) ,
	specular( specular ) ,
	intensity( 1.0f )
{ }

void Light::setShaderUniform( Shader &shader , std::string lightVarName ) const{
	shader.setVec3( lightVarName + ".core.ambient" , intensity * this->ambient );
	shader.setVec3( lightVarName + ".core.diffuse" , intensity * this->diffuse );
	shader.setVec3( lightVarName + ".core.specular" , intensity * this->specular );
}

