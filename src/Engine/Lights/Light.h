#ifndef ENGINE_LIGHTS_LIGHT_H_
#define ENGINE_LIGHTS_LIGHT_H_

#include <glm/glm.hpp>
#include <string>

#include "../Shader.h"

enum LightTypes {
	DIRECTIONAL = 1 ,
	POINT = 2 ,
	SPOT = 3
};

class Light {

protected:
	/**
	 * Defaults:
	 * ambient = vec3( 0.1 )
	 * diffuse = vec3( 0.5 )
	 * specular = vec3( 1.0 )
	 */
	Light();

	/**
	 * Accepts ambient, diffuse and specular
	 */
	Light( glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular );

public:
	//Light color components
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	float intensity;

	void setShaderUniform( Shader &shader , std::string lightVarName ) const;

};

#endif /* SCENES_DEFERREDSCENE_LIGHTS_H_ */
