#include <glm/glm.hpp>
#include "SpotLight.h"

SpotLight::SpotLight() :
		Light() ,
		position( glm::vec3( 0.0f ) ) ,
		direction( glm::vec3( 0.0f , -1.0f , 0.0f ) ) ,
		c( 1.0f ) , l( 1.0f ) , q( 1.0f ) ,
		cutoff( glm::radians( 25.0f ) ) ,
		outerCutoff( glm::radians( 40.0f ) )
{ }

SpotLight::SpotLight( glm::vec3 pos , glm::vec3 dir ) :
	Light() ,
	position( pos ) , direction( dir ) ,
	c( 1.0f ) , l( 1.0f ) , q( 1.0f ) ,
	cutoff( glm::radians( 25.0f ) ) ,
	outerCutoff( glm::radians( 40.0f ) )
{ }

SpotLight::SpotLight( glm::vec3 pos , glm::vec3 dir ,
		   	   	   	  float cutoff, float outerCutoff ) :
	Light() ,
	position( pos ) , direction( dir ) ,
	c( 1.0f ) , l( 1.0f ) , q( 1.0f ) ,
	cutoff( cutoff ) , outerCutoff( outerCutoff )
{ }

SpotLight::SpotLight( glm::vec3 pos , glm::vec3 dir , float c , float l , float q ) :
	Light() ,
	position( pos ) , direction( dir ) ,
	c( c ) , l( l ) , q( q ) ,
	cutoff( glm::radians( 25.0f ) ) ,
	outerCutoff( glm::radians( 40.0f ) )
{ }

SpotLight::SpotLight( glm::vec3 pos , glm::vec3 dir ,
			   	   	  float c , float l , float q ,
					  float cutoff, float outerCutoff ,
					  glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular ) :
	Light( ambient , diffuse , specular ) ,
	position( pos ) , direction( dir ) ,
	c( c ) , l( l ) , q( q ) ,
	cutoff( cutoff ) , outerCutoff( outerCutoff )
{ }

void SpotLight::setShaderUniform( Shader &shader , std::string lightVarName ) const {
	Light::setShaderUniform( shader , lightVarName );
	shader.setVec3( lightVarName + ".position" , this->position );
	shader.setVec3( lightVarName + ".direction" , this->direction );
	shader.setFloat( lightVarName + ".c" , this->c );
	shader.setFloat( lightVarName + ".l" , this->l );
	shader.setFloat( lightVarName + ".q" , this->q );
	shader.setFloat( lightVarName + ".cutoff" , this->cutoff );
	shader.setFloat( lightVarName + ".outerCutoff" , this->outerCutoff );
	shader.setFloat( lightVarName + ".type" , LightTypes::SPOT );
}
