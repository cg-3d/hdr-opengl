#include <glm/glm.hpp>
#include "DirectionalLight.h"

DirectionalLight::DirectionalLight() :
		Light() ,
		direction( glm::vec3( 0.0f , -1.0f , 0.0f ) )
{ }

DirectionalLight::DirectionalLight( glm::vec3 direction ) :
		Light() ,
		direction( direction )
{ }

DirectionalLight::DirectionalLight( glm::vec3 direction ,
								    glm::vec3 ambient ,
									glm::vec3 diffuse ,
									glm::vec3 specular ) :
	    Light( ambient , diffuse , specular ) ,
		direction( direction )
{ }

void DirectionalLight::setShaderUniform( Shader &shader , std::string lightVarName ) const{
	Light::setShaderUniform( shader , lightVarName );
	shader.setVec3( lightVarName + ".direction" , this->direction );
	shader.setInt( lightVarName + ".type" , LightTypes::DIRECTIONAL );
}
