#ifndef ENGINE_LIGHTS_POINTLIGHT_H_
#define ENGINE_LIGHTS_POINTLIGHT_H_

#include <glm/glm.hpp>

#include "Light.h"

class PointLight : public Light{

public:
	// Position
	glm::vec3 position;

	// Quadratic attenuation model
	float c , l  , q;

	PointLight();

	PointLight( glm::vec3 pos );
	PointLight( glm::vec3 pos , float c , float l , float q );
	PointLight( glm::vec3 pos , float c , float l , float q ,
				glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular );

	void setShaderUniform( Shader &shader , std::string lightVarName ) const;
};



#endif /* ENGINE_LIGHTS_POINTLIGHT_H_ */
