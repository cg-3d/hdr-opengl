#ifndef ENGINE_MESH_H_
#define ENGINE_MESH_H_

//#include <glad/glad.h> // holds all OpenGL type declarations

#include <glm/glm.hpp>

#include "Material.h"
#include "Shader.h"

#include <string>
#include <vector>
using namespace std;

struct Vertex {
    // position
    glm::vec3 Position;
    // normal
    glm::vec3 Normal;
    // texCoords
    glm::vec2 TexCoords;
    // tangent
    glm::vec3 Tangent;
    // bitangent
    glm::vec3 Bitangent;
};

struct Texture {
    unsigned int id;
    string type;
    string path;
};

class Mesh {
public:
    /*  Mesh Data  */
    vector<Vertex> vertices;
    vector<unsigned int> indices;
    vector<Texture> textures;
    unsigned int VAO;

    bool hasMaterial;
    Material material;

    /*  Functions  */
    // constructor
    Mesh(vector<Vertex> vertices, vector<unsigned int> indices, vector<Texture> textures);

    // render the mesh
    void Draw(Shader shader);

    bool hasMaterialProperties();
    void setMaterialProperties( glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular , float shininess );

    void setShaderMaterialProperties( Shader &shader );

private:
    /*  Render data  */
    unsigned int VBO, EBO;

    /*  Functions    */
    // initializes all the buffer objects/arrays
    void setupMesh();
};



#endif /* ENGINE_MESH_H_ */
