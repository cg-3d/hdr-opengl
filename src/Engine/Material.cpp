#include <glm/glm.hpp>

#include "Material.h"

Material::Material() :
	ambient( glm::vec3( 0.05f ) ) ,
	diffuse( glm::vec3( 0.5f ) ) ,
	specular( glm::vec3( 1.0f ) ) ,
	shininess( 1.0f ) {

}

Material::Material( glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular , float shininess ) :
	ambient(ambient) ,
	diffuse(diffuse) ,
	specular(specular) ,
	shininess(shininess){
}
