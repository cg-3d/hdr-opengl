#ifndef ENGINE_MATERIAL_H_
#define ENGINE_MATERIAL_H_

#include <glm/glm.hpp>

class Material{

public:
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
	float shininess;

	Material();
	Material( glm::vec3 ambient , glm::vec3 diffuse , glm::vec3 specular , float shininess );

};



#endif /* ENGINE_MATERIAL_H_ */
