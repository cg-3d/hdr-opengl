#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include "linear-algebra.h"

glm::mat2x3 engLinAlg::calculateTangentAndBitangent( glm::vec3 p1 , glm::vec3 p2 , glm::vec3 p3 ,
												     glm::vec2 uv1 , glm::vec2 uv2 , glm::vec2 uv3 ){

	// Edges
	glm::vec3 e1 = p2 - p1;
	glm::vec3 e2 = p3 - p1;
	// UV deltas
	glm::vec2 deltaUV1 = uv2 - uv1;
	glm::vec2 deltaUV2 = uv3 - uv2;

	//  2x3                      2x2                  2x3
	//
	// [ T ]	=	 [ deltaUV1.x  deltaUV2.x ]^-1   [ E1 ]
	// [ B ]	     [ deltaUV1.y  deltaUV2.y ]      [ E2 ]
	glm::mat2x3 tanBitan = glm::transpose(
						   	   glm::inverse( glm::mat2( deltaUV1 , deltaUV2 ) )
						     * glm::transpose( glm::mat2x3( e1  , e2 ) )
						   );

	tanBitan[0] = glm::normalize( tanBitan[0] );
	tanBitan[1] = glm::normalize( tanBitan[1] );

	return tanBitan;

	// Explicit calculation
//	glm::mat2x3 tanBitan;
//	float d = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y );
//
//	tanBitan[0].x = d * ( deltaUV2.y * e1.x - deltaUV1.y * e2.x );
//	tanBitan[0].y = d * ( deltaUV2.y * e1.y - deltaUV1.y * e2.y );
//	tanBitan[0].z = d * ( deltaUV2.y * e1.z - deltaUV1.y * e2.z );
//
//	tanBitan[1].x = d * ( -deltaUV2.x * e1.x - deltaUV1.x * e2.x );
//	tanBitan[1].y = d * ( -deltaUV2.x * e1.y - deltaUV1.x * e2.y );
//	tanBitan[1].z = d * ( -deltaUV2.x * e1.z - deltaUV1.x * e2.z );
//
//	tanBitan[0] = glm::normalize( tanBitan[0] );
//	tanBitan[1] = glm::normalize( tanBitan[1] );

	//return tanBitan;
}
