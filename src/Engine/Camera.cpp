#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../utilities/utilities.h"

#include "Camera.h"


void Camera::printStatus(){
	std::cout << "Position: ";
	utilities::printVec3( this->position );

	std::cout << " | Target: ";
	utilities::printVec3( this->target );

	std::cout << " | Up: ";
	utilities::printVec3( this->up );

	std::cout << " | Front: ";
	utilities::printVec3( this->front );

	std::cout << " | PosToTarget: ";
	utilities::printVec3( this->target - this->position );

	std::cout << " | yaw: " << this->yaw << " , pitch: " << this->pitch;

	std::cout << std::endl;
}

/**
 * Builds a camera located at "position", with "up" upvector and
 * pointing to "target"
 */
Camera::Camera( const glm::vec3 position , const glm::vec3 target , const glm::vec3 up ) :
		position( position ) , target( target ) , up( up ) ,
		pitch( 0.0f ) , yaw( 0.0f ) ,
		lockTargetOnMove( false ) , cameraOrbitRadius( 5.0f ){

	this->front = glm::normalize( target - position );
	this->worldUp = up;

	printStatus();
};

/**
 * Builds a camera pointing to the specified target
 * and defaulting:
 * 	position = ( 0 , 0 , 5 )
 * 	up = ( 0 , 1 , 0 )
 */
Camera::Camera( const glm::vec3 target ){
	Camera( glm::vec3( 0.0f , 0.0f , 0.5f ) ,
			target ,
			glm::vec3( 0.0f , 1.0f , 0.0f ) );
}

/**
 * Default constructor with:
 *
 * 	position = (  0 , 0 , 5 )
 * 	target = (  0 , 0 , 0 )
 * 	up = (  0 , 1 , 0 )
 */
Camera::Camera(){
	Camera( glm::vec3( 0.0f , 0.0f , 0.0f ) );
}

/**
 *
 * Returns the view matrix built from the
 * current camera state.
 */
glm::mat4 Camera::getViewMatrix(){
	//if( !lockTargetOnMove )
		return glm::lookAt( this-> position , this->position + this->front , this->up );
	//else
		//return glm::lookAt( this->position , this->position + cameraOrbitRadius * this->front , this->up );
}

/**
 * Returns the current camera position.
 */
const glm::vec3& Camera::getPosition() const{
	return this->position;
}

/**
 * Returns the current camera target vector.
 */
const glm::vec3& Camera::getTarget() const{
	return this->target;
}

/**
 * Returns the current camera up vector.
 */
const glm::vec3& Camera::getUp() const{
	return this->up;
}

const glm::vec3& Camera::getFront() const{
	return this->front;
}

const glm::vec3& Camera::getWorldUp() const{
	return this->worldUp;
}

const float Camera::getYaw() const{
	return this->yaw;
}

const float Camera::getPitch() const{
	return this->pitch;
}

/**
 * Returns true if the target is locked when the
 * camera is moved, false if the camera keeps pointing
 * in the same direction it was pointing before moving.
 */
bool Camera::isTargetLockedOnMove() const{
	return lockTargetOnMove;
}

float Camera::getCameraOrbitRadius() const{
	return cameraOrbitRadius;
}

void Camera::setCameraOrbitRadius( float cameraOrbitRadius ){
	this->cameraOrbitRadius = cameraOrbitRadius;
}



/**
 * Set if the camera should lock the target or not.
 */
void Camera::setTargetLockedOnMove( bool locked ) {
	this->lockTargetOnMove = locked;
	if( locked ){
		this->target = this->position + cameraOrbitRadius * this->front;
	}else{
		this->target = this->position + this->front;
		// TODO : track yaw and pitch during obit movement
		//		  to avoid front reset after unlock
		this->yaw = 0;
		this->pitch = 0;
	}
}

/**
 * Move the camera absolutely to the specified position.
 */
void Camera::moveTo( const glm::vec3 position ){

	glm::vec3 positionToTarget = this->target - position;
//	glm::vec3 movement = position - this->position;

	if( !lockTargetOnMove || glm::length( positionToTarget ) > 0.05f )
		this->position = position;

	if( !lockTargetOnMove ) // Update the target vector if not locked
		this->target = this->position + this->front;
		//this->target = this->target + movement;

	// Update the front vector
//	if( positionToTarget != glm::vec3( 0.0f , 0.0f , 0.0f ) )
//		this->front = glm::normalize( positionToTarget );
	//this->front = glm::normalize( this->target - this->position );

	// Debug
	// printStatus();
}

/**
 * Move the relatively camera from the current position by
 * "movement".
 */
void Camera::moveBy( const glm::vec3 movement ){
	this->moveTo( this->position + movement );
}

/**
 * Orbit the camera around the current target by "randians".
 */
void Camera::orbitBy( const float radians ){

	// Calculates the vector from "target" to "position".
	// This vector represents the camera position in a
	// coordinate system centered in "target"
	//glm::vec3 targetToPosition = this->position - this->target;
	//this->target = this->target - this->front * cameraOrbitRadius;
	glm::vec3 targetToPosition = this->position - this->target;

	// Calculates the position after the rotation around
	// "target", this is the new position expressed in the coordinate
	// system centered in "target"
	glm::vec4 newPos_target = glm::vec4( targetToPosition , 1.0f )
							* glm::rotate( glm::mat4( 1.0f ) , radians , this->up );


	// Retrieve the new position in world coordinates, translating by "target
	this->position = glm::vec3( newPos_target.x , newPos_target.y , newPos_target.z )
				   + this->target;

	// Updates the front vector
	this->front = glm::normalize( this->target - this->position );

	// Debug
	//printStatus();
}

void Camera::moveLook( const float xoffset , const float yoffset ){

	if( lockTargetOnMove )
		return;

	//std::cout << "moveLook( " << xoffset << " , " << yoffset << "  ) : ";

	this->yaw += xoffset;
	this->pitch += yoffset;

	if( this->pitch > 89.0f )
		this->pitch = 89.0f;
	if( this->pitch < -89.0f )
		this->pitch = -89.0f;

	//std::cout << "yaw = " << yaw << " , pitch = " << pitch << "  ) : Front ";

	glm::vec3 newFront;
	newFront.z = -cos( glm::radians( yaw ) ) * cos( glm::radians( pitch ) );
	newFront.y = -sin( glm::radians( pitch ) );
	newFront.x = sin( glm::radians( yaw ) ) * cos( glm::radians( pitch ) );

	// Updates front and up
	this->front = glm::normalize( newFront );
	this->up = glm::normalize( glm::cross( glm::cross( front , worldUp ) , front ) );

	// Debug
//	utilities::printVec3( this->front );
//	std::cout << std::endl;
}
