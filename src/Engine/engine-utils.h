#ifndef ENGINE_ENGINE_UTILS_H_
#define ENGINE_ENGINE_UTILS_H_

//#include <glad/glad.h>

namespace engUtils{

unsigned int textureFromFile(const char *path, bool gamma);

void createFrameTextureRenderBuffer( unsigned int *fbo , unsigned int *texcb , unsigned int *rb ,
									 int width , int height  ,
									 GLint colorInternalFormat = GL_RGB,
									 GLenum colorFormat = GL_RGB ,
									 GLenum colorDataFormat = GL_UNSIGNED_BYTE ,
									 GLenum renderInternalFormat = GL_DEPTH24_STENCIL8 ,
									 GLenum renderAttachment = GL_DEPTH_STENCIL_ATTACHMENT
								   );

void createFrameTextureBuffer( unsigned int *fbo , unsigned int *texcb ,
							   int width , int height ,
							   GLint colorInternalFormat , GLenum colorFormat , GLenum colorDataFormat );

}


#endif /* ENGINE_ENGINE_UTILS_H_ */
