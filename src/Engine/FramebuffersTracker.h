#ifndef ENGINE_FRAMEBUFFERSTRACKER_H_
#define ENGINE_FRAMEBUFFERSTRACKER_H_

#include <vector>
#include <map>

//#include <glad/glad.h>

/**
 * Tracks and resize framebuffers
 */
class FramebuffersTracker {

public:
	FramebuffersTracker();
	~FramebuffersTracker();

	/**
	 * Attach a GL_TEXTURE_2D colorbuffer to this tracker.
	 */
	void attachFramebufferTex2DColorbuffer( unsigned int *fbo ,
										    unsigned int *cb ,
										    GLenum attachment ,
										    GLint internalFormat ,
										    GLenum format ,
										    GLenum dataFormat ,
										    GLint level ,
											GLenum magFilter ,
											GLenum minFilter
										  ) const;

	/**
	 * Attach a renderbuffer to this tracker.
	 */
	void attachFramebufferRenderbuffer( unsigned int *fbo ,
									    unsigned int *rbo ,
									    GLenum attachment ,
									    GLenum format
									  ) const;

	/**
	 * Attach a GL_TEXTURE_2D colorbuffer and a
	 * renderbuffer to this tracker.
	 */
	void attachFramebufferTex2DColorbufferRenderbuffer( unsigned int *fbo ,
													    unsigned int *cb ,
													    GLenum cbAttachment ,
													    GLint cbInternalFormat ,
													    GLenum cbFormat ,
													    GLenum cbDataFormat ,
													    GLint cbLevel ,
														GLenum magFilter ,
														GLenum minFilter ,
														unsigned int *rbo ,
														GLenum rbAttachment ,
														GLenum rbFormat
													  ) const;

	/*
	 * Detach an entire framebuffer and all of its attachments from this tracker
	 */
	void detachFramebuffer( unsigned int *fbo ) const;

	/**
	 * Detach the cbo colbuffer from the tracked attachments of the
	 * fbo framebuffer.
	 */
	void detachFramebufferColorbuffer( unsigned int* fbo , unsigned int *cbo ) const;

	/**
	 * Detach the rbo renderbuffer from the tracked attachments of the
	 * fbo framebuffer.
	 */
	void detachFramebufferRenderbuffer( unsigned int* fbo , unsigned int *rbo ) const;

	/**
	 * Set a call to glDrawBuffers() after regenerating the color attachments
	 * for the specified framebuffer.
	 * The drawbuffers is performed for the all the CURRENTLY attached
	 * framebuffer's color attachments.
	 */
	void addDrawBuffer( unsigned int *fbo , GLenum attachment ) const;

	/*
	 * Resize framebuffer (if attached) and all of its attachments.
	 */
	void resizeFramebuffer( unsigned int * fbo , int width , int height ) const;

	/*
	 * Resize all attached framebuffers and their respective attachments.
	 */
	void resizeFramebuffers( int width , int height ) const;

	/**
	 * Debug
	 */
	void dumpFramebuffers();

private:
	struct Texture2DColorbuffer{
		unsigned int *cbo;
		GLenum attachment;
		GLint internalFormat;
		GLenum format;
		GLenum dataFormat;
		GLint level;
		GLenum magFilter;
		GLenum minFilter;
	};

	struct Renderbuffer{
		unsigned int *rbo;
		GLenum attachment;
		GLenum format;
	};

	mutable std::map< unsigned int * ,
			  std::pair<
		  	  	  std::vector<Texture2DColorbuffer> ,
				  std::vector<Renderbuffer>
			  >
	> framebuffers;

	mutable std::map< unsigned int *, std::vector<GLenum>> drawbuffers;

	void bindFramebufferTex2DColorbuffer( unsigned int *fbo , Texture2DColorbuffer texcb ) const;

	void bindFramebufferRenderbuffer( unsigned int *fbo , Renderbuffer rb ) const;

	void bindFramebufferTex2DColorbufferRenderbuffer( unsigned int *fbo , Texture2DColorbuffer texcb , Renderbuffer rb ) const;

	void resizeFramebuffer( unsigned int *fbo ,
						    std::vector<Texture2DColorbuffer> texcbs ,
							std::vector<Renderbuffer> rbos ,
							int width , int height ) const;


};



#endif /* ENGINE_FRAMEBUFFERSTRACKER_H_ */
