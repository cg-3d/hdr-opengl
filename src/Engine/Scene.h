#ifndef ENGINE_SCENE_H_
#define ENGINE_SCENE_H_

#include <vector>

class Scene{

	typedef struct BufferObject{
		unsigned int nBuffers;
		unsigned int buffer;
	} BufferObject;

	private:
		std::vector<BufferObject *> VBOs;
		std::vector<BufferObject *> VAOs;
//		std::vector<BufferObject> EBOs;


	public:
		unsigned int framebuffer;
		//unsigned int renderbuffer;
		unsigned int textureColorBuffer;

		Scene(); // The zero-arguments constructor gets called automatically in the subclasses constructor
		virtual ~Scene();

		void deleteFrameTextureRenderBuffers();
		void deleteBufferObjects();

		//Frameworks methods
		virtual void draw() = 0;
		virtual void drawGUI() = 0;
		virtual void freeGLResources() = 0;

		virtual void freeScene() final; //not overridable

	protected:
		// These methods are available in sub classes but not ovverridable
		virtual unsigned int newVBO( unsigned int nBuffers = 1 ) final;
		virtual unsigned int newVAO( unsigned int nBuffers = 1 ) final;
//		virtual unsigned int newEBO( unsigned int nBuffers = 1 ) final;

};



#endif /* ENGINE_SCENE_H_ */
