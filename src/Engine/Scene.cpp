/*
 * Scene.cpp
 *
 *  Created on: Jun 12, 2019
 *      Author: chris
 */
#include<iostream>

#include <glad/glad.h>

#include "input-callbacks.h"
#include "./engine-utils.h"
//#include "../engine-modules.h"

#include "Scene.h"

Scene::Scene() {
	// Init vectors to track buffers
	VBOs = std::vector<BufferObject *>();
	VAOs = std::vector<BufferObject *>();
//	EBOs = std::vector<BufferObject *>();

	// Init frame , texture color buffers
	engUtils::createFrameTextureBuffer(
		&framebuffer ,
		&textureColorBuffer ,
		incalls::sceneViewWidth ,
		incalls::sceneViewHeight ,
		GL_RGB , GL_RGB , GL_UNSIGNED_BYTE
	);

}

/**
 * Add a Vertex Buffer Object to the scene.
 * nBuffers is the number of buffers to allocate for the
 * buffer object.
 */
unsigned int Scene::newVBO( unsigned int nBuffers ){
	BufferObject *VBO = new BufferObject;
	VBO->nBuffers = nBuffers;
	glGenBuffers( nBuffers , &VBO->buffer );
	VBOs.push_back( VBO );

	return VBO->buffer;
}

unsigned int Scene::newVAO( unsigned int nBuffers ){
	BufferObject *VAO = new BufferObject;
	VAO->nBuffers = nBuffers;
	glGenVertexArrays( nBuffers , &VAO->buffer );
	VAOs.push_back( VAO );

	return VAO->buffer;
}

//unsigned int Scene::newEBO( unsigned int nBuffers ){
//	BufferObject EBO = { 0 , NULL };
//	EBO.nBuffers = nBuffers;
//	glGenBuffers( nBuffers , EBO.buffer );
//	VBOs.push_back( EBO );
//
//	return *EBO.buffer;
//}

/*
 * Release the Frame, Texture and Render buffers of the scene.
 */
void Scene::deleteFrameTextureRenderBuffers(){
	//glDeleteRenderbuffers( 1 , &renderbuffer );
	glDeleteTextures( 1 , &textureColorBuffer );
	glDeleteFramebuffers( 1 , &framebuffer );
}

/**
 * Release all VertexBufferObjects, VertexArrayObjects and
 * ElementBufferObjects of the scene.
 */
void Scene::deleteBufferObjects(){

	for( unsigned int i = 0 ; i < VBOs.size() ; i++ ){
		glDeleteBuffers( VBOs[i]->nBuffers , &VBOs[i]->buffer );
		delete VBOs[i];
	}
	VBOs.clear();

	for( unsigned int i = 0 ; i < VAOs.size() ; i++ ){
		glDeleteVertexArrays( VAOs[i]->nBuffers , &VAOs[i]->buffer );
		delete VAOs[i];
	}
	VAOs.clear();

//	for( unsigned int i = 0 ; i < EBOs.size() ; i++ ){
//		glDeleteBuffers( EBOs[i].nBuffers , EBOs[i].buffer );
//	}
//	EBOs.clear();
}

void Scene::freeScene(){
	freeGLResources();
	deleteBufferObjects();
	deleteFrameTextureRenderBuffers();
}

/**
 * Desstructor, release all scene buffers.
 */
Scene::~Scene(){
	//deleteFrameTextureRenderBuffers();
	//deleteBufferObjects();
}



