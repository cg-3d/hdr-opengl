#ifndef SHADER_H
#define SHADER_H

//#include <glad/glad.h> // include glad to get all the required OpenGL headers

#include <string>
#include <glm/glm.hpp>

class Shader {
public:
    // the program ID
    unsigned int ID;

    // constructor reads and builds the shader
    //Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
    Shader(const char* vertexPath, const char* fragmentPath);
    // use/activate the shader
    void use();
    // utility uniform functions
    void setBool(const std::string &name, bool value);
    void setInt(const std::string &name, int value);
    void setFloat(const std::string &name, float value);
    void setMatrix4fv( const std::string &name , glm::mat4 matrix );
    void setMatrix3fv( const std::string &name , glm::mat3 matrix );
    void setVec3( const std::string &name , glm::vec3 v );
};


#endif /* SHADER_H_ */
