#ifndef INPUT_CALLBACKS_H_
#define INPUT_CALLBACKS_H_

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include <map>

#include "FramebuffersTracker.h"
#include "Shader.h"

#include "Camera.h"

namespace incalls
{
	// Viewports
	extern int screenWidth , screenHeight;
	extern int sceneViewWidth , sceneViewHeight;
	extern bool sceneViewActive;

	// Delta time correction
	extern float deltaTime;
	extern float lastFrame;

	// Camera
	extern Camera *camera;
	extern float cameraSpeed;
	extern float orbitSpeed; //degrees

	// Mouse
	struct MouseStatus {
		float sensitivity;
		double lastX;
		double lastY;
		bool firstMouse;
	};

	extern MouseStatus mouse;

	//Projection
	extern glm::mat4 *projection;
    extern std::map<Shader * , std::string> projectionUniforms;

    /**
     * Const access to the FramebuffersTracker
     */
    const FramebuffersTracker &fbTracker();

	/**
	 * Binds a camera as the camera managed by the
	 * input callbacks.
	 */
	void bindCamera( Camera *cam );

	void bindProjectionMatrix( glm::mat4 *projection );

	void bindProjectionUniform( Shader *shader , std::string name );

	void unbindProjectionUniform( Shader *shader );

	void setScreenSize( int width , int height );

	/**
	 * Sets the camera speed.
	 */
	void setCameraSpeed( float speed );

	/**
	 * Returns cameraSpeed with delta time correction.
	 */
	float getCameraSpeed();

	/**
	 * Sets the camera orbit speed.
	 */
	void setCameraOrbitSpeed( float speed );

	/**
	 * Return orbitSpeed with delta time correction.
	 */
	float getCameraOrbitSpeed();

	/**
	 * Updates the current delta time stored.
	 */
	void updateDeltaTime();

	/**
	 * The callback to call on window resizing
	 */
	void framebuffer_size_callback( GLFWwindow* window , int width , int height );

	void key_callback( GLFWwindow* window , int key , int scancode , int action , int mods );

	void mouse_callback( GLFWwindow* window , double xpos , double ypos );

	void mouse_button_callback( GLFWwindow *window , int button , int action , int mods );

	/**
	 * Process inputs from the user
	 */
	void process_prolonged_inputs( GLFWwindow *window );

}

#endif /* INPUT_CALLBACKS_H_ */
