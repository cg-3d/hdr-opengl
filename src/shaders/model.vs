#version 450
layout (location=0) in vec3 pos;
layout (location=1) in vec3 normal;
layout (location=2) in vec2 textCoords;

out vec2 TextCoords;
out vec3 Normal;
out vec3 FragPos;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(){
	gl_Position = projection * view * model * vec4( pos , 1.0f );
	
	Normal = normalize( normal );
	FragPos = vec3( model * vec4( pos , 1.0f ) );
	TextCoords = textCoords;
}