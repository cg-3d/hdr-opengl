#version 450
in vec3 TextDir;

out vec4 FragColor;

uniform samplerCube cubemap; //Specific sampler for cubemaps

void main(){

	FragColor = texture( cubemap , TextDir );
}


