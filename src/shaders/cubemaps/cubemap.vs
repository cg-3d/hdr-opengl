#version 450
layout (location=0) in vec3 aPos;

out vec3 TextDir;

uniform mat4 projection;
uniform mat4 view;

void main(){
	TextDir = aPos;
	//gl_Position = projection * view * vec4( aPos , 1.0 );
	vec4 pos = projection * view * vec4( aPos , 1.0 );
	gl_Position = pos.xyww; // set the z component equale to the w coordinate to have:
						    // 	depth = ( z / w ) = ( w / w ) = 1.0.
						    // ie alwaiys depth value of 1.0 for the skybox
}

