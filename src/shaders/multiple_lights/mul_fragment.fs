#version 450

// Encapsulates the ambient, diffuse and specular
// light colors in a structure for covenience
struct LightCore{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};


// Directional light
struct DirectionalLight{
	vec3 direction;
	
	LightCore core;
};

// Point Light
struct PointLight{
	vec3 position;
	
	LightCore core;
	
	float c;
	float l;
	float q;
};

// Spot Light
struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutoff;
	float outerCutoff;
	
	LightCore core;
	
	float c;
	float l;
	float q;
};

mat3 calculateLightComponents( LightCore core , vec3 normal , vec3 viewDirection , vec3 lightDirection );
vec3 calculateDirectionalLight( DirectionalLight light , vec3 normal , vec3 viewDirection );
vec3 calculatePointLight( PointLight light , vec3 normal , vec3 viewDirection );
vec3 calculateSpotLight( SpotLight light , vec3 normal , vec3 viewDirection );

//Uniforms
uniform DirectionalLight dirLight;
uniform PointLight pointLight;
uniform SpotLight spotLight;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform float shininess;

uniform mat3 normalMatrix;
uniform vec3 ViewPos;

//Inputs
in vec3 Normal;
in vec2 TextCoords;
in vec3 FragPos;

//Outputs
out vec4 FragColor;

void main(){

	vec3 norm = normalize( normalMatrix * Normal );
	vec3 viewDirection = normalize( ViewPos - FragPos );
	
	//FragColor = vec4( calculateDirectionalLight( dirLight , norm , viewDirection ) , 1.0f );
	//FragColor = vec4( calculatePointLight( pointLight , norm , viewDirection ) , 1.0f );
	//FragColor = vec4( calculateSpotLight( spotLight , norm , viewDirection ) , 1.0f );
	
	vec3 outputColor = calculateDirectionalLight( dirLight , norm , viewDirection );
	outputColor += calculatePointLight( pointLight , norm , viewDirection );
	outputColor += calculateSpotLight( spotLight , norm , viewDirection );
	FragColor = vec4( outputColor , 1.0f );

}

// Calculates the light components and return them as a mat3, for convenience. 
// In such matrix the columns are respectively the ambient, diffuse and specular components.
// This is the core function for every light calculation function.
mat3 calculateLightComponents( LightCore core , vec3 normal , vec3 viewDirection , vec3 lightDirection ){
	
	vec3 diffuseMapColor = texture( texture_diffuse1 , TextCoords ).rgb;
	vec3 reflectDirection = reflect( -lightDirection , normal );
	float spec = pow( max( dot( viewDirection  , reflectDirection ) , 0.0f ) , shininess );
	
	vec3 ambient = core.ambient * diffuseMapColor;	
	vec3 diffuse = core.diffuse * ( max( dot( normal , lightDirection ) , 0.0f ) * diffuseMapColor );
	vec3 specular = core.specular * ( spec * texture( texture_specular1 , TextCoords ).rgb );
	
	return mat3( ambient , diffuse , specular );
}

// Directional light calculation
vec3 calculateDirectionalLight( DirectionalLight light , vec3 normal , vec3 viewDirection ){

	vec3 lightDirection = normalize( -light.direction ); // uses the light direction
	mat3 lightComponents = calculateLightComponents( light.core  , normal , viewDirection , lightDirection );
	
	vec3 ambient = lightComponents[0];
	vec3 diffuse = lightComponents[1];
	vec3 specular = lightComponents[2]; 

	return ( ambient + diffuse + specular );
} 

// Point light calculation
vec3 calculatePointLight( PointLight light , vec3 normal , vec3 viewDirection ){

	vec3 lightDirection = normalize( light.position - FragPos );
	float dist = length( light.position - FragPos );
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist );
	
	mat3 lightComponents = calculateLightComponents( light.core , normal , viewDirection , lightDirection );
	
	vec3 ambient = attenuation * lightComponents[0];
	vec3 diffuse = attenuation * lightComponents[1];
	vec3 specular = attenuation * lightComponents[2];
	
	return ( ambient + diffuse + specular );
}

// Spot light calculation
vec3 calculateSpotLight( SpotLight light , vec3 normal , vec3 viewDirection ){

	vec3 lightDirection = normalize( light.position - FragPos );
	float dist = length( light.position - FragPos );
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist ); 

	// smooth cone edges
	float thetaCos = dot( lightDirection , normalize( -light.direction ) );
	float epsilon = light.cutoff - light.outerCutoff;
	float intensity = clamp( ( thetaCos - light.outerCutoff ) / epsilon , 0.0f , 1.0f );
	
	mat3 lightComponents = calculateLightComponents( light.core , normal , viewDirection , lightDirection );
	
	vec3 ambient = attenuation * lightComponents[0]; // ambient does not use intensity
	vec3 diffuse = intensity * attenuation * lightComponents[1];
	vec3 specular = intensity * attenuation * lightComponents[2];
	
	return ( ambient + diffuse + specular );
}
  