#version 450
//Lights

// Directional light:
// needs direction only, assumes all lightrays are parallel
struct DirectionalLight{
	vec3 direction;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
}; 

// Point Light:
// 	takes attenuation into account, inversely propotional  
//  to the fragment's distance d:
//
//		attenuationFactor = 1  / ( c + l * d  + q * d^2 )
// 
struct PointLight{
	vec3 position;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	
	float c;
	float l;
	float q;
};

// Spot Light:
// 	shoots light rays in a specific direction, 
//  described by:
//		- spot light position
//		- spot light direction
// 		- cutoff: cosine of the maximum angle between light's direction ( light.position - FragPos ), 
//				  and the spot light direction to determine if the 
//				  fragment is lit by the spotlight or not.  
// 		- outerCutoff: cutoff for the outer cone to simulate
//					   smoooth edges				       
struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutoff;      // INNER cone cosine threshold
	float outerCutoff; // OUTER cone cosine threshold  
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular; 
	
	// Implements attenuation in spot lights
	float c;	
	float l;	
	float q;
};

uniform SpotLight light;

//Uniforms
uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform mat3 normalMatrix;
uniform vec3 ViewPos;

//Inputs
in vec3 Normal;
in vec2 TextCoords;
in vec3 FragPos;

//Outputs
out vec4 FragColor;

void main(){

	vec3 norm = normalize( normalMatrix * Normal );
	 
	// Directional light
	//vec3 lightDirection = normalize( -light.direction ); //Flips light direction
	
	// Point and Spot light: calculate attenuation 
	vec3 lightDirection = normalize( light.position - FragPos ); 
	float dist = length( light.position - FragPos );
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist );
	
	// Spot Light
	float thetaCos = dot( lightDirection , normalize( -light.direction ) );
	// Compute intesity to smooth the cone edge:
	//
	// clamping between 0 and 1 assures the intensity is 0 for
	// every fragment outside the light cone
	float epsilon = light.cutoff - light.outerCutoff;
	float intensity =  clamp( ( thetaCos - light.outerCutoff ) / epsilon , 0.0f , 1.0f );    
								
	// Calculate ambient, diffuse and specular components
	vec3 diffuseSampledColor = vec3( texture( texture_diffuse1 , TextCoords ) );
	float diff = max( dot( norm , lightDirection ) , 0.0 );
	vec3 diffuse = light.diffuse * ( diff * diffuseSampledColor );
	
	vec3 ambient = light.ambient * diffuseSampledColor;
	
	vec3 viewDirection = normalize( ViewPos - FragPos );
	vec3 reflectDirection = reflect( -lightDirection , norm );
	float spec = pow( max( dot( viewDirection , reflectDirection ) , 0.0 ) , 64.0 );
	vec3 specular = light.specular * ( spec * vec3( texture( texture_specular1 , TextCoords ) ) );
	
	// Point and Spot lights : apply attenuation and/or intensity
	// intensity does not affect ambient light so we still 
	// have a litte lighting for the fragment outside the light cone 
	ambient *= attenuation;  
	diffuse *= intensity * attenuation;
	specular *= intensity * attenuation;
	
	FragColor = vec4( ambient + diffuse + specular , 1.0f );
}