// Reflection shader implementation

#version 450 

out vec4 FragColor;

in vec3 Normal;
in vec3 Position;

uniform vec3 cameraPos;
uniform samplerCube skybox;

void main(){
	vec3 viewDirection = normalize( Position - cameraPos );
	vec3 reflectDirection = reflect( viewDirection , Normal );
	FragColor = vec4( texture( skybox , reflectDirection ).rgb , 1.0 );
}