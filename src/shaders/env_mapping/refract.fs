// Refaction shader implementation

#version 450 

out vec4 FragColor;

in vec3 Normal;
in vec3 Position;

uniform vec3 cameraPos;
uniform samplerCube skybox;

void main(){
	float ratio = 1.00 / 1.52;
	vec3 viewDirection = normalize( Position - cameraPos );
	vec3 refractDirection = refract( viewDirection , Normal , ratio );
	FragColor = vec4( texture( skybox , refractDirection ).rgb , 1.0 );
}