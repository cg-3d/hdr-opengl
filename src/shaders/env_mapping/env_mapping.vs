#version 450 
layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aNormal;

out vec3 Position;
out vec3 Normal;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat3 normalMatrix;

void main(){
	Normal = normalize( normalMatrix * aNormal );
	Position = vec3( model * vec4( aPos , 1.0 ) ); // pass the vertex position in world coordinates
	gl_Position = projection * view * model * vec4( aPos , 1.0 );
}