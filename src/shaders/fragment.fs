#version 450 core

// Materials
struct Material {
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float shininess;
};

uniform Material material;

// Lights
struct Light {
	vec3 position;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

uniform Light light;

//in vec2 TexCoord;
//uniform sampler2D ourTexture;

uniform vec3 lightColor;
uniform vec3 objectColor;
uniform vec3 lightPosition;
uniform vec3 ViewPos;

uniform mat3 normalMatrix;

// Inputs
in vec3 Normal;
in vec3 FragPos;

// Outputs
out vec4 FragColor;

void main(){
	//FragColor = texture( ourTexture , TexCoord );
	vec3 norm = normalize( normalMatrix * Normal );
	
	//float ambientLightStrength = 0.1;
	vec3 ambient = light.ambient * material.ambient;

	vec3 lightDirection = normalize( lightPosition - FragPos );
	float diff = max( dot( norm , lightDirection ) , 0.0 );
	vec3 diffuse = light.diffuse * ( diff * material.diffuse );
				 
	//float specularStrength = 0.5;
	vec3 viewDirection = normalize( ViewPos - FragPos );
	vec3 reflectDirection = reflect( -lightDirection , norm );
	float spec = pow( max( dot( viewDirection , reflectDirection ) , 0.0 ), material.shininess );
	vec3 specular = light.specular * ( spec * material.specular );
	
	FragColor = vec4( ambient + diffuse + specular , 1.0f );
}