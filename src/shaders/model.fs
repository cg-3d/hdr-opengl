#version 450

//Light 
struct Light{
	vec3 position;
	
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

uniform Light light;

// Outputs 
out vec4 FragColor;

// Inputs
in vec2 TextCoords;
in vec3 Normal;
in vec3 FragPos;

//Uniforms
uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform mat3 normalMatrix;
uniform vec3 ViewPos;

void mai(){
	FragColor = vec4( vec3( 0.5 ) , 1.0f );
} 

void main(){
	// Modify normal according to the normalMatrix
	vec3 norm = normalize( normalMatrix * Normal );
	
	vec3 lightDirection = normalize( light.position - FragPos );
	float diff = max( dot( norm , lightDirection ) , 0.0 );
	vec3 diffuseSampledColor = vec3( texture( texture_diffuse1 , TextCoords ) );
	vec3 diffuse = light.diffuse * ( diff * diffuseSampledColor );
	
	vec3 ambient = light.ambient * ( diff * diffuseSampledColor );
	
	vec3 viewDirection = normalize( ViewPos - FragPos );
	vec3 reflectDirection = reflect( -lightDirection , norm );
	float spec = pow( max( dot( viewDirection , reflectDirection ) , 0.0 ) , 64.0 );
	vec3 specular = light.specular * ( spec * vec3( texture( texture_specular1 , TextCoords ) ) );

	FragColor = vec4( ambient + diffuse + specular , 1.0f );
	//FragColor = vec4( diffuse , 1.0f );
}