#include <iostream>
#include <experimental/filesystem>
//#include <stdlib.h>
//#include <glad/glad.h>
#include <GLFW/glfw3.h>
//
#include <glm/glm.hpp>
//#include <glm/gtc/matrix_transform.hpp>
//#include <glm/gtc/type_ptr.hpp>
//#include <glm/gtc/constants.hpp>
//
//#include "Engine/Shader.h"
//#include "Engine/Image.h"
//#include "Engine/Model.h"
//
#include "Engine/Camera.h"
#include "Engine/input-callbacks.h"
//
//#include "Engine/cubemaps.h"
//
//#include "shapes/shapes.h"
#include "utilities/utilities.h"
//#include "Engine/engine-utils.h"

#include "engine-modules.h"

#include "Scenes/DemoScene/DemoScene.h"
#include "Scenes/BPLightsScene/BPLightsScene.h"
#include "Scenes/HDRScene/HDRScene.h"
#include "Scenes/DeferredScene/DeferredScene.h"

// Global to be accessible by process_prolonged_inputs()
HDRScene *hdrScene = nullptr;

int main(int argc, char** argv){

	// Engine window initialization
	GLFWwindow *window = engModules::initWindow( 1440 , 900 , "HDR Demo" );
	if( window == NULL ){
		std::cout << "INIT::ERROR Window initialization error. Terminating." << std::endl;
		return-1;
	}

//	DemoScene demo = DemoScene();
//	BPLightsScene bplscene = BPLightsScene();
	HDRScene scene = HDRScene();
	hdrScene = &scene;
//	DeferredScene scene = DeferredScene();

	// Render scenes
	//engModules::scene1( window );
//	engModules::render( window , static_cast<Scene&>( demo ) );
//	engModules::render( window , static_cast<Scene&>( bplscene ) );
	engModules::render( window , static_cast<Scene&>( *hdrScene ) );
//	engModules::render( window , static_cast<Scene&>( scene ) );
	return 0;
}

/**
 * Implementation: Process inputs from the user
**/
void incalls::process_prolonged_inputs( GLFWwindow *window ){

	incalls::updateDeltaTime();

	// camera variable is local to the incalls namespace ( incalls::camera )
	// it's bound in the scene through incalls::bindCamera()

	// Move forward
	if( glfwGetKey( window , GLFW_KEY_W ) == GLFW_PRESS || glfwGetKey( window , GLFW_KEY_UP ) == GLFW_PRESS ){
		camera->moveBy( camera->getFront() * incalls::getCameraSpeed()  );
//		hdrScene->camPos = camera->getPosition();
	}

	// Move backward
	if( glfwGetKey( window , GLFW_KEY_S ) == GLFW_PRESS || glfwGetKey( window , GLFW_KEY_DOWN ) == GLFW_PRESS ){
		camera->moveBy( camera->getFront() * -incalls::getCameraSpeed()  );
//		hdrScene->camPos = camera->getPosition();
	}

	// Move left
	if( glfwGetKey( window , GLFW_KEY_A ) == GLFW_PRESS || glfwGetKey( window , GLFW_KEY_LEFT ) == GLFW_PRESS ){
		if( !camera->isTargetLockedOnMove() )
			camera->moveBy( glm::normalize( glm::cross( camera->getFront() , camera->getUp() ) ) * - incalls::getCameraSpeed() );
		else
			camera->orbitBy( glm::radians( incalls::getCameraOrbitSpeed() ) );
//		hdrScene->camPos = camera->getPosition();
	}

	// Move right
	if( glfwGetKey( window , GLFW_KEY_D ) == GLFW_PRESS || glfwGetKey( window , GLFW_KEY_RIGHT ) == GLFW_PRESS ){
		if( !camera->isTargetLockedOnMove() )
			camera->moveBy( glm::normalize( glm::cross( camera->getFront() , camera->getUp() ) ) * incalls::getCameraSpeed() );
		else
			camera->orbitBy( glm::radians( -incalls::getCameraOrbitSpeed() ) );
//		hdrScene->camPos = camera->getPosition();
	}

	// Move up (on z axis)
	if( glfwGetKey( window , GLFW_KEY_SPACE ) == GLFW_PRESS ){
		camera->moveBy( camera->getWorldUp() * incalls::getCameraSpeed()  );
//		hdrScene->camPos = camera->getPosition();
	}

	// Move down (on z axis)
	if( glfwGetKey( window , GLFW_KEY_C ) == GLFW_PRESS ){
		camera->moveBy( camera->getWorldUp() * -incalls::getCameraSpeed()  );
//		hdrScene->camPos = camera->getPosition();
	}

	if( hdrScene != nullptr ){
		hdrScene->camPos = camera->getPosition();
	}
}
