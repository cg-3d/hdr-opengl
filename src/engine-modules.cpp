#include <iostream>
#include <experimental/filesystem>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include "Engine/Shader.h"
#include "Engine/Image.h"
#include "Engine/Model.h"
#include "Engine/Camera.h"

#include "Engine/Scene.h"

#include "Engine/input-callbacks.h"
#include "Engine/cubemaps.h"
#include "Engine/engine-utils.h"

#include "shapes/shapes.h"
#include "utilities/utilities.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_glfw.h"
#include "imgui/imgui_impl_opengl3.h"

#include "engine-modules.h"

/**
 * Initialize a GLFW window and load OpenGL using glad.
 */
GLFWwindow* engModules::initWindow( int width , int height , std::string title ){
	// Initialize GLFW
	if( !glfwInit() ){
		std::cout << "GLFW::ERROR Failed to initialize GLFW" << std::endl;
		return NULL;
	}
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR , 3 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR , 3 );
	glfwWindowHint( GLFW_OPENGL_PROFILE , GLFW_OPENGL_CORE_PROFILE );
	//glfwWindowHint( GLFW_OPENGL_PROFILE , GLFW_OPENGL_ANY_PROFILE );
	//glfwWindowHint( GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE ); //Needed on Mac (USELESS)


	// Window Creation
	GLFWwindow* window = glfwCreateWindow( width , height , title.c_str() , NULL , NULL ); //Creates GLFW window
	if( window == NULL ) { //Check if window has been created
		std::cout << "GLFW::ERROR Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return NULL;
	}
	glfwMakeContextCurrent( window ); //Assign current content to the created window

	// GLAD initialization

	// gladLoadGLLoader() loads the OpenGL functions, it accepts as a parameter the
	// function responsible to provide OpenGL function pointers.
	//
	// Such provider function is OS-specific.
	//
	// We pass 'glfwGetProcAddress' as argument, this is a GLFW function which loads the
	// correct function pointers provider based on the the OS we're compiling for.
	if( !gladLoadGLLoader( (GLADloadproc) glfwGetProcAddress ) ) {
		std::cout << "Failed to initialize GLAD" << std::endl;
		return NULL;
	}

	// Tells OpenGL the size of the rendering window.
	// glViwePort( lowerLeftCornerX , lowerLeftCornerY , width , height )
	incalls::setScreenSize( width , height ); // Set initial screen size for callbacks
	glViewport( 0 , 0 , incalls::sceneViewWidth , incalls::sceneViewHeight );

	//Register the window resize callback
	glfwSetFramebufferSizeCallback( window , incalls::framebuffer_size_callback );

	glfwSetMouseButtonCallback( window , incalls::mouse_button_callback );

	glfwSetKeyCallback( window , incalls::key_callback );
//	glfwSetInputMode(window, GLFW_STICKY_KEYS, GLFW_TRUE);

//	glfwSetInputMode( window , GLFW_CURSOR , GLFW_CURSOR_DISABLED );

	/* IMGUI */
	//
	const char* glsl_version = "#version 330";
	//const char* glsl_version = "#version 130";
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontDefault();
	ImGui::StyleColorsDark();

	ImGui_ImplGlfw_InitForOpenGL(window, true);
	ImGui_ImplOpenGL3_Init(glsl_version);

	return window;
}

/* The following describes the render() routine:


 Create buffers

 Create and bind frame, texture, render buffers

 Scene settings:
	- Camera
  - Models
  - Shaders

 RENDERING LOOP :
 ^
 |	- Start DearImGui Frame
 | 	- Bind framebuffer and draw scene
 |
 |	- Scene window
 |		Set size and position
 |		Begin( "Scene Window" )
 |		pollEvents() and process_prolonged_inputs() <-- putting these here makes the callbacks local to the "Scene Window" context
 |		add framebuffer to the draw list
 |		End()
 |
 |	- Bind the default framebuffer ( 0 )
 |
 | 	- Controls window
 |		Set size and position
 |		Begin( "Controls Window" )
 |		Add controls and components <-- to be defined
 |		End()
 |
 |	- Render:
 |		ImGui::Render()
 |		glClear(GL_COLOR_BUFFER_BIT);
 |		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
 |		glfwSwapBuffers( window );
 |____|

 Delete Buffers

 glfwTerminate()
 -----------------------------------------

 */
void engModules::render( GLFWwindow *window , Scene &scene ){
//	incalls::bindFrameTextureBuffer( &scene.framebuffer , &scene.textureColorBuffer ,
//									 GL_RGB , GL_RGB , GL_UNSIGNED_BYTE );

	// Bind the scene' s framebuffer to be resized
	// by framebuffer_size_callback
	incalls::fbTracker().attachFramebufferTex2DColorbuffer(
			&scene.framebuffer , &scene.textureColorBuffer ,
			GL_COLOR_ATTACHMENT0 ,
			GL_RGB , GL_RGB , GL_UNSIGNED_BYTE ,
			0 , GL_LINEAR , GL_LINEAR
	);

	// RENDERING LOOP
	while( !glfwWindowShouldClose(window) ){

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		// Bind framebuffer and draw scene
		glBindFramebuffer(GL_FRAMEBUFFER, scene.framebuffer );
		scene.draw();

		// SCENE VIEW
		// Position and size
		ImGui::SetNextWindowPos( ImVec2( incalls::screenWidth - incalls::sceneViewWidth , 0 ) );
		ImGui::SetNextWindowSize(ImVec2( incalls::sceneViewWidth ,
										 incalls::sceneViewHeight ) );

		// Content
		ImGui::Begin("Scene Window");
		ImVec2 pos = ImGui::GetCursorScreenPos();

		// Inputs for the scene view
		glfwPollEvents(); // Calling pollEvents() sets every callback
						  // in the context of the "Scene Window"

		if( incalls::sceneViewActive )
			incalls::process_prolonged_inputs( window );

		ImGui::GetWindowDrawList()->AddImage(
			(ImTextureID)scene.framebuffer,
			pos,
			ImVec2( pos.x + incalls::sceneViewWidth ,
				    pos.y + incalls::sceneViewHeight ),
		    ImVec2(0, 1), ImVec2(1, 0)
		);
		ImGui::End();
		//--------------------------------------------------------

		// now bind back to default framebuffer
		glBindFramebuffer( GL_FRAMEBUFFER , 0 );

		// CONSTROLS VIEW
		// Position and size
		ImGui::SetNextWindowPos( ImVec2( 0 , 0 ) );
		ImGui::SetNextWindowSize(ImVec2( incalls::screenWidth - incalls::sceneViewWidth ,
										 incalls::screenHeight ) );

		ImGuiWindowFlags controlsWindowFlags = ImGuiWindowFlags_NoMove; // Make the controls window not draggable
		if( incalls::sceneViewActive )
			controlsWindowFlags |= ImGuiWindowFlags_NoInputs; //Disable inputs on the controls window if the scene view is active

		ImGui::Begin( "Controls Window" , NULL , controlsWindowFlags );

		//ImGui::Text( "Controls" );
		// Draw Controls
		scene.drawGUI();

		ImGui::End();

		//ImGUI RENDERING
		ImGui::Render();
//		int display_w , display_h;
//		glfwMakeContextCurrent( window );
//		glfwGetFramebufferSize( window , &display_w , &display_h );
		glClearColor( 0.098f, 0.114f, 0.122f , 1.0f );
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		/** DOUBLE BUFFERING **/
		glfwSwapBuffers( window ); // Swap the color buffer (buffer containing the color for each pixel)
								   // used to draw in this iteration (back buffer) with the rendered one (front buffer)
	}

	// Free scene's resources
	scene.freeScene();

	glfwTerminate();
}
