/*
 * utilities.h
 *
 *  Created on: May 23, 2019
 *      Author: chris
 */

#ifndef UTILITIES_UTILITIES_H_
#define UTILITIES_UTILITIES_H_

#include <iostream>
#include <glm/glm.hpp>

namespace utilities
{
	extern std::string fsBase;
	extern bool fsInitialized;

	void printVec3( glm::vec3 v );

	void initFS();

	std::string getPath( const std::string resourcePath );

	void checkGLError();
}


#endif /* UTILITIES_UTILITIES_H_ */
