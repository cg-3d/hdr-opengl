#include <glad/glad.h>

#include <glm/glm.hpp>

#include "utilities.h"

using namespace utilities;

std::string utilities::fsBase = "";
bool utilities::fsInitialized = false;

void utilities::printVec3( glm::vec3 v ){
	std::cout << "( " << v.x << " , "
			  << v.y << " , "
			  << v.z  << " )" ;
}

void utilities::initFS(){
	std::cout << "Initializing FileSystem utilities ..." << std::endl;

	char *appdir = getenv( "APPDIR" );
	if( appdir ){
		std::cout << "\tDetected APPDIR: \"" << appdir << "\""<< std::endl;
		fsBase = appdir;
		fsBase.append( "/usr/bin" );
	}else{
		fsBase = "";
		std::cout << "\tAPPDIR not set using default app path: \"" << fsBase << "\"" << std::endl;
	}

	fsInitialized = true;
}

std::string utilities::getPath( const std::string resourcePath ){
	std::string path;

	if( !fsInitialized )
		initFS();

	path = fsBase;
	path.append( fsBase.empty() ? "" : "/" )
		.append( resourcePath );

	return path;
}

void utilities::checkGLError(){

	GLenum err;
	err = glGetError();
	if( err != GL_NO_ERROR ){
		std::cout << "OpenGL error: " << err << std::endl;
	}

}
