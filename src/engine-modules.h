#ifndef ENGINE_MODULES_H_
#define ENGINE_MODULES_H_

#include <string>
#include <GLFW/glfw3.h>

#include "Engine/Scene.h"

namespace engModules{

/**
 * Initialize a GLFW window and load OpenGL using glad.
 */
GLFWwindow* initWindow( int width , int height , std::string title );

void render( GLFWwindow *window , Scene &scene );

}





#endif /* ENGINE_MODULES_H_ */
