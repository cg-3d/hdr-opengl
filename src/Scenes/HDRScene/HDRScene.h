#ifndef SCENES_HDRSCENE_HDRSCENE_H_
#define SCENES_HDRSCENE_HDRSCENE_H_

#include <map>

#include "../../Engine/Model.h"
#include "../../Engine/Lights/Lights.h"
#include "../../Engine/Scene.h"


class HDRScene : public Scene {

public:
	PointLight pLight;
	DirectionalLight dLight;

	// Material structure
//	struct Material{
//		//Shininess
//		float shininess;
//
//		// Reflectivities
//		glm::vec3 ambient;
//		glm::vec3 diffuse;
//		glm::vec3 specular;
//
//		//Textures
//		unsigned int colorMap;
//		unsigned int specularMap;
//		unsigned int normalMap;
//	} material;

	//Buffers
	// screen-filled quad
	unsigned int quadVBO;
	unsigned int quadVAO;

//	unsigned int planeVBO;
//	unsigned int planeVAO;

	// skybox
	unsigned int skyboxVBO;
	unsigned int skyboxVAO;

	// light cubes
	unsigned int lightVBO;
	unsigned int lightVAO;

	// cubes
//	unsigned int cubeVBO;
//	unsigned int cubeVAO;

	// Floating point framebuffer
	unsigned int floatFBO;
	unsigned int floatColorBuffer;
	unsigned int floatRBO;

	//Projection matrix
	glm::mat4 projection;

	//Camera
	Camera cam;
	glm::vec3 camPos; //To track position from inputs
	float camSpeed;

	// Shaders
	Shader sceneShader;
	Shader flatShader;
	Shader hdrShader;
	Shader skyboxShader;

	//HDR shader settings
	bool hdr;
	float exposure;

	// Models
	Model modelHouse;

	//Skybox
	std::vector<std::string> skyboxFaces;
	unsigned int skyboxTexture;

	// GUI
	std::map< std::string , int > toneMaps;
	char* selectedToneMap;
	char **toneMapsComboItems;

//	std::map< std::string , std::pair< std::string , std::string> > textures;
//	char *selectedTexture;
//	char **texturesComboItems;

	// Methods
	HDRScene();
	virtual ~HDRScene();
	void draw();
	void drawGUI();

	void freeGLResources();

private:
	//Shader updates
	void updateShaderPointLightProperties();
	void updateShaderDirectionalLightProperties();
	//void updateShaderMaterialProperties();

	// Gui lights settings
	void pointLightSettings();
	void directionalLightSettings();

	//void materialSettings();

};




#endif /* SCENES_HDRSCENE_HDRSCENE_H_ */
