
#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <iterator>

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include <cmath>

#include "../../imgui/imgui.h"
#include "../../imgui/imgui_impl_glfw.h"
#include "../../imgui/imgui_impl_opengl3.h"
#include "../../imgui/toggle_button.h"

#include "../../utilities/utilities.h"
#include "../../Engine/engine-utils.h"

#include "../../Engine/Camera.h"
#include "../../Engine/Shader.h"
#include "../../Engine/input-callbacks.h"

#include "../../shapes/shapes.h"
#include "../../Engine/Scene.h"

#include  "../../Engine/Lights/Lights.h"

#include "../../Engine/cubemaps.h"

#include "HDRScene.h"

HDRScene::HDRScene() :
	//Camera
	cam( glm::vec3( 55.0f , 0.0f , 1.5f ) ,
		 glm::vec3( 0.0f , 0.0f , 0.0f ) ,
		 glm::vec3( 0.0f , 1.0f , 0.0f ) ) ,

	// Shaders
	sceneShader( utilities::getPath( "src/Scenes/HDRScene/shaders/scene.vs" ).c_str() ,
			     utilities::getPath( "src/Scenes/HDRScene/shaders/scene.fs" ).c_str() ) ,

    flatShader( utilities::getPath( "src/Scenes/HDRScene/shaders/vertex.vs" ).c_str() ,
			    utilities::getPath( "src/Scenes/HDRScene/shaders/flat.fs" ).c_str() ) ,

	hdrShader( utilities::getPath( "src/Scenes/HDRScene/shaders/hdr.vs" ).c_str() ,
			   utilities::getPath( "src/Scenes/HDRScene/shaders/hdr.fs" ).c_str()) ,

	skyboxShader( utilities::getPath( "src/Scenes/HDRScene/shaders/cubemap.vs" ).c_str() ,
			   	  utilities::getPath( "src/Scenes/HDRScene/shaders/cubemap.fs" ).c_str()) ,

	// Model
    modelHouse( utilities::getPath( "models/church/church.obj" ).c_str() , false )
{
	//Frambuffers
	// Floating point FBO
	engUtils::createFrameTextureRenderBuffer( &floatFBO , &floatColorBuffer , &floatRBO ,
											  incalls::sceneViewWidth , incalls::sceneViewHeight ,
											  GL_RGBA16F , GL_RGBA , GL_FLOAT ,
											  GL_DEPTH24_STENCIL8 , GL_DEPTH_STENCIL_ATTACHMENT );

	// Bind the float framebuffer to be updated on window resize
	incalls::fbTracker().attachFramebufferTex2DColorbufferRenderbuffer(
			&floatFBO , &floatColorBuffer ,
		    GL_COLOR_ATTACHMENT0 , GL_RGBA16F , GL_RGBA , GL_FLOAT , 0 , GL_LINEAR , GL_LINEAR ,
			&floatRBO , GL_DEPTH_STENCIL_ATTACHMENT , GL_DEPTH24_STENCIL8 );

	//-------------------------------------------------

	//Quad
	quadVBO = Scene::newVBO();
	quadVAO = Scene::newVAO();

	float quadVertices[] = {
		// positions        // texture Coords
		-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
		 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
		 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
	};
	// setup plane VAO
	glBindVertexArray(quadVAO);
	glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));

	//---------------------------------------------------

	// Projection
	projection = glm::perspective( glm::radians( 45.0f ) ,
							       incalls::sceneViewWidth / (float)incalls::sceneViewHeight ,
								   0.1f , 500.0f );

	incalls::bindProjectionMatrix( &projection );
	incalls::bindProjectionUniform( &sceneShader , "projection" );
	incalls::bindProjectionUniform( &flatShader , "projection" );
	sceneShader.setMatrix4fv( "projection" , projection );
	flatShader.setMatrix4fv( "projection" , projection );
	//---------------------------------------------------

	//Camera
	camPos = cam.getPosition();
	cam.setTargetLockedOnMove( false );
	incalls::bindCamera( &cam );
	camSpeed = 10.0f;
	incalls::setCameraSpeed( camSpeed );
	incalls::setCameraOrbitSpeed( 40.0f );
	//---------------------------------------------------

	glClearColor( 0.098f, 0.114f, 0.122f , 1.0f );

	// Vertex buffers
//	planeVBO = Scene::newVBO();
//	glBindBuffer( GL_ARRAY_BUFFER , planeVBO );
//	glBufferData( GL_ARRAY_BUFFER , sizeof( plane::vertNormTex ) , plane::vertNormTex , GL_STATIC_DRAW );
//
//	planeVAO = Scene::newVAO();
//	glBindBuffer( GL_ARRAY_BUFFER , planeVBO );
//	glBindVertexArray( planeVAO );
//	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 8 * sizeof( float ) , (void *)0 );
//	glVertexAttribPointer( 1 , 3 , GL_FLOAT , GL_FALSE , 8 * sizeof( float ) , (void *)( 3 * sizeof( float ) ) );
//	glVertexAttribPointer( 2 , 2 , GL_FLOAT , GL_FALSE , 8 * sizeof( float ) , (void *)( 6 * sizeof( float ) ) );
//	glEnableVertexAttribArray( 0 );
//	glEnableVertexAttribArray( 1 );
//	glEnableVertexAttribArray( 2 );

	lightVBO = Scene::newVBO();
	glBindBuffer( GL_ARRAY_BUFFER , lightVBO );
	glBufferData( GL_ARRAY_BUFFER , sizeof( cube::vertexPositions ) , cube::vertexPositions , GL_STATIC_DRAW );

	lightVAO = Scene::newVAO();
	glBindBuffer( GL_ARRAY_BUFFER , lightVBO );
	glBindVertexArray( lightVAO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 3 * sizeof( float ) , (void *)0 );
	glEnableVertexAttribArray( 0 );

//	// Optional
//	// Flip cubes to face inside the cube
//	float *tunnel = (float *) malloc( sizeof( cube::fullVertices ) );
//	for( int i = 0 ; i < 8 * 36 ; ++i ){
//		tunnel[i] = cube::fullVertices[i];
//		if( i % 8 == 3 || i % 8 == 4 || i % 8 == 5 )
//			tunnel[i] *= -1.0f;
//	}
//
//	// Calculate vertices tangent space
////	float *cubeTangent = cubelWithTangentSpace( tunnel );
//	float *cubeTangent = cube::withTangentsAndBitangents( tunnel );
//	float cubeTangentSize = 6 * 2 * 3 * 14 * sizeof( float );
//
//	cubeVBO = Scene::newVBO();
//	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
//	glBufferData( GL_ARRAY_BUFFER , cubeTangentSize , cubeTangent , GL_STATIC_DRAW );
//
//	cubeVAO = Scene::newVAO();
//	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
//	glBindVertexArray( cubeVAO );
//	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)(0) );
//	glVertexAttribPointer( 1 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 3 * sizeof( float ) ) );
//	glVertexAttribPointer( 2 , 2 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 6 * sizeof( float ) ) );
//	glVertexAttribPointer( 3 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 8 * sizeof( float ) ) );
//	glVertexAttribPointer( 4 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 11 * sizeof( float ) ) );
//	glEnableVertexAttribArray( 0 ); // position
//	glEnableVertexAttribArray( 1 ); // normal
//	glEnableVertexAttribArray( 2 ); // texture coords
//	glEnableVertexAttribArray( 3 ); // tangent
//	glEnableVertexAttribArray( 4 ); // bitangent
//
//	delete tunnel;
//	delete cubeTangent;
	//---------------------------------------------------

//	textures = std::map< std::string , std::pair< std::string , std::string> >();
//	textures.insert( std::pair< std::string , std::pair< std::string , std::string> >(
//			"abstract" ,
//			std::pair<std::string , std::string>( "textures/abstract_blue/abstract" , ".jpg" ) ) );
//
//	textures.insert( std::pair< std::string , std::pair< std::string , std::string> >(
//		    "lava" ,
//			std::pair<std::string , std::string>( "textures/lava/lava" , ".png" ) ) );
//
//	textures.insert( std::pair< std::string , std::pair< std::string , std::string> >(
//			"ice" ,
//			std::pair<std::string , std::string>( "textures/ice/ice" , ".jpg" ) ) );
//	textures.insert( std::pair< std::string , std::pair< std::string , std::string> >(
//			"marble colored" ,
//			std::pair<std::string , std::string>( "textures/marble_colored/marble_colored" , ".png" ) ) );
//	textures.insert( std::pair< std::string , std::pair< std::string , std::string> >(
//			"marble blue" ,
//			std::pair<std::string , std::string>( "textures/marble_blue/marble_blue" , ".jpg" ) ) );
//	textures.insert( std::pair< std::string , std::pair< std::string , std::string> >(
//			"stone wall" ,
//			std::pair<std::string , std::string>( "textures/stone_wall/stone_wall" , ".jpg" ) ) );

//	texturesComboItems = (char **) malloc( textures.size() * sizeof( char * ) );
//	std::map< std::string , std::pair< std::string , std::string> >::iterator texIt;
//	int i = 0;
//	for( texIt = textures.begin() ; texIt != textures.end() ; ++texIt , ++i ){
//		texturesComboItems[i] = (char *)( texIt->first.c_str() );
//		if( texIt->first == "abstract" )
//			selectedTexture = texturesComboItems[i];
//	}

	// Scene shader
	//
	//Material
//	glActiveTexture( GL_TEXTURE1 );
//	material.colorMap = engUtils::textureFromFile( utilities::getPath( "textures/abstract_blue/abstract_color.jpg" ).c_str() , true );
//	sceneShader.setInt( "texture_diffuse1" , 1 );

//	std::cout << "Color Map Texture: " << material.colorMap << std::endl;
//
//	glActiveTexture( GL_TEXTURE2 );
//	material.specularMap = engUtils::textureFromFile( utilities::getPath( "textures/abstract_blue/abstract_spec.jpg" ).c_str() , false );
//	//sceneShader.setInt( "texture_specular1" , 2 );
//	std::cout << "Specular Map Texture: " << material.specularMap << std::endl;
//
//	glActiveTexture( GL_TEXTURE3 );
//	material.normalMap = engUtils::textureFromFile( utilities::getPath( "textures/abstract_blue/abstract_norm.jpg" ).c_str() , false );
//	//sceneShader.setInt( "texture_normal1" , 3 );
//	std::cout << "Normal Map Texture: " << material.normalMap << std::endl;

	utilities::checkGLError();

//	material.ambient = glm::vec3( 1.0f );
//	material.diffuse = glm::vec3( 1.0f );
//	material.specular = glm::vec3( 1.0f );
//
//	material.shininess = 1.0f;
//	sceneShader.setFloat( "shininess" , material.shininess );

	// Point light
	pLight = PointLight( glm::vec3( 17.5f , -7.5f , 8.5f ) ,
			 	 	 	 1.0f , 1.0f , 10.0f ,
						 glm::vec3( 0.05f ) ,
						 glm::vec3( 1.0f ) ,
						 glm::vec3( 1.0f ) );
	pLight.intensity = 100.0f;
	pLight.setShaderUniform( sceneShader , "pointLight" );
	sceneShader.setVec3( "pointLightPos" , pLight.position );
	//---------------------------------------------------

	// Directional light
	dLight = DirectionalLight( glm::vec3( -1.0f , -1.0f , -1.0f ) ,
							   glm::vec3( 0.05f ) ,
							   glm::vec3( 0.5f ) ,
							   glm::vec3( 1.0f ) );
	dLight.intensity = 10.0f;
	dLight.setShaderUniform( sceneShader , "dirLight" );
	sceneShader.setVec3( "directionalLightDir" , dLight.direction );
	//---------------------------------------------------

	// Flat/Debug shader
	flatShader.setVec3( "color" , pLight.specular );
	//---------------------------------------------------

	// HDR shader
	hdr = true;
	exposure = 1.0f;
	toneMaps = std::map< std::string , int >();
	toneMaps.insert( std::pair<std::string , int>( "Reinhard" , 1 ) );
	toneMaps.insert( std::pair<std::string , int>( "Exposure" , 2 ) );

	hdrShader.setInt( "hdr" , hdr );
	hdrShader.setFloat( "exposure" , exposure );
	hdrShader.setInt( "selectedToneMap" , 1 );

	toneMapsComboItems = (char **) malloc( toneMaps.size() * sizeof( char* ) );
	std::map< std::string , int>::iterator tmIt;
	int i = 0;
	for( tmIt = toneMaps.begin() ; tmIt != toneMaps.end() ; ++tmIt , ++i ){
		toneMapsComboItems[i] = (char *)( tmIt->first.c_str() );
		if( tmIt->second == 1 )
			selectedToneMap = toneMapsComboItems[i];
	}
	//---------------------------------------------------

	// Skybox
	skyboxVBO = Scene::newVBO();
	glBindBuffer( GL_ARRAY_BUFFER , skyboxVBO );
	glBufferData( GL_ARRAY_BUFFER , sizeof( cubemaps::skyboxVertices ) ,
				  cubemaps::skyboxVertices , GL_STATIC_DRAW );

	skyboxVAO = Scene::newVAO();
	glBindVertexArray( skyboxVAO );
	glBindBuffer( GL_ARRAY_BUFFER , skyboxVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE ,
						   3 * sizeof ( float ) , (void*) 0 );
	glEnableVertexAttribArray( 0 );

	skyboxShader.setMatrix4fv( "projection" , projection );
	incalls::bindProjectionUniform( &skyboxShader , "projection" );
	skyboxFaces = {
		utilities::getPath( "skybox/default/right.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/left.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/top.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/bottom.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/front.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/back.jpg" ).c_str()
	};
	skyboxTexture = cubemaps::loadCubemap( skyboxFaces );
	skyboxShader.setInt( "cubemap" , 0 ); // Set skybox shader sampler
	//---------------------------------------------------

// modelHouse.printModel();

}

/**
 * Free scene's openGL resources
 */
void HDRScene::freeGLResources(){
	// Delete the float frambuffer and its attchments
	glDeleteRenderbuffers( 1 , &floatRBO );
	glDeleteTextures( 1 , &floatColorBuffer );
	glDeleteFramebuffers( 1 , &floatFBO );
}

/**
 * Scene destructor
 *
 * delete dynamically allocated objects
 */
HDRScene::~HDRScene(){
	// Free combo box items
	delete toneMapsComboItems;
}

/**
 * Conveniency to update the point light properties
 * in the scene's shader
 */
void HDRScene::updateShaderPointLightProperties(){

	// Combined color + intensity
	pLight.setShaderUniform( sceneShader , "pointLight" );
	sceneShader.setVec3( "pointLightPos" , pLight.position );
}

void HDRScene::updateShaderDirectionalLightProperties(){
	dLight.setShaderUniform( sceneShader , "dirLight" );
	sceneShader.setVec3( "directionalLightDir" , dLight.direction );
}


/*
void HDRScene::updateShaderMaterialProperties(){

	// Shininess
//	sceneShader.setFloat( "shininess" , material.shininess );

	// Combined color + intensity
//	sceneShader.setVec3( "pointLight.core.ambient" ,
//						  pointLight.intensity * material.ambient * pointLight.ambient );
//	sceneShader.setVec3( "pointLight.core.diffuse" ,
//						  pointLight.intensity * material.diffuse * pointLight.diffuse );
//	sceneShader.setVec3( "pointLight.core.specular" ,
//						  pointLight.intensity * material.specular * pointLight.specular );
}
*/

/**
 * Draw method implementation
 */
void HDRScene::draw(){

	glEnable( GL_DEPTH_TEST );

	// Draw to the float framebuffer
	glBindFramebuffer( GL_FRAMEBUFFER , floatFBO );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		glm::mat4 model = glm::mat4( 1.0f );
		//model = glm::translate( model , glm::vec3( -2.0f , 0.0f , 0.0f ) );
		//model = glm::scale( model , glm::vec3( 40.0f ) );
		//model = glm::rotate( model , glm::radians( 180.0f ) ,  glm::vec3( 0.0f , 0.0f , 1.0f ) );
		sceneShader.setMatrix4fv( "model" , model );
		sceneShader.setMatrix3fv( "normalMatrix" , glm::mat3( glm::transpose( glm::inverse( model ) ) ) );
		sceneShader.setMatrix4fv( "view" , cam.getViewMatrix() );

//		sceneShader.setVec3( "ViewPos" , cam.getPosition() );
		sceneShader.setVec3( "viewPos" , cam.getPosition() ); // for the vertex
		//sceneShader.setVec3( "lightPos" , pointLight.position ); // for the vertex
		//sceneShader.setVec3( "lightPos" , pLight.position ); // for the vertex

//		glActiveTexture( GL_TEXTURE1 );
//		glBindTexture( GL_TEXTURE_2D , material.colorMap );
//
//		glActiveTexture( GL_TEXTURE2 );
//		glBindTexture( GL_TEXTURE_2D , material.specularMap );
//
//		glActiveTexture( GL_TEXTURE3 );
//		glBindTexture( GL_TEXTURE_2D , material.normalMap );

	//	glBindVertexArray( planeVAO );
	//	glDrawArrays( GL_TRIANGLES , 0 , 6 );

//		glBindVertexArray( cubeVAO );
//		glDrawArrays( GL_TRIANGLES , 0 , 36 );
		modelHouse.draw( sceneShader );

		//Lights
		model = glm::mat4( 1.0f );
//		model = glm::translate( model , pointLight.position );
		model = glm::translate( model , pLight.position );
		model = glm::scale( model , glm::vec3( 0.2f ) );
		flatShader.setMatrix4fv( "model" , model );
		flatShader.setMatrix4fv( "view" , cam.getViewMatrix() );
		glBindVertexArray( lightVAO );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );

	// Skybox drawing
	glBindFramebuffer( GL_FRAMEBUFFER , floatFBO );
		glDepthMask( GL_FALSE );
		skyboxShader.use();
		// Removes translation components from view matrix for the skybox
		skyboxShader.setMatrix4fv( "view" , glm::mat4( glm::mat3( cam.getViewMatrix() ) ) );

		glDepthFunc( GL_LEQUAL );
		glBindVertexArray( skyboxVAO );
		glBindTexture( GL_TEXTURE_CUBE_MAP , skyboxTexture );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );

		// Restore to default
		glDepthMask( GL_TRUE );
		glDepthFunc( GL_LESS );

	// Draw to the scene's framebuffer applying tonemapping
	// to the color values in the float colorbuffer.
	glBindFramebuffer( GL_FRAMEBUFFER , this->framebuffer );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		hdrShader.use();
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D , floatColorBuffer );
		hdrShader.setInt( "floatBuffer" , 0 );
		glBindVertexArray( quadVAO );
		glDrawArrays( GL_TRIANGLE_STRIP , 0 , 4 );


	glBindVertexArray( 0 );
}

/**
 * DrawGUI implementation
 */
void HDRScene::drawGUI(){

	ImGui::Text( "FPS: %.1f" , ImGui::GetIO().Framerate );

  //Camera
  if( ImGui::CollapsingHeader( "Camera" ) ){
	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text("Position");
	  if( ImGui::InputFloat("cam.x", &camPos.x, 0.5f, 1.0f, "%.2f") ) {
		cam.moveTo( camPos );
	  	sceneShader.setVec3( "ViewPos" , cam.getPosition() );
	  }
	  if( ImGui::InputFloat("cam.y", &camPos.y, 0.5f, 1.0f, "%.2f") ) {
		cam.moveTo( camPos );
		sceneShader.setVec3( "ViewPos" , cam.getPosition() );
	  }
	  if( ImGui::InputFloat("cam.z", &camPos.z, 0.5f, 1.0f, "%.2f") ) {
		cam.moveTo( camPos );
		sceneShader.setVec3( "ViewPos" , cam.getPosition() );
	  }

	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text("Speed");
	  if( ImGui::InputFloat("cam.speed", &camSpeed, 0.5f, 1.0f, "%.1f") ) {
		camSpeed = max( camSpeed , 0.0f );
		incalls::setCameraSpeed( camSpeed  );
	  }

	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text( "Target: " );
	  std::stringstream ss;
	  ss << "pitch: " << cam.getPitch();
	  ImGui::Text( ss.str().c_str() );
	  ImGui::SameLine();
	  ss.str( std::string() );
	  ss << "yaw: " << cam.getYaw();
	  ImGui::Text( ss.str().c_str() );
  }

  ImGui::Dummy(ImVec2(0.0f, 5.0f));
  ImGui::Separator();
  ImGui::Dummy(ImVec2(0.0f, 5.0f));

  // HDR
  if( ImGui::CollapsingHeader( "HDR settings" ) ){
	  ImGui::Dummy(ImVec2(0.0f, 5.0f));
	  if( ToggleButton("##hdr", &hdr) ) {
		hdr = !hdr;
		hdrShader.setInt( "hdr" , hdr );
		std::cout << "HDR: " << hdr << std::endl;
	  }
	  ImGui::SameLine();
	  ImGui::Text("HDR");

	  ImGui::Dummy(ImVec2(0.0f, 5.0f));

	  // Combo box
	  //const char* comboItems[] = { "AAAA", "BBBB", "CCCC" };
	  char* current_item = selectedToneMap;

	  ImGui::Text("Tone-Mapping:");
	  if (ImGui::BeginCombo("##comboToneMap", current_item)) {
		  for (unsigned int i = 0; i < toneMaps.size() ; i++) {
			  bool is_selected = (current_item == toneMapsComboItems[i]);
			  if (ImGui::Selectable(toneMapsComboItems[i], is_selected)){
				current_item = toneMapsComboItems[i];
				selectedToneMap = current_item;

				hdrShader.setInt( "selectedToneMap" , toneMaps[std::string(current_item)] );
				std::cout << "Selectable: " << toneMapsComboItems[i] << std::endl;
			  }

			  if (is_selected){
			    ImGui::SetItemDefaultFocus();
			  }
		  }
		  ImGui::EndCombo();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text("exposure");
	  if( ImGui::InputFloat("exposure", &exposure, 0.001f, 0.1f, "%.4f") ) {
		//pointLightPos = glm::vec3( pointLightPos.x, pointLightPos.y , pointLightPos.z );
		exposure = std::max( exposure , 0.0001f );
		hdrShader.setFloat( "exposure" , exposure );
	  }
  }

  ImGui::Dummy(ImVec2(0.0f, 5.0f));
  ImGui::Separator();
  ImGui::Dummy(ImVec2(0.0f, 5.0f));

  //Point light settings
  pointLightSettings();

  ImGui::Dummy(ImVec2(0.0f, 5.0f));
  ImGui::Separator();
  ImGui::Dummy(ImVec2(0.0f, 5.0f));

  //Directional light settings
  directionalLightSettings();

  ImGui::Dummy(ImVec2(0.0f, 5.0f));
  ImGui::Separator();

}

void HDRScene::pointLightSettings(){
  //Light settings
  //--------------------------------------------------------------
  // Light position
  if( ImGui::CollapsingHeader( "Light" ) ){
	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text("Light position");
	  if( ImGui::InputFloat("l.x", &pLight.position.x, 0.5f, 1.0f, "%.2f") ) {
		sceneShader.setVec3( "pointLight.position" , pLight.position );
		updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.y", &pLight.position.y, 0.5f, 1.0f, "%.2f") ) {
		sceneShader.setVec3( "pointLight.position" , pLight.position );
		updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.z", &pLight.position.z, 0.5f, 1.0f, "%.2f") ) {
		sceneShader.setVec3( "pointLight.position" , pLight.position );
		updateShaderPointLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 5.0f));

	  // Light intensity
	  ImGui::Text("Light intensity");
	  if( ImGui::InputFloat("l.intensity", &pLight.intensity, 0.5f, 1.0f, "%.2f") ) {
		if( pLight.intensity < 0.0f ){
			pLight.intensity = 0.0f;
		}else{
			updateShaderPointLightProperties();
		}
	  }

	  ImGui::Dummy(ImVec2(0.0f, 5.0f));

	  //Light Contributions
	  ImGui::Text("Light contributions");
	  ImGui::Dummy(ImVec2(0.0f, 2.0f));
	  // Ambient
	  ImGui::Text("ambient");
	  if( ImGui::InputFloat("l.a.R", &pLight.ambient.x, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.ambient.x < 0.0f ) pLight.ambient.x = 0.0f;
		else if( pLight.ambient.x > 1.0f ) pLight.ambient.x = 1.0f;
		else updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.a.G", &pLight.ambient.y, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.ambient.y < 0.0f ) pLight.ambient.y = 0.0f;
		else if( pLight.ambient.y > 1.0f ) pLight.ambient.y = 1.0f;
		else updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.a.B", &pLight.ambient.z, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.ambient.z < 0.0f ) pLight.ambient.z = 0.0f;
		else if( pLight.ambient.z > 1.0f ) pLight.ambient.z = 1.0f;
		else updateShaderPointLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 2.0f));
	  // Diffuse
	  ImGui::Text("diffuse");
	  if( ImGui::InputFloat("l.d.R", &pLight.diffuse.x, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.diffuse.x < 0.0f ) pLight.diffuse.x = 0.0f;
		else if( pLight.diffuse.x > 1.0f ) pLight.diffuse.x = 1.0f;
		else updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.d.G", &pLight.diffuse.y, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.diffuse.y < 0.0f ) pLight.diffuse.y = 0.0f;
		else if( pLight.diffuse.y > 1.0f ) pLight.diffuse.y = 1.0f;
		else updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.d.B", &pLight.diffuse.z, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.diffuse.z < 0.0f ) pLight.diffuse.z = 0.0f;
		else if( pLight.diffuse.z > 1.0f ) pLight.diffuse.z = 1.0f;
		else updateShaderPointLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 2.0f));
	  // Specular
	  ImGui::Text("specular");
	  if( ImGui::InputFloat("l.s.R", &pLight.specular.x, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.specular.x < 0.0f ) pLight.specular.x = 0.0f;
		else if( pLight.specular.x > 1.0f ) pLight.specular.x = 1.0f;
		else updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.s.G", &pLight.specular.y, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.specular.y < 0.0f ) pLight.specular.y = 0.0f;
		else if( pLight.specular.y > 1.0f ) pLight.specular.y = 1.0f;
		else updateShaderPointLightProperties();
	  }
	  if( ImGui::InputFloat("l.s.B", &pLight.specular.z, 0.001f, 0.01f, "%.5f") ) {
		if( pLight.specular.z < 0.0f ) pLight.specular.z = 0.0f;
		else if( pLight.specular.z > 1.0f ) pLight.specular.z = 1.0f;
		else updateShaderPointLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 5.0f));

	  //Attenuation
	  ImGui::Text("Light attenuation");
	  if( ImGui::InputFloat("l.att.c", &pLight.c, 0.1f, 1.0f, "%.5f") ) {
		updateShaderPointLightProperties();
	  }

	  if( ImGui::InputFloat("l.att.l", &pLight.l, 0.1f, 1.0f, "%.5f") ) {
		updateShaderPointLightProperties();
	  }

	  if( ImGui::InputFloat("l.att.q", &pLight.q, 0.1f, 1.0f, "%.5f") ) {
		updateShaderPointLightProperties();
	  }
  }

}

void HDRScene::directionalLightSettings(){
  //Light settings
  //--------------------------------------------------------------
  // Light position
  if( ImGui::CollapsingHeader( "Directional Light" ) ){
	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text("Light direction");
	  if( ImGui::InputFloat("dl.x", &dLight.direction.x, 0.5f, 1.0f, "%.2f") ) {
		updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.y", &dLight.direction.y, 0.5f, 1.0f, "%.2f") ) {
		updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.z", &dLight.direction.z, 0.5f, 1.0f, "%.2f") ) {
		updateShaderDirectionalLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 5.0f));

	  // Light intensity
	  ImGui::Text("Light intensity");
	  if( ImGui::InputFloat("dl.intensity", &dLight.intensity, 0.5f, 1.0f, "%.2f") ) {
		if( dLight.intensity < 0.0f ){
			dLight.intensity = 0.0f;
		}else{
			updateShaderDirectionalLightProperties();
		}
	  }

	  ImGui::Dummy(ImVec2(0.0f, 5.0f));

	  //Light Contributions
	  ImGui::Text("Light contributions");
	  ImGui::Dummy(ImVec2(0.0f, 2.0f));
	  // Ambient
	  ImGui::Text("ambient");
	  if( ImGui::InputFloat("dl.a.R", &dLight.ambient.x, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.ambient.x < 0.0f ) dLight.ambient.x = 0.0f;
		else if( dLight.ambient.x > 1.0f ) dLight.ambient.x = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.a.G", &dLight.ambient.y, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.ambient.y < 0.0f ) dLight.ambient.y = 0.0f;
		else if( dLight.ambient.y > 1.0f ) dLight.ambient.y = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.a.B", &dLight.ambient.z, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.ambient.z < 0.0f ) dLight.ambient.z = 0.0f;
		else if( dLight.ambient.z > 1.0f ) dLight.ambient.z = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 2.0f));
	  // Diffuse
	  ImGui::Text("diffuse");
	  if( ImGui::InputFloat("dl.d.R", &dLight.diffuse.x, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.diffuse.x < 0.0f ) dLight.diffuse.x = 0.0f;
		else if( dLight.diffuse.x > 1.0f ) dLight.diffuse.x = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.d.G", &dLight.diffuse.y, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.diffuse.y < 0.0f ) dLight.diffuse.y = 0.0f;
		else if( dLight.diffuse.y > 1.0f ) dLight.diffuse.y = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.d.B", &dLight.diffuse.z, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.diffuse.z < 0.0f ) dLight.diffuse.z = 0.0f;
		else if( dLight.diffuse.z > 1.0f ) dLight.diffuse.z = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 2.0f));
	  // Specular
	  ImGui::Text("specular");
	  if( ImGui::InputFloat("dl.s.R", &dLight.specular.x, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.specular.x < 0.0f ) dLight.specular.x = 0.0f;
		else if( dLight.specular.x > 1.0f ) dLight.specular.x = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.s.G", &dLight.specular.y, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.specular.y < 0.0f ) dLight.specular.y = 0.0f;
		else if( dLight.specular.y > 1.0f ) dLight.specular.y = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
	  if( ImGui::InputFloat("dl.s.B", &dLight.specular.z, 0.001f, 0.01f, "%.5f") ) {
		if( dLight.specular.z < 0.0f ) dLight.specular.z = 0.0f;
		else if( dLight.specular.z > 1.0f ) dLight.specular.z = 1.0f;
		else updateShaderDirectionalLightProperties();
	  }
  }

}

/*
void HDRScene::materialSettings(){

  ImGui::Dummy(ImVec2(0.0f, 5.0f));
  ImGui::Separator();
  ImGui::Dummy(ImVec2(0.0f, 5.0f));

  //Material settings
  //--------------------------------------------------------------
  if( ImGui::CollapsingHeader( "Material" ) ){
	  ImGui::Dummy(ImVec2(0.0f, 3.0f));

	  // Combo box
	  //const char* comboItems[] = { "AAAA", "BBBB", "CCCC" };
	  char* current_item = selectedTexture;

	  ImGui::Text("Texture:");
	  if (ImGui::BeginCombo("##comboTexture", current_item)) {
		  for (unsigned int i = 0; i < textures.size() ; i++) {
			  bool is_selected = (current_item == texturesComboItems[i]);
			  if (ImGui::Selectable(texturesComboItems[i], is_selected)){
				current_item = texturesComboItems[i];
				selectedTexture = current_item;

				//glActiveTexture( GL_TEXTURE1 );
				glDeleteTextures( 1 , &material.colorMap );
				glDeleteTextures( 1 , &material.specularMap );
				glDeleteTextures( 1 , &material.normalMap );

				glActiveTexture( GL_TEXTURE1 );
				material.colorMap = engUtils::textureFromFile(
										utilities::getPath( textures[current_item].first + "_color" + textures[current_item].second ).c_str() ,
										true );
				glActiveTexture( GL_TEXTURE2 );
				material.specularMap = engUtils::textureFromFile(
										utilities::getPath( textures[current_item].first + "_spec" + textures[current_item].second ).c_str() ,
										false );
				glActiveTexture( GL_TEXTURE3 );
				material.normalMap = engUtils::textureFromFile(
										utilities::getPath( textures[current_item].first + "_norm" + textures[current_item].second ).c_str() ,
										false );


				std::cout << "Selected texture: " << texturesComboItems[i] << std::endl;
			  }

			  if (is_selected){
				ImGui::SetItemDefaultFocus();
			  }
		  }
		  ImGui::EndCombo();
	  }

	  ImGui::Dummy(ImVec2(0.0f, 3.0f));

	  // Shininess
	  ImGui::Text("shininess");
	  if (ImGui::SliderFloat("##sliderShininess", &material.shininess, 1.0f, 500.0f, "%.1f")) {
		// slider widget auto-update variable value
		sceneShader.setFloat( "shininess" , material.shininess );
	  }

	  // Reflectivities
	  //
	  // Ambient
	  ImGui::Dummy(ImVec2(0.0f, 5.0f));
	  ImGui::Text( "ambient" );
	  ImGui::SameLine();
	  if( ImGui::Button( "-##m.a" , ImVec2( 20 , 20 ) ) ){
		  //clamp components in [0.0 - 1.0]
		  material.ambient.x = std::min( std::max( material.ambient.x - 0.1f , 0.0f ) , 1.0f );
		  material.ambient.y = std::min( std::max( material.ambient.y - 0.1f , 0.0f ) , 1.0f );
		  material.ambient.z = std::min( std::max( material.ambient.z - 0.1f , 0.0f ) , 1.0f );
	  }
	  ImGui::SameLine();
	  if( ImGui::Button( "+##m.a" , ImVec2( 20 , 20 ) ) ){
		  //clamp components in [0.0 - 1.0]
		  material.ambient.x = std::min( std::max( material.ambient.x + 0.1f , 0.0f ) , 1.0f );
		  material.ambient.y = std::min( std::max( material.ambient.y + 0.1f , 0.0f ) , 1.0f );
		  material.ambient.z = std::min( std::max( material.ambient.z + 0.1f , 0.0f ) , 1.0f );
	  }
	  if( ImGui::InputFloat("m.a.R", &material.ambient.x, 0.001f, 0.01f, "%.5f") ) {
		if( material.ambient.x < 0.0f ) material.ambient.x = 0.0f;
		else if( material.ambient.x > 1.0f ) material.ambient.x = 1.0f;
		else updateShaderMaterialProperties();
	  }
	  if( ImGui::InputFloat("m.a.G", &material.ambient.y, 0.001f, 0.01f, "%.5f") ) {
		if( material.ambient.y < 0.0f ) material.ambient.y = 0.0f;
		else if( material.ambient.y > 1.0f ) material.ambient.y = 1.0f;
		else updateShaderMaterialProperties();
	  }
	  if( ImGui::InputFloat("m.a.B", &material.ambient.z, 0.001f, 0.01f, "%.5f") ) {
		if( material.ambient.z < 0.0f ) material.ambient.z = 0.0f;
		else if( material.ambient.z > 1.0f ) material.ambient.z = 1.0f;
		else updateShaderMaterialProperties();
	  }

	  // Diffuse
	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text( "diffuse" );
	  ImGui::SameLine();
	  if( ImGui::Button( "-##m.d" , ImVec2( 20 , 20 ) ) ){
	   //clamp components in [0.0 - 1.0]
		material.diffuse.x = std::min( std::max( material.diffuse.x - 0.1f , 0.0f ) , 1.0f );
		material.diffuse.y = std::min( std::max( material.diffuse.y - 0.1f , 0.0f ) , 1.0f );
		material.diffuse.z = std::min( std::max( material.diffuse.z - 0.1f , 0.0f ) , 1.0f );
	  }
	  ImGui::SameLine();
	  if( ImGui::Button( "+##m.d" , ImVec2( 20 , 20 ) ) ){
		//clamp components in [0.0 - 1.0]
		material.diffuse.x = std::min( std::max( material.diffuse.x + 0.1f , 0.0f ) , 1.0f );
		material.diffuse.y = std::min( std::max( material.diffuse.y + 0.1f , 0.0f ) , 1.0f );
		material.diffuse.z = std::min( std::max( material.diffuse.z + 0.1f , 0.0f ) , 1.0f );
	  }
	  if( ImGui::InputFloat("m.d.R", &material.diffuse.x, 0.001f, 0.01f, "%.5f") ) {
		if( material.diffuse.x < 0.0f ) material.diffuse.x = 0.0f;
		else if( material.diffuse.x > 1.0f ) material.diffuse.x = 1.0f;
		else updateShaderMaterialProperties();
	  }
	  if( ImGui::InputFloat("m.d.G", &material.diffuse.y, 0.001f, 0.01f, "%.5f") ) {
		if( material.diffuse.y < 0.0f ) material.diffuse.y = 0.0f;
		else if( material.diffuse.y > 1.0f ) material.diffuse.y = 1.0f;
		else updateShaderMaterialProperties();
	  }
	  if( ImGui::InputFloat("m.d.B", &material.diffuse.z, 0.001f, 0.01f, "%.5f") ) {
		if( material.diffuse.z < 0.0f ) material.diffuse.z = 0.0f;
		else if( material.diffuse.z > 1.0f ) material.diffuse.z = 1.0f;
		else updateShaderMaterialProperties();
	  }

	  // Specular
	  ImGui::Dummy(ImVec2(0.0f, 3.0f));
	  ImGui::Text( "specular" );
	  ImGui::SameLine();
	  if( ImGui::Button( "-##m.s" , ImVec2( 20 , 20 ) ) ){
		//clamp components in [0.0 - 1.0]
		material.specular.x = std::min( std::max( material.specular.x - 0.1f , 0.0f ) , 1.0f );
		material.specular.y = std::min( std::max( material.specular.y - 0.1f , 0.0f ) , 1.0f );
		material.specular.z = std::min( std::max( material.specular.z - 0.1f , 0.0f ) , 1.0f );
	  }
	  ImGui::SameLine();
	  if( ImGui::Button( "+##m.s" , ImVec2( 20 , 20 ) ) ){
		//clamp components in [0.0 - 1.0]
		material.specular.x = std::min( std::max( material.specular.x + 0.1f , 0.0f ) , 1.0f );
		material.specular.y = std::min( std::max( material.specular.y + 0.1f , 0.0f ) , 1.0f );
		material.specular.z = std::min( std::max( material.specular.z + 0.1f , 0.0f ) , 1.0f );
	  }
	  if( ImGui::InputFloat("m.s.R", &material.specular.x, 0.001f, 0.01f, "%.5f") ) {
		if( material.specular.x < 0.0f ) material.specular.x = 0.0f;
		else if( material.specular.x > 1.0f ) material.specular.x = 1.0f;
		else updateShaderMaterialProperties();
	  }
	  if( ImGui::InputFloat("m.s.G", &material.specular.y, 0.001f, 0.01f, "%.5f") ) {
		if( material.specular.y < 0.0f ) material.specular.y = 0.0f;
		else if( material.specular.y > 1.0f ) material.specular.y = 1.0f;
		else updateShaderMaterialProperties();
	  }
	  if( ImGui::InputFloat("m.s.B", &material.specular.z, 0.001f, 0.01f, "%.5f") ) {
		if( material.specular.z < 0.0f ) material.specular.z = 0.0f;
		else if( material.specular.z > 1.0f ) material.specular.z = 1.0f;
		else updateShaderMaterialProperties();
	  }

  }
}
*/
