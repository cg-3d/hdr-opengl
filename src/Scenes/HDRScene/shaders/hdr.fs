#version 330 core

out vec4 FragColor;
in vec2 TexCoords;

uniform sampler2D floatBuffer;
uniform bool hdr;
uniform int selectedToneMap;
uniform float exposure;

vec3 reinhardToneMap( vec3 floatColor );

vec3 exposureToneMap( vec3 floatColor );

void main(){
	
	const float gamma = 2.2;
	vec3 floatColor = texture( floatBuffer, TexCoords ).rgb;
	
	if( hdr ){
		vec3 result;
		
		if( selectedToneMap == 1 ) 		 //Reinhard
			result = reinhardToneMap( floatColor );
		else if ( selectedToneMap == 2 ) //Exposure
			result = exposureToneMap( floatColor );
		
		FragColor = vec4( pow( result , vec3( 1.0 / gamma ) ) , 1.0 );
	} else {	// No HDR
		FragColor = vec4( pow( floatColor , vec3( 1.0 / gamma ) ) , 1.0 );
	}

} 

// Reinhard tone mapping
vec3 reinhardToneMap( vec3 floatColor ){
	
	vec3 result = floatColor / ( floatColor + vec3( 1.0 ) );
	return result;
	
}

// Exposure tone mapping
vec3 exposureToneMap( vec3 floatColor ){
	vec3 result = vec3( 1.0 ) - exp( -floatColor * exposure );
	return result;
}