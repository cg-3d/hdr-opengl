#version 330
//#version 130
//#extension GL_ARB_explicit_uniform_location : enable
//#extension GL_ARB_explicit_attrib_location : enable 

uniform vec3 color;

out vec4 FragCol;

void main(){
	
	FragCol = vec4( color , 1.0f );
	
} 