#version 450
in vec3 TextDir;

out vec4 FragColor;

uniform samplerCube cubemap; //Specific sampler for cubemaps

void main(){

	vec4 skyboxColor = texture( cubemap , TextDir );
	//skyboxColor = skyboxColor / ( 1.0 - skyboxColor );

	FragColor = skyboxColor;
	
}


