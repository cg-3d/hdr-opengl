#version 330
//#version 130
//#extension GL_ARB_explicit_uniform_location : enable
//#extension GL_ARB_explicit_attrib_location : enable

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aNorm;
layout (location=2) in vec2 aTexCoord;
layout (location=3) in vec3 aTangent;
layout (location=4) in vec3 aBitangent;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat3 normalMatrix;
uniform vec3 viewPos;

//Light uniform for tangent conversion
uniform vec3 pointLightPos;
uniform vec3 directionalLightDir;

out vec2 TextCoords;
//out vec3 FragPos; 
//out vec3 Normal;
//out vec3 Tangent;
//out vec3 Bitangent;

out vec3 TangentViewPos;
out vec3 TangentFragPos;

// Lights outputs
out vec3 TangentPointLightPos;
out vec3 TangentDirectionalLightDir;

void main(){
	
	vec3 T = normalize( vec3( normalMatrix * aTangent ) );
	vec3 B = normalize( vec3( normalMatrix * aBitangent ) );
	vec3 N = normalize( vec3( normalMatrix * aNorm ) );
	mat3 TBN = transpose( mat3( T , B , N ) ); // TBN is orthogonal, inverse = transpose
	
	TangentViewPos = TBN * viewPos;
	TangentFragPos = TBN * vec3( model * vec4( aPos , 1.0f ) );
	
	TangentPointLightPos = TBN * pointLightPos;
	TangentDirectionalLightDir = TBN * (-directionalLightDir);
	
	//Normal = normalize( normalMatrix * aNorm );
	//Tangent = T;
	//Bitangent = B;
	
	gl_Position =  projection * view * model * vec4( aPos , 1.0f );
	TextCoords = aTexCoord;
	//FragPos =  vec3( model * vec4( aPos , 1.0f ) );
	
}