#version 330
//#version 130
//#extension GL_ARB_explicit_uniform_location : enable
//#extension GL_ARB_explicit_attrib_location : enable

// Encapsulates the ambient, diffuse and specular
// light colors in a structure for covenience
struct LightCore{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};


// Directional light
struct DirectionalLight{
	vec3 direction;
	
	LightCore core;
};

// Point Light
struct PointLight{
	vec3 position;
	
	LightCore core;
	
	float c;
	float l;
	float q;
};

// Spot Light
struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutoff;
	float outerCutoff;
	
	LightCore core;
	
	float c;
	float l;
	float q;
};

//Material
struct Material{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular; 
	float shininess;
};

mat3 calculateBPLightComponents( LightCore core , vec3 normal , vec3 viewDirection , vec3 lightDirection );
vec3 calculateDirectionalLight( DirectionalLight light , vec3 normal , vec3 viewDirection );
vec3 calculatePointLight( PointLight light , vec3 normal , vec3 viewDirection );
vec3 calculateSpotLight( SpotLight light , vec3 normal , vec3 viewDirection );

//Uniforms
/////////
uniform DirectionalLight dirLight;
uniform PointLight pointLight;
//uniform SpotLight spotLight;
uniform Material material;
/////////

//uniform sampler2D texture_diffuse1;
//uniform sampler2D texture_specular1;
uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;
//uniform float shininess;

//uniform mat3 normalMatrix;
//uniform vec3 ViewPos;

//Inputs
in vec3 Normal;
//in vec3 Tangent;
//in vec3 Bitangent;

in vec2 TextCoords;
//in vec3 FragPos;
in vec3 TangentViewPos;
in vec3 TangentFragPos;

// Lights inputs
in vec3 TangentPointLightPos;
in vec3 TangentDirectionalLightDir;

//Outputs
out vec4 FragColor;

void main(){

	//vec3 norm = normalize( normalMatrix * Normal );
	//vec3 viewDirection = normalize( ViewPos - FragPos );
	vec3 viewDirection = normalize( TangentViewPos - TangentFragPos );
	vec3 tNorm = vec3( normalize( texture( texture_normal1 , TextCoords ).rgb * 2.0 - 1.0 ) ); //convert to [-1,1] range
	//tNorm *= -1.0f; //Flip normal
	//FragColor = vec4( calculateDirectionalLight( dirLight , norm , viewDirection ) , 1.0f );
	//FragColor = vec4( calculatePointLight( pointLight , norm , viewDirection ) , 1.0f );
	//FragColor = vec4( calculateSpotLight( spotLight , norm , viewDirection ) , 1.0f );
	
	//vec3 outputColor = calculateDirectionalLight( dirLight , Normal , viewDirection );
	//outputColor += calculatePointLight( pointLight , Normal , viewDirection );
	//outputColor += calculateSpotLight( spotLight , Normal , viewDirection );
	
	//float gamma = 2.2;
	//vec3 outputColor = calculateSpotLight( spotLight , Normal , viewDirection );
	vec3 outputColor = calculatePointLight( pointLight , tNorm , viewDirection );
	outputColor += calculateDirectionalLight( dirLight , tNorm , viewDirection );
	//FragColor = vec4( pow( outputColor.rgb , vec3( 1.0 / gamma ) ) , 1.0f );
	
	//FragColor = vec4( pow( outputColor , vec3( 1.0 / gamma ) ) , 1.0f );
	
	//vec3 outputColor = texture( texture_diffuse1 , TextCoords ).rgb;
	//vec3 outputColor = Bitangent;
	//outputColor = Normal;
	FragColor = vec4( outputColor , 1.0f );
	 
	//FragColor = vec4( tNorm , 1.0f );
	//FragColor = vec4( texture( texture_diffuse1 , TextCoords ).rgb , 1.0f ); 

}

// Blinn-Phong Lighting model 
//
// 
mat3 calculateBPLightComponents( LightCore core , vec3 normal , vec3 viewDir , vec3 lightDir ){
	
	// Illuminates only the face facing the light
	//float vnAngle = dot( viewDirection , normal );
	//if( vnAngle < 0 )
		//return mat3( 0.0f );
		
	//vec3 diffuseMapColor = texture( texture_diffuse1 , TextCoords ).rgb;
	vec3 diffuseMapColor = texture( texture_diffuse1 , TextCoords ).rgb;
	
	//vec3 reflectDirection = reflect( -lightDirection , normal );
	//float spec = pow( max( dot( viewDirection  , reflectDirection ) , 0.0f ) , shininess );
	
	vec3 halfwayDir = normalize( viewDir + lightDir ); 
	float spec = pow( max( dot( halfwayDir , normal ) , 0.0f ) , material.shininess );
	
	vec3 ambient = core.ambient * diffuseMapColor * material.ambient;
	vec3 diffuse = core.diffuse * ( max( dot( normal , lightDir ) , 0.0f ) * diffuseMapColor )  
				   * material.diffuse;
	//vec3 diffuse = ( max( dot( normal , lightDirection ) , 0.0f ) * diffuseMapColor );
	
	vec3 specular = core.specular * ( spec * texture( texture_specular1 , TextCoords ).rgb ) 
				    * material.specular;
	//Always 1.0f for specular
	//vec3 specular = core.specular * ( spec * vec3( 1.0f ) );
	
	return mat3( ambient , diffuse , specular );
}

// Directional light calculation
vec3 calculateDirectionalLight( DirectionalLight light , vec3 normal , vec3 viewDirection ){

	//vec3 lightDirection = normalize( -light.direction ); // uses the light direction
	vec3 lightDirection = normalize( TangentDirectionalLightDir ); // uses the light direction
	mat3 lightComponents = calculateBPLightComponents( light.core  , normal , viewDirection , lightDirection );
	
	vec3 ambient = lightComponents[0];
	vec3 diffuse = lightComponents[1];
	vec3 specular = lightComponents[2]; 

	return ( ambient + diffuse + specular );
} 

// Point light calculation
vec3 calculatePointLight( PointLight light , vec3 normal , vec3 viewDirection ){

	// vec3 lightDirection = normalize( light.position - FragPos );
	vec3 lightDirection = normalize( TangentPointLightPos - TangentFragPos );
	float dist = length( TangentPointLightPos - TangentFragPos );
	
	//Quadratic attenuation
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist );
	//float attenuation = 1.0f / ( dist * dist );
	
	//Linear attenuation  
	//float attenuation = 1.0f / ( light.c + light.l * dist );
	
	mat3 lightComponents = calculateBPLightComponents( light.core , normal , viewDirection , lightDirection );
	
	vec3 ambient = attenuation * lightComponents[0];
	vec3 diffuse = attenuation * lightComponents[1];
	vec3 specular = attenuation * lightComponents[2];
	
	return ( ambient + diffuse + specular );
}

// Spot light calculation
vec3 calculateSpotLight( SpotLight light , vec3 normal , vec3 viewDirection ){

	//vec3 lightDirection = normalize( light.position - FragPos );
	//float dist = length( light.position - FragPos );
	
	//TODO use TangentSpotLightPos 
	vec3 lightDirection = normalize( TangentPointLightPos - TangentFragPos );
	float dist = length( TangentPointLightPos - TangentFragPos );
	
	//Quadratic attenuation
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist );
	//float attenuation = 1.0f / ( dist * dist );
	
	//Linear attenuation  
	//float attenuation = 1.0f / ( light.c + light.l * dist ); 

	// smooth cone edges
	float thetaCos = dot( lightDirection , normalize( -light.direction ) );
	float epsilon = light.cutoff - light.outerCutoff;
	float intensity = clamp( ( thetaCos - light.outerCutoff ) / epsilon , 0.0f , 1.0f );
	
	mat3 lightComponents = calculateBPLightComponents( light.core , normal , viewDirection , lightDirection );
	
	vec3 ambient = attenuation * lightComponents[0]; // ambient does not use intensity
	vec3 diffuse = intensity * attenuation * lightComponents[1];
	vec3 specular = intensity * attenuation * lightComponents[2];
	
	return ( ambient + diffuse + specular );
}
  