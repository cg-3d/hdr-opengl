#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include "../../utilities/utilities.h"

#include "../../Engine/Shader.h"
#include "../../Engine/Model.h"
#include "../../Engine/Camera.h"
#include "../../Engine/cubemaps.h"
#include "../../Engine/input-callbacks.h"

#include "../../shapes/shapes.h"

#include "../../Engine/Scene.h"

#include "DemoScene.h"

DemoScene::DemoScene() :
	//Camera
	cam( glm::vec3( 0.0 , 2.0 , 2.0 ) , // position
		 glm::vec3( 0.0 , 0.0 , 0.0 ) , // target
		 glm::vec3( 0.0 , 1.0 , 0.0) // world up
    ) ,

	//Shaders
	lampShaderProgram(
		utilities::getPath( "src/shaders/lamp_vertex.vs" ).c_str() ,
		utilities::getPath( "src/shaders/lamp_fragment.fs").c_str()
    ) ,

	multipleLightsShaderProgram(
		utilities::getPath( "src/shaders/multiple_lights/mul_vertex.vs" ).c_str() ,
		utilities::getPath( "src/shaders/multiple_lights/mul_fragment.fs").c_str()
    ) ,

	skyboxShaderProgram(
		utilities::getPath( "src/shaders/cubemaps/cubemap.vs" ).c_str() ,
		utilities::getPath( "src/shaders/cubemaps/cubemap.fs" ).c_str()
	) ,

	reflectionShaderProgram(
		utilities::getPath( "src/shaders/env_mapping/env_mapping.vs" ).c_str() ,
		utilities::getPath( "src/shaders/env_mapping/reflect.fs" ).c_str()
	) ,

	// Models
	wraithModel(
		utilities::getPath( "models/wraith/wraith.obj" ).c_str()
	)
{

	//Projection matrix
	projection = glm::perspective( glm::radians( 45.0f ) ,
								   incalls::sceneViewHeight / (float) incalls::sceneViewHeight ,
								   0.1f , 500.0f );

	// Bind the projection matrix to be updated by the callback on window resize
	incalls::bindProjectionMatrix( &projection );
	// Bind the shaders projection uniforms to be updated on window resize
	incalls::bindProjectionUniform( &lampShaderProgram , "projection" );
	incalls::bindProjectionUniform( &multipleLightsShaderProgram , "projection" );
	incalls::bindProjectionUniform( &skyboxShaderProgram , "projection" );
	incalls::bindProjectionUniform( &reflectionShaderProgram , "projection" );
	// Set projection matrix uniforms
	lampShaderProgram.setMatrix4fv( "projection" , projection );
	skyboxShaderProgram.setMatrix4fv( "projection" , projection );
	multipleLightsShaderProgram.setMatrix4fv( "projection" , projection );
	reflectionShaderProgram.setMatrix4fv( "projection" , projection );

	//Camera
	cam.setTargetLockedOnMove( false );
	incalls::bindCamera( &cam );
	incalls::setCameraSpeed( 3.0f );
	incalls::setCameraOrbitSpeed( 40.0f );

	//Clear Color
	clearColor = glm::vec3( 0.098f, 0.114f, 0.122f );

	// Skybox
	skyboxVBO = Scene::newVBO();
	glBindBuffer( GL_ARRAY_BUFFER , skyboxVBO );
	glBufferData( GL_ARRAY_BUFFER , sizeof( cubemaps::skyboxVertices ) , cubemaps::skyboxVertices , GL_STATIC_DRAW );

	skyboxVAO = Scene::newVAO();
	glBindVertexArray( skyboxVAO );
	glBindBuffer( GL_ARRAY_BUFFER , skyboxVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 3 * sizeof ( float ) , (void*) 0 );
	glEnableVertexAttribArray( 0 );

	skyboxShaderProgram.setMatrix4fv( "projection" , projection );
	skyboxFaces = {
		utilities::getPath( "skybox/default/right.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/left.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/top.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/bottom.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/front.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/back.jpg" ).c_str()
	};

	skyboxTexture = cubemaps::loadCubemap( skyboxFaces );
	skyboxShaderProgram.setInt( "cubemap" , 0 );

	// Cubes and lines
	cubeVBO = Scene::newVBO();
	//glGenBuffers( 1 , &cubeVBO );
	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
//	glBufferData( GL_ARRAY_BUFFER , sizeof( cube::fullVertices ) , cube::fullVertices , GL_STATIC_DRAW );
	glBufferData( GL_ARRAY_BUFFER , sizeof( cube::van ) , cube::van , GL_STATIC_DRAW );
//	glBufferData( GL_ARRAY_BUFFER , 36 * 6 * sizeof( float ) , cubeVan , GL_STATIC_DRAW );

	cubeVAO = Scene::newVAO();
	//glGenVertexArrays( 1 , &cubeVAO );
	glBindVertexArray( cubeVAO );
	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 6 * sizeof( float ) , (void *)0 );
	glEnableVertexAttribArray( 0 );

	glVertexAttribPointer( 1 , 3 , GL_FLOAT , GL_FALSE , 6 * sizeof( float ) , (void *)( 3 * sizeof( float ) ) );
	glEnableVertexAttribArray( 1 );

	line = line::to( -1.0f , 0.0f , 0.0f  );
	lineVBO = Scene::newVBO();
	//glGenBuffers( 1 , &lineVBO );
	glBindBuffer( GL_ARRAY_BUFFER , lineVBO );
	glBufferData( GL_ARRAY_BUFFER , 6 * sizeof( float ) , line , GL_STATIC_DRAW  );

	lineVAO = Scene::newVAO();
	//glGenVertexArrays( 1 , &lineVAO );
	glBindVertexArray( lineVAO );
	glBindBuffer( GL_ARRAY_BUFFER , lineVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 3 * sizeof( float ) , (void *)0 );
	glEnableVertexAttribArray( 0 );

	//Model
	wraithModel.printModel();

	//Lights
	multipleLightsShaderProgram.setFloat( "shininess" , 64.0f );

	// Directional light
	dirLightDir = glm::normalize( glm::vec3( 0.0f , 0.0f , 1.0f ) );
	dirLightColor = glm::vec3( 0.0f , 0.4f , 0.6f );
	multipleLightsShaderProgram.setVec3( "dirLight.core.ambient" , 0.1f * dirLightColor );
	multipleLightsShaderProgram.setVec3( "dirLight.core.diffuse" , 0.5f * dirLightColor );
	multipleLightsShaderProgram.setVec3( "dirLight.core.specular" , 1.0f * dirLightColor );
	multipleLightsShaderProgram.setVec3( "dirLight.direction" , dirLightDir );

	// Point light
	pointLightColor = glm::vec3( 1.0f , 0.0f , 0.0f );
	pointLightPos = glm::vec3( 0.0f , 2.0f , 3.0f );
	multipleLightsShaderProgram.setVec3( "pointLight.core.ambient" , 0.1f * pointLightColor );
	multipleLightsShaderProgram.setVec3( "pointLight.core.diffuse" , 0.5f * pointLightColor );
	multipleLightsShaderProgram.setVec3( "pointLight.core.specular" , 1.0f * pointLightColor );
	multipleLightsShaderProgram.setVec3( "pointLight.position" , pointLightPos );
	multipleLightsShaderProgram.setFloat( "pointLight.c" , 1.0f );
	multipleLightsShaderProgram.setFloat( "pointLight.l" , 0.14f );
	multipleLightsShaderProgram.setFloat( "pointLight.q" , 0.007f );

	//Spot light
	spotLightPos = glm::vec3( 0.0f , 4.0f , -1.0f );
	spotLightDir = glm::normalize( glm::vec3( 0.0f , -1.0f , 1.0f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.core.ambient" , glm::vec3( 0.1f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.core.diffuse" , glm::vec3( 0.5f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.core.specular" , glm::vec3( 1.0f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.position" , spotLightPos );
	multipleLightsShaderProgram.setVec3( "spotLight.direction" , spotLightDir );
	multipleLightsShaderProgram.setFloat( "spotLight.cutoff" , glm::cos( glm::radians( 12.5f ) ) );
	multipleLightsShaderProgram.setFloat( "spotLight.outerCutoff" , glm::cos( glm::radians( 25.0f ) ) );
	multipleLightsShaderProgram.setFloat( "spotLight.c" , 1.0f );
	multipleLightsShaderProgram.setFloat( "spotLight.l" , 0.14f );
	multipleLightsShaderProgram.setFloat( "spotLight.q" , 0.007f );

}

void DemoScene::freeGLResources(){
	glDeleteTextures( 1 , &skyboxTexture );
}

DemoScene::~DemoScene(){
	delete line;
}

void DemoScene::draw(){
	glEnable( GL_DEPTH_TEST );
	glClearColor( clearColor.x , clearColor.y , clearColor.z , 1.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	//Model matrix
	glm::mat4 model = glm::mat4( 1.0f );

	// Reflective cube drawing
	model = glm::translate( model , glm::vec3( 3.0f , 0.0f , 0.0f ) );

	reflectionShaderProgram.setMatrix4fv( "model" , model );
	reflectionShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
	reflectionShaderProgram.setMatrix3fv( "normalMatrix" ,
										  glm::mat3( glm::transpose( glm::inverse( model ) ) ) );
	reflectionShaderProgram.setVec3( "cameraPos" , cam.getPosition() );
	reflectionShaderProgram.use();
	glBindVertexArray( cubeVAO );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_CUBE_MAP , skyboxTexture );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );
	//--------------------------------------------

	// Model drawing
	model = glm::mat4( 1.0f ); //reset the model matrix
	multipleLightsShaderProgram.use();
	multipleLightsShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
	multipleLightsShaderProgram.setMatrix4fv( "model" , model );
	multipleLightsShaderProgram.setMatrix3fv( "normalMatrix" ,
											  glm::mat3( glm::transpose( glm::inverse( model ) ) ) );
	multipleLightsShaderProgram.setVec3( "ViewPos" , cam.getPosition() );
	wraithModel.draw( multipleLightsShaderProgram );
	//--------------------------------------------

	//Lamps objects drawing
	//
	//Spot light
	model = glm::translate( glm::mat4( 1.0f ) , spotLightPos );
	model = glm::scale( model , glm::vec3( 0.2f ) );

	lampShaderProgram.use();
	lampShaderProgram.setMatrix4fv( "model" , model );
	lampShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
	lampShaderProgram.setVec3( "color" , glm::vec3( 1.0f ) );
	glBindVertexArray( cubeVAO );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );

	//Point light
	model = glm::translate( glm::mat4( 1.0f ) , pointLightPos );
	model = glm::scale( model , glm::vec3( 0.1f ) );

	lampShaderProgram.use();
	lampShaderProgram.setMatrix4fv( "model" , model );
	lampShaderProgram.setVec3( "color" , pointLightColor );
	glBindVertexArray( cubeVAO );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );

	//Directional light
	for( int i = 0 ; i < 3 ; i++ ){
		model = glm::mat4( 1.0f )
			  * glm::translate( glm::mat4( 1.0f ) , 7.0f * -dirLightDir + 0.05f * i * glm::vec3( 0.0f , 1.0f , 0.0f ) )
			  * glm::rotate( glm::mat4( 1.0f ) ,
							-glm::acos( glm::dot( dirLightDir , glm::vec3( -1.0f , 0.0f , 0.0f ) ) ),
							 glm::cross( dirLightDir , glm::vec3( -1.0f , 0.0f , 0.0f  ) ) )
			  * glm::scale( glm::mat4( 1.0f ) , glm::vec3( 3.0f ) );
		lampShaderProgram.setMatrix4fv( "model" , model );
		lampShaderProgram.setVec3( "color" , dirLightColor );
		glBindVertexArray( lineVAO );
		glDrawArrays( GL_LINES , 0 , 2 );
	}
	//--------------------------------------------


	//Skybox drawing : as last for optimizing
	glDepthMask( GL_FALSE );
	skyboxShaderProgram.use();
	// Removes translation components from view matrix for the skybox
	skyboxShaderProgram.setMatrix4fv( "view" , glm::mat4( glm::mat3( cam.getViewMatrix() ) ) );

	glDepthFunc( GL_LEQUAL );
	glBindVertexArray( skyboxVAO );
	glBindTexture( GL_TEXTURE_CUBE_MAP , skyboxTexture );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );
	glDepthMask( GL_TRUE );
	//--------------------------------------------

	//Reset
	glBindVertexArray( 0 );
	glDepthFunc( GL_LESS );
}

void DemoScene::drawGUI(){

}
