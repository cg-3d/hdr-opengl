#ifndef DEMOSCENE_H_
#define DEMOSCENE_H_

#include <glm/glm.hpp>

#include "../../Engine/Shader.h"
#include "../../Engine/Model.h"
#include "../../Engine/Camera.h"

#include "../../Engine/cubemaps.h"

#include "../../Engine/Scene.h"

class DemoScene : public Scene{

public:

	// Buffers
	unsigned int skyboxVBO;
	unsigned int skyboxVAO;

	unsigned int cubeVBO;
	unsigned int cubeVAO;

	unsigned int lineVBO;
	unsigned int lineVAO;
	float *line;

	// Camera
	Camera cam;

	// Shaders
	Shader lampShaderProgram;
	Shader multipleLightsShaderProgram;
	Shader skyboxShaderProgram;
	Shader reflectionShaderProgram;

	//Models
	Model wraithModel;

	// Skybox
	std::vector<std::string> skyboxFaces;
	unsigned int skyboxTexture;

	// Matrices
	glm::mat4 projection;

	//Lights
	glm::vec3 dirLightDir;
	glm::vec3 dirLightColor;

	glm::vec3 pointLightPos;
	glm::vec3 pointLightColor;

	glm::vec3 spotLightPos;
	glm::vec3 spotLightDir;

	glm::vec3 clearColor;

	//-----------------------------------
	//Methods
	DemoScene();

	// Drawing ( called inside rendering loop )
	void draw();
	void drawGUI();

	void freeGLResources();
	virtual ~DemoScene();

};


#endif /* DEMOSCENE_H_ */
