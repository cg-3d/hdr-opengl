/*
 * BPLightsScene.h
 *
 *  Created on: Jun 14, 2019
 *      Author: chris
 */

#ifndef SCENES_BPLIGHTSSCENE_H_
#define SCENES_BPLIGHTSSCENE_H_

#include <glm/glm.hpp>


/**
 * Scene to demonstrate Blinn-Phong lighting model
 */
class BPLightsScene : public Scene {

public:


	unsigned int planeVBO;
	unsigned int planeVAO;

	glm::mat4 projection;

	Camera cam;
	Shader bpLightsShader;

	unsigned int woodTexture;


	BPLightsScene();
	virtual ~BPLightsScene();
	void draw();

	void freeGLResources();

};


#endif /* SCENES_BPLIGHTSSCENE_H_ */
