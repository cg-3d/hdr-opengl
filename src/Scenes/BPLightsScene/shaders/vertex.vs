#version 450 

layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aNorm;
layout (location=2) in vec2 aTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

uniform mat3 normalMatrix;

out vec2 TextCoords;
out vec3 FragPos; 
out vec3 Normal;

void main(){
	
	Normal = - normalize( normalMatrix * aNorm );
	gl_Position =  projection * view * model * vec4( aPos , 1.0f );
	TextCoords = aTexCoord;
	FragPos =  vec3( model * vec4( aPos , 1.0f ) );

}