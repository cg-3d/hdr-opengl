/*
 * BPLightsScene.cpp
 *
 *  Created on: Jun 14, 2019
 *      Author: chris
 */

#include <iostream>
#include <string>

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include "../../utilities/utilities.h"
#include "../../Engine/engine-utils.h"

#include "../../Engine/Camera.h"
#include "../../Engine/Shader.h"
#include "../../Engine/input-callbacks.h"

#include "../../shapes/shapes.h"

#include "../../Engine/Scene.h"

#include "BPLightsScene.h"

BPLightsScene::BPLightsScene() :
	cam( glm::vec3( 0.0f , 1.0f , 2.0f  ) ,
		 glm::vec3( 0.0f , 0.0f , 0.0f  ) ,
		 glm::vec3( 0.0f , 1.0f , 0.0f  )
	) ,

	bpLightsShader(
		utilities::getPath( "src/Scenes/BPLightsScene/shaders/vertex.vs").c_str() ,
		utilities::getPath( "src/Scenes/BPLightsScene/shaders/bpl_fragment.fs").c_str()
	)

{
	projection = glm::perspective( glm::radians( 45.0f ) ,
							       incalls::sceneViewWidth / (float)incalls::sceneViewHeight ,
								   0.1f , 500.0f );

	incalls::bindProjectionMatrix( &projection );
	incalls::bindProjectionUniform( &bpLightsShader , "projection" );
	bpLightsShader.setMatrix4fv( "projection" , projection );

	cam.setTargetLockedOnMove( false );
	incalls::bindCamera( &cam );
	incalls::setCameraSpeed( 3.0f );
	incalls::setCameraOrbitSpeed( 40.0f );

	glClearColor( 0.098f, 0.114f, 0.122f , 1.0f );

	planeVBO = Scene::newVBO();
	glBindBuffer( GL_ARRAY_BUFFER , planeVBO );
	glBufferData( GL_ARRAY_BUFFER , sizeof( plane::vertNormTex ) , plane::vertNormTex , GL_STATIC_DRAW );

	planeVAO = Scene::newVAO();
	glBindVertexArray( planeVAO );
	glBindBuffer( GL_ARRAY_BUFFER , planeVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 8 * sizeof( float ) , (void*) 0 );
	glVertexAttribPointer( 1 , 3 , GL_FLOAT , GL_FALSE , 8 * sizeof( float ) , (void*) ( 3 * sizeof (float ) ) );
	glVertexAttribPointer( 2 , 2 , GL_FLOAT , GL_FALSE , 8 * sizeof( float ) , (void*) ( 6 * sizeof (float ) ) );
	glEnableVertexAttribArray( 0 );
	glEnableVertexAttribArray( 1 );
	glEnableVertexAttribArray( 2 );

	//bpLightsShader.setVec3( "color" , glm::vec3( 0.0f , 0.0f , 1.0f ) );
	glm::mat4 model = glm::mat4( 1.0f );
	model = glm::scale( model , glm::vec3( 10.0f ) );
	bpLightsShader.setMatrix4fv( "model" , model );
	bpLightsShader.setMatrix3fv( "normalMatrix" ,
								 glm::mat3( glm::transpose( glm::inverse( model ) ) ) );

	glActiveTexture( GL_TEXTURE1 );
	woodTexture = engUtils::textureFromFile(  "textures/wood.png" , true );
	bpLightsShader.setInt( "textureSampler" , 1 );

	bpLightsShader.setFloat( "shininess" , 10.0f );

	//Spot Light
	glm::vec3 spotLightPos = glm::vec3( 0.0f , 2.0f , 0.0f );
	glm::vec3 spotLightDir = glm::normalize( glm::vec3( -0.5f , -1.0f , 0.0f ) );
	bpLightsShader.setVec3( "spotLight.core.ambient" , glm::vec3( 0.1f ) );
	bpLightsShader.setVec3( "spotLight.core.diffuse" , glm::vec3( 0.5f ) );
	bpLightsShader.setVec3( "spotLight.core.specular" , glm::vec3( 1.0f ) );
	bpLightsShader.setVec3( "spotLight.position" , spotLightPos );
	bpLightsShader.setVec3( "spotLight.direction" , spotLightDir );
	bpLightsShader.setFloat( "spotLight.cutoff" , glm::cos( glm::radians( 20.0f ) ) );
	bpLightsShader.setFloat( "spotLight.outerCutoff" , glm::cos( glm::radians( 40.0f ) ) );
	bpLightsShader.setFloat( "spotLight.c" , 1.0f );
	bpLightsShader.setFloat( "spotLight.l" , 0.14f );
	bpLightsShader.setFloat( "spotLight.q" , 0.07f );

}

BPLightsScene::~BPLightsScene(){

}

void BPLightsScene::freeGLResources(){ }

void BPLightsScene::draw(){

	glEnable( GL_DEPTH_TEST );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

//	glm::mat4 model = glm::mat4( 1.0f );

	bpLightsShader.use();
//	bpLightsShader.setMatrix4fv( "model" , model );
	bpLightsShader.setMatrix4fv( "view" , cam.getViewMatrix() );
	bpLightsShader.setVec3( "ViewPos" , cam.getPosition() );
	glBindVertexArray( planeVAO );
	//glActiveTexture( GL_TEXTURE1 );
	glBindTexture( GL_TEXTURE_2D , woodTexture );
	glDrawArrays( GL_TRIANGLES , 0 , 6 );

	glBindVertexArray( 0 );
}
