#include <iostream>
#include <string>

#include <glad/glad.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>

#include "../../imgui/imgui.h"
#include "../../imgui/imgui_impl_glfw.h"
#include "../../imgui/imgui_impl_opengl3.h"
#include "../../imgui/toggle_button.h"

#include "../../Engine/input-callbacks.h"
#include "../../Engine/engine-utils.h"
#include "../../shapes/shapes.h"
#include "../../utilities/utilities.h"

//#include "Lights/DirectionalLight.h"
//#include "Lights/PointLight.h"
//#include "Lights/SpotLight.h"

#include "DeferredScene.h"


DeferredScene::DeferredScene() :
	cam( glm::vec3( 0.0f , 0.0f , 0.0f ) ,
		 glm::vec3( 0.0f , 0.0f , -5.0f ) ,
		 glm::vec3( 0.0f , 1.0f , 0.0f ) ) ,
	gPassShader( utilities::getPath( "src/Scenes/DeferredScene/shaders/gPass.vs" ).c_str() ,
				 utilities::getPath( "src/Scenes/DeferredScene/shaders/gPass.fs" ).c_str() ) ,
    dLightShader( utilities::getPath( "src/Scenes/DeferredScene/shaders/dLightPass.vs" ).c_str() ,
				  utilities::getPath( "src/Scenes/DeferredScene/shaders/dLightPass.fs" ).c_str() ) ,
	lightBoxShader( utilities::getPath( "src/Scenes/DeferredScene/shaders/lightBox.vs" ).c_str() ,
				    utilities::getPath( "src/Scenes/DeferredScene/shaders/lightBox.fs" ).c_str() )

//   , debugSahder( utilities::getPath( "src/Scenes/DeferredScene/shaders/debug/debug.vs" ).c_str() ,
//				utilities::getPath( "src/Scenes/DeferredScene/shaders/debug/debug.fs" ).c_str() )


{
	//Scene' render depth buffer
	glGenRenderbuffers( 1 , &renderbuffer );
	glBindFramebuffer( GL_FRAMEBUFFER , this->framebuffer );
	glBindRenderbuffer( GL_RENDERBUFFER , renderbuffer );
	glRenderbufferStorage( GL_RENDERBUFFER , GL_DEPTH_COMPONENT ,
						   incalls::sceneViewWidth , incalls::sceneViewHeight );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER , GL_DEPTH_ATTACHMENT ,
							   GL_RENDERBUFFER , renderbuffer );
	incalls::fbTracker()
			.attachFramebufferRenderbuffer(
				&this->framebuffer , &renderbuffer ,
				GL_DEPTH_ATTACHMENT , GL_DEPTH_COMPONENT
			);

	// Projection
	projection = glm::perspective( glm::radians( 45.0f ) ,
								   incalls::sceneViewWidth / (float)incalls::sceneViewHeight ,
								   0.1f , 500.0f );

	std::cout << "ScreenSize: ( " << incalls::screenWidth << " , " << incalls::screenHeight << std::endl;

	incalls::bindProjectionMatrix( &projection );
	gPassShader.setMatrix4fv( "projection" , projection );
	lightBoxShader.setMatrix4fv( "projection" , projection );
	incalls::bindProjectionUniform( &gPassShader , "projection" );
	incalls::bindProjectionUniform( &lightBoxShader , "projection" );
//	incalls::bindProjectionUniform( &flatShader , "projection" );
//	sceneShader.setMatrix4fv( "projection" , projection );
//	flatShader.setMatrix4fv( "projection" , projection );
	//---------------------------------------------------

	//Camera
	cam.setTargetLockedOnMove( false );
	incalls::bindCamera( &cam );
	incalls::setCameraSpeed( 3.0f );
	incalls::setCameraOrbitSpeed( 40.0f );
	//---------------------------------------------------

	// Shapes buffers
	float *cubeWithTangentSpace = cube::withTangentsAndBitangents( cube::fullVertices );

	cubeVBO = Scene::newVBO();
	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
	glBufferData( GL_ARRAY_BUFFER , cube::withTangentAndBitangentsSize , cubeWithTangentSpace , GL_STATIC_DRAW );

	cubeVAO = Scene::newVAO();
	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
	glBindVertexArray( cubeVAO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)0 );
	glVertexAttribPointer( 1 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 3 * sizeof( float ) ) );
	glVertexAttribPointer( 2 , 2 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 6 * sizeof( float ) ) );
	glVertexAttribPointer( 3 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 8 * sizeof( float ) ) );
	glVertexAttribPointer( 4 , 3 , GL_FLOAT , GL_FALSE , 14 * sizeof( float ) , (void *)( 11 * sizeof( float ) ) );
	glEnableVertexAttribArray( 0 );
	glEnableVertexAttribArray( 1 );
	glEnableVertexAttribArray( 2 );
	glEnableVertexAttribArray( 3 );
	glEnableVertexAttribArray( 4 );

	// Quad buffer
	quadVBO = Scene::newVBO();
	glBindBuffer( GL_ARRAY_BUFFER , quadVBO );
	glBufferData( GL_ARRAY_BUFFER , sizeof( quad::positionsAndTextureCoords ) , quad::positionsAndTextureCoords , GL_STATIC_DRAW );

	quadVAO = Scene::newVAO();
	glBindBuffer( GL_ARRAY_BUFFER , quadVBO );
	glBindVertexArray( quadVAO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 5 * sizeof( float ) , (void *)0 );
	glVertexAttribPointer( 1 , 2 , GL_FLOAT , GL_FALSE , 5 * sizeof( float ) , (void *)( 3 * sizeof( float ) ) );
	glEnableVertexAttribArray( 0 );
	glEnableVertexAttribArray( 1 );

	delete cubeWithTangentSpace;
	//---------------------------------------------------

	glEnable( GL_DEPTH_TEST );

	// Geometry-PASS Buffers
	initGeometryPassBuffers();

	std::cout << "gBuffer: " << gBuffer << std::endl;
	std::cout << "gNormal: " << gNormals << std::endl;
	std::cout << "gColorSpec: " << gColorSpec << std::endl;
	std::cout << "gRenderbuffer: " << gRenderbuffer << std::endl;

	// Shaders

	// G-PASS Shader
	//
	// Geometry pass input textures
	gPassShader.setInt( "texture_diffuse1" , 0 );
	gPassShader.setInt( "texture_specular1" , 1 );
	gPassShader.setInt( "texture_normal1" , 2 );

	invertSpecular = false;
	dLightShader.setInt( "invertSpecular" , ( invertSpecular ? 2 : 1 ) );

	glActiveTexture( GL_TEXTURE0 );
	colorMap = engUtils::textureFromFile( utilities::getPath( "textures/stone_wall/stone_wall_color.jpg" ).c_str() , true );

	glActiveTexture( GL_TEXTURE1 );
	specularMap = engUtils::textureFromFile( utilities::getPath( "textures/stone_wall/stone_wall_spec.jpg" ).c_str() , false );

	glActiveTexture( GL_TEXTURE2 );
	normalMap = engUtils::textureFromFile( utilities::getPath( "textures/stone_wall/stone_wall_norm.jpg" ).c_str() , false );

	std::cout << "ColorMap: " << colorMap << std::endl;
	std::cout << "SpecularMap: " << specularMap << std::endl;
	std::cout << "NormalMap: " << normalMap << std::endl;

	// DEFERRED PASS Lighting shader
	//
	// Lights
	pLights = std::vector<PointLight>();
	pLights.push_back(
	  PointLight(
		glm::vec3( 0.0f , 2.0f , 0.0f ) ,
		1.0f , 1.0f , 0.1f
	  )
	);

	pLights.push_back(
	  PointLight(
		glm::vec3( 1.0f , 2.0f , 0.0f ) ,
		1.0f , 1.0f , 0.01f
	  )
	);

	pLights.push_back(
	  PointLight(
		glm::vec3( 0.0f , 0.0f , -2.5f ) ,
		1.0f , 1.0f , 0.1f
	  )
	);

	for( unsigned int i = 0 ; i < pLights.size() ; ++i ){
		pLights[i].setShaderUniform( dLightShader , "pLights[" + std::to_string(i) + "]" );
	}

	// Deferred lighting pass input textures
	dLightShader.setInt( "gPosition" , 0 );
	dLightShader.setInt( "gNormal" , 1 );
	dLightShader.setInt( "gAlbedoSpec" , 2 );

	//Configs
	dLightShader.setInt( "invertSpecular" , 1 );
	dLightShader.setFloat( "shininess" , 8.0f );
}

void DeferredScene::initGeometryPassBuffers(){

	// Framebuffer
	glGenFramebuffers( 1 , &gBuffer );
	glBindFramebuffer( GL_FRAMEBUFFER , gBuffer );

	// Attachments

	// 	Positions color buffer : RGB16F - RGB
	glGenTextures( 1 , &gPosition );
	glBindTexture( GL_TEXTURE_2D , gPosition );
	glTexImage2D( GL_TEXTURE_2D , 0 , GL_RGB32F , incalls::sceneViewWidth , incalls::sceneViewHeight ,
				  	  	  	      0 , GL_RGB , GL_FLOAT , NULL );
	glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , GL_NEAREST );
	glFramebufferTexture2D( GL_FRAMEBUFFER , gAttachments[0] , GL_TEXTURE_2D , gPosition , 0 );

	// 	Normals color buffer : RGB16F - RGB
	glGenTextures( 1 , &gNormals );
	glBindTexture( GL_TEXTURE_2D , gNormals );
	glTexImage2D( GL_TEXTURE_2D , 0 , GL_RGB32F , incalls::sceneViewWidth , incalls::sceneViewHeight ,
								  0 , GL_RGB , GL_FLOAT , NULL );
	glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , GL_NEAREST );
	glFramebufferTexture2D( GL_FRAMEBUFFER , gAttachments[1] , GL_TEXTURE_2D , gNormals , 0 );

	// 	Diffuse(Albedo) + Specular color buffer : RGBA - RGBA
	glGenTextures( 1 , &gColorSpec );
	glBindTexture( GL_TEXTURE_2D , gColorSpec );
	glTexImage2D( GL_TEXTURE_2D , 0 , GL_RGBA , incalls::sceneViewWidth , incalls::sceneViewHeight ,
								  0 , GL_RGBA , GL_UNSIGNED_BYTE , NULL );
	glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MIN_FILTER , GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D , GL_TEXTURE_MAG_FILTER , GL_NEAREST );
	glFramebufferTexture2D( GL_FRAMEBUFFER , gAttachments[2] , GL_TEXTURE_2D , gColorSpec , 0 );

	// ** Tell openGL wich color attachments to use (of this framebuffer) for rendering **
	glDrawBuffers( 3 , gAttachments );

	// Renderbuffer - Depth
	glGenRenderbuffers( 1 , &gRenderbuffer );
	glBindRenderbuffer( GL_RENDERBUFFER , gRenderbuffer );
	glRenderbufferStorage( GL_RENDERBUFFER , GL_DEPTH_COMPONENT , incalls::sceneViewWidth , incalls::sceneViewHeight );
	glFramebufferRenderbuffer( GL_FRAMEBUFFER , GL_DEPTH_ATTACHMENT , GL_RENDERBUFFER , gRenderbuffer );

	// check for completeness
	auto fboStatus = glCheckFramebufferStatus( GL_FRAMEBUFFER );
	if( fboStatus != GL_FRAMEBUFFER_COMPLETE ){
		std::cout << "ERROR::GEOMETRY-PASS::FRAMEBUFFER not complete: " <<
					 fboStatus << std::endl;
	}

	// Needed glViewport
//	glViewport( 0 , 0 , incalls::sceneViewWidth , incalls::sceneViewHeight );

	// Bind geometry-pass buffers to be resized by the callbacks
	//
	// Positions colorbuffer + Renderbuffer
	incalls::fbTracker().attachFramebufferTex2DColorbufferRenderbuffer(
			&gBuffer ,
			&gPosition , gAttachments[0] , GL_RGB32F , GL_RGB , GL_FLOAT , 0 , GL_NEAREST , GL_NEAREST ,
			&gRenderbuffer , GL_DEPTH_ATTACHMENT , GL_DEPTH_COMPONENT
	);

	// Normals colorbuffer
	incalls::fbTracker().attachFramebufferTex2DColorbuffer(
			&gBuffer ,
			&gNormals, gAttachments[1] , GL_RGB32F , GL_RGB , GL_FLOAT , 0 , GL_NEAREST , GL_NEAREST
	);

	// Color (albedo) + Specular colorbuffer
	incalls::fbTracker().attachFramebufferTex2DColorbuffer(
			&gBuffer ,
			&gColorSpec, gAttachments[2] , GL_RGBA , GL_RGBA , GL_UNSIGNED_BYTE , 0 , GL_NEAREST , GL_NEAREST
	);

	incalls::fbTracker().addDrawBuffer( &gBuffer , gAttachments[0] );
	incalls::fbTracker().addDrawBuffer( &gBuffer , gAttachments[1] );
	incalls::fbTracker().addDrawBuffer( &gBuffer , gAttachments[2] );

	// unbind
	glBindFramebuffer( GL_FRAMEBUFFER , 0 );
	glBindTexture( GL_TEXTURE_2D , 0 );
	glBindRenderbuffer( GL_RENDERBUFFER , 0 );
}

DeferredScene::~DeferredScene(){

}

void DeferredScene::freeGLResources(){
	glDeleteTextures( 1 , &colorMap );
	glDeleteTextures( 1 , &specularMap );
	glDeleteTextures( 1 , &normalMap );

	glDeleteRenderbuffers( 1 , &gRenderbuffer );
	glDeleteTextures( 1 , &gColorSpec );
	glDeleteTextures( 1 , &gNormals );
	glDeleteTextures( 1 , &gPosition );
	glDeleteFramebuffers( 1 , &gBuffer );

	glDeleteRenderbuffers( 1 , &renderbuffer );
}

/**
 * Drawing directives for the geometry pass
 */
void DeferredScene::gPassDraw(){

	gPassShader.setMatrix4fv( "view" , cam.getViewMatrix() );
	glm::mat4 model = glm::mat4( 1.0f );
	gPassShader.setMatrix4fv( "model" , model );
	glm::mat3 normalMatrix = glm::mat3( glm::transpose( glm::inverse( model ) ) );
	gPassShader.setMatrix3fv( "normalMatrix" , normalMatrix );
	glBindVertexArray( cubeVAO );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );

	model = glm::translate( glm::mat4( 1.0f ) ,
							glm::vec3( 1.1f , 0.0f , 0.0f ) );
	gPassShader.setMatrix4fv( "model" , model );
	normalMatrix = glm::mat3( glm::transpose( glm::inverse( model ) ) );
	gPassShader.setMatrix3fv( "normalMatrix" , normalMatrix );
	glBindVertexArray( cubeVAO );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );

	model = glm::translate( glm::mat4( 1.0f ) ,
						    glm::vec3( 0.6f , 0.0f , -1.5f ) );
	gPassShader.setMatrix4fv( "model" , model );
	normalMatrix = glm::mat3( glm::transpose( glm::inverse( model ) ) );
	gPassShader.setMatrix3fv( "normalMatrix" , normalMatrix );
	glBindVertexArray( cubeVAO );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );

	model = glm::translate( glm::mat4( 1.0f ) ,
							glm::vec3( -0.6f , 0.0f , -1.5f ) );
	gPassShader.setMatrix4fv( "model" , model );
	normalMatrix = glm::mat3( glm::transpose( glm::inverse( model ) ) );
	gPassShader.setMatrix3fv( "normalMatrix" , normalMatrix );
	glBindVertexArray( cubeVAO );
	glDrawArrays( GL_TRIANGLES , 0 , 36 );

	utilities::checkGLError();

}

/**
 * Drawing directives for the deferred lighting pass
 */
void DeferredScene::dPassDraw(){
	glBindVertexArray( quadVAO );
	dLightShader.setVec3( "ViewPos" , cam.getPosition() );
	dLightShader.use();
	glDrawArrays( GL_TRIANGLE_STRIP , 0 , 4 );

	utilities::checkGLError();
}


void DeferredScene::draw(){

	glEnable( GL_DEPTH_TEST );

	glClearColor( 0.0f , 0.0f , 0.0f , 1.0f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	// "Draw" in the G-Buffer
	glBindFramebuffer( GL_FRAMEBUFFER , gBuffer );
		//glClearColor( 0.0f , 0.0f , 0.0f , 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		// Inputs for the geometry pass
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D , colorMap );
		glActiveTexture( GL_TEXTURE1 );
		glBindTexture( GL_TEXTURE_2D , specularMap );
		glActiveTexture( GL_TEXTURE2 );
		glBindTexture( GL_TEXTURE_2D , normalMap );

		gPassDraw();

	// Draw in the scene's framebuffer
	glBindFramebuffer( GL_FRAMEBUFFER , this->framebuffer );
		//glClear( GL_COLOR_BUFFER_BIT );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		// gBuffer as input for the deferred lighting pass
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D , gPosition );
		glActiveTexture( GL_TEXTURE1 );
		glBindTexture( GL_TEXTURE_2D , gNormals );
		glActiveTexture( GL_TEXTURE2 );
		glBindTexture( GL_TEXTURE_2D , gColorSpec );

		dPassDraw();

	// Blit
	glBindFramebuffer( GL_READ_FRAMEBUFFER , gBuffer );
	glBindFramebuffer( GL_DRAW_FRAMEBUFFER , this->framebuffer );
	glBlitFramebuffer( 0 , 0 , incalls::sceneViewWidth , incalls::sceneViewHeight ,
					   0 , 0 , incalls::sceneViewWidth , incalls::sceneViewHeight ,
					   GL_DEPTH_BUFFER_BIT, GL_NEAREST );
	glBindFramebuffer( GL_FRAMEBUFFER , this->framebuffer );

	lightBoxShader.setMatrix4fv( "projection" , projection );
	lightBoxShader.setMatrix4fv( "view" , cam.getViewMatrix() );
	for( unsigned int i = 0; i < pLights.size() ; ++i ){
		glm::mat4 model = glm::mat4( 1.0f );
		model = glm::translate( model , pLights[i].position );
		model = glm::scale( model , glm::vec3( 0.2f) );
		lightBoxShader.use();
		lightBoxShader.setMatrix4fv( "model" , model );
		lightBoxShader.setVec3( "color" , pLights[i].specular );

		glBindVertexArray( cubeVAO );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );
	}

	utilities::checkGLError();

	glBindVertexArray(0);
	glBindFramebuffer( GL_FRAMEBUFFER , 0 );
}

void DeferredScene::drawGUI(){

	ImGui::Dummy(ImVec2(0.0f, 5.0f));

	//Invert specular map
	if( ToggleButton("##invspec", &invertSpecular) ) {
		invertSpecular = !invertSpecular;
		dLightShader.setInt( "invertSpecular" , ( invertSpecular ? 2 : 1 ) );
		std::cout << "invertSpecular: " << invertSpecular << std::endl;
	}

	ImGui::SameLine();
	ImGui::Text("invertSpecular");

}
