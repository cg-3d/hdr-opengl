#ifndef SCENES_DEFERREDSCENE_DEFERREDSCENE_H_
#define SCENES_DEFERREDSCENE_DEFERREDSCENE_H_

#include <glm/glm.hpp>

//#include <glad/glad.h>

#include "../../Engine/Scene.h"
#include "../../Engine/Camera.h"
#include "../../Engine/Shader.h"

#include "../../Engine/Lights/Lights.h"

class DeferredScene : public Scene {

private:
	void initGeometryPassBuffers();
	void gPassDraw();
	void dPassDraw();

public:
	//Deferred shading buffers
	// GEOMETRY-PASS
	unsigned int gBuffer; // Framebuffer
	unsigned int gPosition , gNormals , gColorSpec; // Attachments
	unsigned int gAttachments[3] = {
			GL_COLOR_ATTACHMENT0 , // Positions
			GL_COLOR_ATTACHMENT1 , // Normals
			GL_COLOR_ATTACHMENT2   // Color + Specular
	};
	unsigned int gRenderbuffer;

	Camera cam;
	glm::vec3 camPos; //To track position from inputs

	//Screen-filled quad buffers
	unsigned int quadVBO;
	unsigned int quadVAO;

	//Shapes buffers
	unsigned int cubeVAO;
	unsigned int cubeVBO;

	//Matrices
	glm::mat4 projection;

	//Shaders
//	Shader sceneShader;
//	Shader debugSahder;
	Shader gPassShader;
	Shader dLightShader;
	Shader lightBoxShader;

	std::vector<PointLight> pLights;

	bool invertSpecular;

	unsigned int colorMap;
	unsigned int specularMap;
	unsigned int normalMap;

	unsigned int renderbuffer;

	DeferredScene();
	~DeferredScene();

	void draw();
	void freeGLResources();
	void drawGUI();


};


#endif /* SCENES_DEFERREDSCENE_DEFERREDSCENE_H_ */
