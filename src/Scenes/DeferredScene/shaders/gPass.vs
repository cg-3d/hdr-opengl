#version 330 core

//Inputs
layout (location=0) in vec3 aPos;
layout (location=1) in vec3 aNorm;
layout (location=2) in vec2 aTexCoords;
layout (location=3) in vec3 aTan;
layout (location=4) in vec3 aBitan;

//Uniforms
uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

uniform mat3 normalMatrix;
//uniform vec3 viewPos;
//uniform vec3 lightPos;

//Outputs
out vec2 TexCoords;
out vec3 FragPos;
out mat3 TBN;
//out vec3 Normal;
//out vec3 TangentViewPos;
//out vec3 TangentLightPos;
//out vec3 TangentFragPos;

mat3 TBNmatrix( vec3 T , vec3 B , vec3 N );

void main(){

	vec4 worldPos = model * vec4( aPos , 1.0 ); 
	FragPos = worldPos.xyz;
	TBN = TBNmatrix( aTan , aBitan , aNorm );
	//Normal = aNorm;
	
	gl_Position = projection * view * worldPos;
	TexCoords = aTexCoords;

	//mat3 invTBN = inverseTBN( aTan , aBitan , aNorm );

//	TangentViewPos = invTBN * viewPos;
//	TangentLightPos = invTBN * lightPos;
//	TangentFragPos = invTBN * vec3( model * vec4( aPos , 1.0 ) );
	
}

// Calculates the TBN matrix
mat3 TBNmatrix( vec3 aT , vec3 aB , vec3 aN ){
	
	vec3 T = normalize( vec3( normalMatrix * aT ) );
	vec3 B = normalize( vec3( normalMatrix * aB ) );
	vec3 N = normalize( vec3( normalMatrix * aN ) );
	
	//return transpose( mat3( T , B , N ) ); //inverse
	return mat3( T , B , N );
}