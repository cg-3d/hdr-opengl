#version 330 core
layout (location=0) out vec3 gPosition;
layout (location=1) out vec3 gNormal;
layout (location=2) out vec4 gAlbedoSpec;

in vec2 TexCoords;
in vec3 FragPos;
//in vec3 Normal;
in mat3 TBN;

uniform sampler2D texture_diffuse1;
uniform sampler2D texture_specular1;
uniform sampler2D texture_normal1;


void main(){
	gPosition = FragPos;
	//gNormal = normalize( Normal );
	gAlbedoSpec.rgb = texture( texture_diffuse1 , TexCoords ).rgb; // first three coordinates as albedo color
	gAlbedoSpec.a = texture( texture_specular1 , TexCoords ).r; //fourth(alpha) coordinate as specular intensity
	
	//gNormal = Normal;
	// Normal mapping
	vec3 norm = texture( texture_normal1 , TexCoords ).rgb;
	norm = normalize( norm * 2.0 - 1.0 ); 		// [-1 ,1] range translation
	gNormal = normalize( TBN * norm );	// TBN to world space
}