#version 330 core

// Encapsulates the ambient, diffuse and specular
// light colors in a structure for covenience
struct LightCore{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	
	int type;
};


// Directional light
struct DirectionalLight{
	vec3 direction;
	
	LightCore core;
};

// Point Light
struct PointLight{
	vec3 position;
	
	LightCore core;
	
	float c;
	float l;
	float q;
};

// Spot Light
struct SpotLight{
	vec3 position;
	vec3 direction;
	float cutoff;
	float outerCutoff;
	
	LightCore core;
	
	float c;
	float l;
	float q;
};

// G-Buffer uniforms
uniform sampler2D gPosition;
uniform sampler2D gNormal;
uniform sampler2D gAlbedoSpec;

// Lighting uniforms
uniform vec3 ViewPos;

// Settings
uniform int invertSpecular;
uniform float shininess;

 // Lights
//const int NR_DIRECTIONAL = 0;
const int NR_POINT = 3;
//const int NR_SPOT = 0;

//uniform DirectionalLight dLights[NR_DIRECTIONAL];
uniform PointLight pLights[NR_POINT];
//uniform SpotLight dLights[NR_SPOT];


// Inputs
in vec2 TexCoords;

// Outputs
out vec4 FragColor;

// Functions
mat3 calculateBPLightComponents( LightCore core , 
								 vec3 normal , vec3 viewDirection , vec3 lightDirection , 
								 vec3 diffuseColor , vec3 specularIntensity );
								 
vec3 calculateDirectionalLight( DirectionalLight light , 
								vec3 normal , vec3 viewDirection , 
								vec3 diffuseColor , vec3 specularIntensity );
								
vec3 calculatePointLight( PointLight light , 
						  vec3 normal , vec3 viewDirection , vec3 FragPos , 
						  vec3 diffuseColor , vec3 specularIntensity );
						  
vec3 calculateSpotLight( SpotLight light , 
						 vec3 normal , vec3 viewDirection , vec3 FragPos  , 
						 vec3 diffuseColor , vec3 specularIntensity );

void main(){

	// Retrieve data from the G-Buffer
	vec3 FragPos = texture( gPosition , TexCoords ).rgb;
	vec3 Normal = texture( gNormal , TexCoords ).rgb;
	vec3 Diffuse = texture( gAlbedoSpec , TexCoords ).rgb;
	float Specular = ( invertSpecular == 1 ? texture( gAlbedoSpec , TexCoords ).a :
											 1.0 - texture( gAlbedoSpec , TexCoords ).a );
	//if( invertSpecular == 1 ){
	//	Specular = texture( gAlbedoSpec , TexCoords ).a;
	//}else{
	//	Specular = 1.0 - texture( gAlbedoSpec , TexCoords ).a;
	//}
	
	vec3 viewDirection = normalize( ViewPos - FragPos );
	
	vec3 outputColor = vec3( 0.0f );
	for( int i = 0; i < NR_POINT ; ++i ){
		outputColor += calculatePointLight( 
			pLights[i] ,
			Normal , viewDirection , FragPos , 
			Diffuse , vec3( Specular ) 
		); 
	}
	
	FragColor = vec4( outputColor , 1.0f );
	
	//FragColor = vec4( Diffuse , 1.0f );
	//FragColor = vec4( Specular , 0.0f , 0.0f , 1.0f );
	
	//Gamma correction
	//float gamma = 2.2f;
	//FragColor = vec4( pow( finalColor , vec3( 1.0 / gamma ) ) , 1.0 );
		
}

// Blinn-Phong Lighting model 
//
// 
mat3 calculateBPLightComponents( LightCore core , vec3 normal , vec3 viewDirection , vec3 lightDirection , 
						         vec3 diffuseColor , vec3 specularIntensity ){
	
	// Illuminates only the face facing the light
	//float vnAngle = dot( viewDirection , normal );
	//if( vnAngle < 0 )
		//return mat3( 0.0f );
		
	//vec3 diffuseMapColor = texture( texture_diffuse1 , TextCoords ).rgb;
	//vec3 diffuseMapColor = texture( textureSampler , TextCoords ).rgb;
	
	//vec3 reflectDirection = reflect( -lightDirection , normal );
	//float spec = pow( max( dot( viewDirection  , reflectDirection ) , 0.0f ) , shininess );
	
	vec3 halfwayDir = normalize( viewDirection + lightDirection ); 
	float spec = pow( max( dot( halfwayDir , normal ) , 0.0f ) , shininess );
	
	vec3 ambient = core.ambient * diffuseColor;
	vec3 diffuse = core.diffuse * ( max( dot( normal , lightDirection ) , 0.0f ) * diffuseColor );
	//vec3 diffuse = ( max( dot( normal , lightDirection ) , 0.0f ) * diffuseMapColor );
	
	vec3 specular = core.specular * ( spec * specularIntensity );
	//Always 1.0f for specular (Flat case)
	//vec3 specular = core.specular * ( spec * vec3( 1.0f ) );
	
	return mat3( ambient , diffuse , specular );
}

// Directional light calculation
vec3 calculateDirectionalLight( DirectionalLight light , vec3 normal , vec3 viewDirection , 
						        vec3 diffuseColor , vec3 specularIntensity ){

	vec3 lightDirection = normalize( -light.direction ); // uses the light direction
	mat3 lightComponents = calculateBPLightComponents( 
		light.core  , 
		normal , 
		viewDirection , 
		lightDirection , 
		diffuseColor , 
		specularIntensity 
	);
	
	vec3 ambient = lightComponents[0];
	vec3 diffuse = lightComponents[1];
	vec3 specular = lightComponents[2]; 

	return ( ambient + diffuse + specular );
} 

// Point light calculation
vec3 calculatePointLight( PointLight light , vec3 normal , vec3 viewDirection ,
						  vec3 FragPos , 
						  vec3 diffuseColor , vec3 specularIntensity ){

	vec3 lightDirection = normalize( light.position - FragPos );
	float dist = length( light.position - FragPos );
	//vec3 lightDirection = normalize( TangentLightPos - TangentFragPos );
	//float dist = length( TangentLightPos - TangentFragPos );
	
	//Quadratic attenuation
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist );
	//float attenuation = 1.0f / ( dist * dist );
	
	mat3 lightComponents = calculateBPLightComponents( 
		light.core , 
		normal , 
		viewDirection , 
		lightDirection , 
		diffuseColor ,
		specularIntensity 
	);
	
	vec3 ambient = attenuation * lightComponents[0];
	vec3 diffuse = attenuation * lightComponents[1];
	vec3 specular = attenuation * lightComponents[2];
	
	return ( ambient + diffuse + specular );
}

// Spot light calculation
vec3 calculateSpotLight( SpotLight light , vec3 normal , vec3 viewDirection ,
						 vec3 FragPos , 
					     vec3 diffuseColor , vec3 specularIntensity ){

	vec3 lightDirection = normalize( light.position - FragPos );
	float dist = length( light.position - FragPos );
	
	//Quadratic attenuation
	float attenuation = 1.0f / ( light.c + light.l * dist + light.q * dist * dist );
	//float attenuation = 1.0f / ( dist * dist ); 

	// smooth cone edges
	float thetaCos = dot( lightDirection , normalize( -light.direction ) );
	float epsilon = light.cutoff - light.outerCutoff;
	float intensity = clamp( ( thetaCos - light.outerCutoff ) / epsilon , 0.0f , 1.0f );
	
	mat3 lightComponents = calculateBPLightComponents( 
		light.core , 
		normal , 
		viewDirection , 
		lightDirection , 
		diffuseColor , 
		specularIntensity 
	);
	
	vec3 ambient = attenuation * lightComponents[0]; // ambient does not use intensity
	vec3 diffuse = intensity * attenuation * lightComponents[1];
	vec3 specular = intensity * attenuation * lightComponents[2];
	
	return ( ambient + diffuse + specular );
}