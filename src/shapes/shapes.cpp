#include <iostream>

#include <glm/glm.hpp>

#include "../Engine/linear-algebra.h"

#include "shapes.h"

const int cube::withTangentAndBitangentsSize = 36 * 14 * sizeof( float );

glm::vec3 cube::getVertex( int index ){
	return glm::vec3( vertexPositions[3 * index] ,
					  vertexPositions[3 * index+1] ,
					  vertexPositions[3 * index+2] );
}

void cube::buildVertexAndNormal( float *van , int index , glm::vec3 v , glm::vec3 n ){

	van[ (6*index) ] = v.x;
	van[ (6*index) +1 ] = v.y;
	van[ (6*index) +2 ] = v.z;

	van[ (6*index) +3 ] = n.x;
	van[ (6*index) +4 ] = n.y;
	van[ (6*index) +5 ] = n.z;

}

float* cube::vertexAndNormals(){
	float* van = (float *) malloc(  36 * ( 3 + 3 ) * sizeof( float ) );

	int i = 0;
	while( i < 12 ){
		glm::vec3 v1 = cube::getVertex( 3 * i );
		glm::vec3 v2 = cube::getVertex( 3 * i + 1);
		glm::vec3 v3 = cube::getVertex( 3 * i + 2);

		glm::vec3 n = glm::cross( v2 - v1 , v2 - v3 );

		buildVertexAndNormal( van , 3 * i , v1 , n );
		buildVertexAndNormal( van , 3 * i+1 , v2 , n );
		buildVertexAndNormal( van , 3 * i+2 , v3 , n );
		i++;
	}

	return van;
}

void cube::printVan( float *van ){
//		float *van = vertexAndNormals();

	std::cout << "{" << std::endl << "\t";
	for( int i = 0 ; i < 36 * 6 ; i++ ){
		std::cout << van[i] << ( i < (36 * 6 - 1) ? ", " : "" );
		if( i % 6 == 5 )
			std::cout << std::endl << "\t";

		if( i % ( 18 * 2 ) == 18 * 2 - 1 )
			std::cout << std::endl << "\t";
	}
	std::cout << "}" << std::endl;

//		delete van;
}

/**
 * Extends the cube structure calculating the tangent space for each vertex.
 * The cube must be specified as a 36-vertices cube ( 2 triangles/face )
 * with the following values order:
 *
 *    position.x, position.y, position.z, normal.x, normal.y, normal.z, textCoord.u, textCoord.v
 */
float* cube::withTangentsAndBitangents( const float *cube ){

	//const float *cube = cube::fullVertices; // pos , normal , texcoords
	int cubeRowSize = 8;
	int faceSize = 8 * 6;

	float *cubeTangent = (float *) malloc( cube::withTangentAndBitangentsSize );

    //	6 faces , 2 triangles each
	for( int j = 0 ; j < 6 ; ++j ){
		for( int k = 0 ; k < 2 ; ++k ){
			glm::vec3 p[3];
			glm::vec2 uv[3];
			for( int i = 0 ; i < 3 ; ++i ){
				int offset = j*faceSize + k*3*cubeRowSize + i*cubeRowSize;
				p[i] = glm::vec3( cube[offset] , cube[offset+1] , cube[offset+2] );
				uv[i] = glm::vec2( cube[offset+6] , cube[offset+7] );

//				std::cout << p[i].x << " " << p[i].y << " " << p[i].z << "\t"
//						  << uv[i].x << " " << uv[i].y << std::endl;

			}

			// Calculates the tangent and the bitangent for the vertex
			glm::mat2x3 TB = engLinAlg::calculateTangentAndBitangent(
					p[0] , p[1] , p[2] ,
					uv[0] , uv[1] , uv[2]
			);

			for( int i = 0 ; i < 3 ; ++i ){
				int offset = j*faceSize + k*3*cubeRowSize + i*cubeRowSize;
				int newOffset = j*6*14 + k*3*14 + i*14;

				for( int l = 0 ; l < 8 ; ++l ){
					cubeTangent[newOffset+l] = cube[offset+l];
				}
				cubeTangent[newOffset+8] = TB[0].x;
				cubeTangent[newOffset+9] = TB[0].y;
				cubeTangent[newOffset+10] = TB[0].z;

				cubeTangent[newOffset+11] = TB[1].x;
				cubeTangent[newOffset+12] = TB[1].y;
				cubeTangent[newOffset+13] = TB[1].z;

				//Debug
//				for( int l = 0 ; l < 14 ; ++l ){
//					std::cout << cubeTangent[newOffset + l] << " ";
//				}
//				std::cout << std::endl;
			}
		}
//		std::cout << std::endl;
	}

	return cubeTangent;
}

float* line::to( glm::vec3 point ){
	float *vertices = new float[6];

	vertices[0] = vertices[1] = vertices[2] = 0.0f;
	vertices[3] = point.x;
	vertices[4] = point.y;
	vertices[5] = point.z;

	return vertices;
}

float* line::to( float x , float y , float z ){
	return to( glm::vec3( x , y , z ) );
}

