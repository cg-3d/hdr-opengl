#include <iostream>
#include <experimental/filesystem>
#include <stdlib.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/constants.hpp>

#include "../Engine/Shader.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"

#include "../Engine/Scene.h"

#include "../Engine/input-callbacks.h"
#include "../Engine/cubemaps.h"
#include "../Engine/engine-utils.h"

#include "../shapes/shapes.h"
#include "../utilities/utilities.h"

#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_glfw.h"
#include "../imgui/imgui_impl_opengl3.h"

void scene1( GLFWwindow *window ){
	//--------------------------------------------------
		// Buffers
	//	float *cubeVan = cube::vertexAndNormals();
	//	cube::printVan( cubeVan );

	unsigned int skyboxVBO;
	glGenBuffers( 1 , &skyboxVBO );
	glBindBuffer( GL_ARRAY_BUFFER , skyboxVBO );
	glBufferData( GL_ARRAY_BUFFER , sizeof( cubemaps::skyboxVertices ) , cubemaps::skyboxVertices , GL_STATIC_DRAW  );

	unsigned int skyboxVAO;
	glGenVertexArrays( 1 , &skyboxVAO );
	glBindVertexArray( skyboxVAO );
	glBindBuffer( GL_ARRAY_BUFFER , skyboxVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 3 * sizeof( float ) , (void *)0 );
	glEnableVertexAttribArray( 0 );

	unsigned int cubeVBO;
	glGenBuffers( 1 , &cubeVBO );
	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
//	glBufferData( GL_ARRAY_BUFFER , sizeof( cube::fullVertices ) , cube::fullVertices , GL_STATIC_DRAW );
	glBufferData( GL_ARRAY_BUFFER , sizeof( cube::van ) , cube::van , GL_STATIC_DRAW );
//	glBufferData( GL_ARRAY_BUFFER , 36 * 6 * sizeof( float ) , cubeVan , GL_STATIC_DRAW );

	unsigned int cubeVAO;
	glGenVertexArrays( 1 , &cubeVAO );
	glBindVertexArray( cubeVAO );
	glBindBuffer( GL_ARRAY_BUFFER , cubeVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 6 * sizeof( float ) , (void *)0 );
	glEnableVertexAttribArray( 0 );

	glVertexAttribPointer( 1 , 3 , GL_FLOAT , GL_FALSE , 6 * sizeof( float ) , (void *)( 3 * sizeof( float ) ) );
	glEnableVertexAttribArray( 1 );

	float *line = line::to( -1.0f , 0.0f , 0.0f  );
	unsigned int lineVBO;
	glGenBuffers( 1 , &lineVBO );
	glBindBuffer( GL_ARRAY_BUFFER , lineVBO );
	glBufferData( GL_ARRAY_BUFFER , 6 * sizeof( float ) , line , GL_STATIC_DRAW  );

	unsigned int lineVAO;
	glGenVertexArrays( 1 , &lineVAO );
	glBindVertexArray( lineVAO );
	glBindBuffer( GL_ARRAY_BUFFER , lineVBO );
	glVertexAttribPointer( 0 , 3 , GL_FLOAT , GL_FALSE , 3 * sizeof( float ) , (void *)0 );
	glEnableVertexAttribArray( 0 );

	unsigned int framebuffer;
	unsigned int textureColorbuffer;
	unsigned int renderbuffer;
	engUtils::createFrameTextureRenderBuffer( &framebuffer , &textureColorbuffer , &renderbuffer ,
											   (int)( incalls::screenWidth * 3 / 4.0f ) ,
											   incalls::screenHeight );
	// Bind buffers to recreate in case of window resizing
	incalls::bindFrameTextureRenderBuffers( &framebuffer , &textureColorbuffer , &renderbuffer );


	glm::mat4 model = glm::mat4( 1.0f );

	// View matrix
//	glm::mat4 view = glm::mat4( 1.0f );
//	view = glm::translate( view , glm::vec3( 0.0f , 0.0f , -5.0f ) );
	Camera cam = Camera( glm::vec3( 3.0f , 2.0f , 0.0f ) ,
						 glm::vec3( 0.0f , 0.0f , 0.0f ) ,
						 glm::vec3( 0.0f , 1.0f , 0.0f ) );
	cam.setTargetLockedOnMove( false );
	incalls::bindCamera( &cam );
	//glfwSetCursorPosCallback( window , incalls::mouse_callback );
	//glfwSetMouseButtonCallback( window , incalls::mouse_button_callback );


	//Shaders definitions
	//
	// Light placeholders shader
	Shader lampShaderProgram( utilities::getPath( "src/shaders/lamp_vertex.vs" ).c_str() ,
							  utilities::getPath( "src/shaders/lamp_fragment.fs" ).c_str() );
	//Shader modelShaderProgram( "src/shaders/model.vs" , "src/shaders/model.fs" );

	// Lights shader
	//Shader castersShaderProgram( "src/shaders/light_casters/cast_vertex.vs" , "src/shaders/light_casters/cast_fragment.fs" );
	Shader multipleLightsShaderProgram( utilities::getPath( "src/shaders/multiple_lights/mul_vertex.vs" ).c_str() ,
										utilities::getPath( "src/shaders/multiple_lights/mul_fragment.fs" ).c_str() );

	//Skybox shader
	Shader skyboxShaderProgram( utilities::getPath( "src/shaders/cubemaps/cubemap.vs" ).c_str() ,
								utilities::getPath( "src/shaders/cubemaps/cubemap.fs" ).c_str() );

	//Reflection/refraction shader
	Shader reflectionShaderProgram( utilities::getPath( "src/shaders/env_mapping/env_mapping.vs" ).c_str() ,
									utilities::getPath( "src/shaders/env_mapping/reflect.fs" ).c_str() );

	// Projection matrix
	glm::mat4 projection;
	projection = glm::perspective( glm::radians( 45.0f ) ,
								   incalls::sceneViewWidth / (float) incalls::sceneViewHeight ,
								   0.1f , 500.0f );
	incalls::bindProjectionMatrix( &projection );

//	lampShaderProgram.use();
	lampShaderProgram.setMatrix4fv( "projection" , projection );
	skyboxShaderProgram.setMatrix4fv( "projection" , projection );
	reflectionShaderProgram.setMatrix4fv( "projection" , projection );
//	normalShaderProgram.setMatrix4fv( "projection" , projection );

	// Depth testing
	glEnable( GL_DEPTH_TEST );

	incalls::setCameraSpeed( 3.0f );
	incalls::setCameraOrbitSpeed( 40.0f );

	glm::vec3 clearColor = glm::vec3( 0.098f, 0.114f, 0.122f );

	// load model
	Model wraithModel( utilities::getPath( "models/wraith/wraith.obj" ).c_str() );
	wraithModel.printModel();

//	modelShaderProgram.setVec3( "light.position" , lightPos );
//	modelShaderProgram.setVec3( "light.ambient" , glm::vec3( 0.4f ) );
//	modelShaderProgram.setVec3( "light.diffuse" , glm::vec3( 0.7f ) );
//	modelShaderProgram.setVec3( "light.specular" , glm::vec3( 1.0f ) );

	//Lights settings
	//
	glm::vec3 dirLightDir = glm::normalize( glm::vec3( 0.0f , 0.0f , 1.0f ) );

	glm::vec3 spotLightPos = glm::vec3( 0.0f , 4.0f , -1.0f );
	glm::vec3 spotLightDir = glm::normalize( glm::vec3( 0.0f , -1.0f , 1.0f ) );

	glm::vec3 pointLightPos = glm::vec3( 0.0f , 2.0f , 3.0f );

	multipleLightsShaderProgram.setFloat( "shininess" , 64.0f );
	glm::vec3 dirLightColor = glm::vec3( 0.0f , 0.4f , 0.6f );
	multipleLightsShaderProgram.setVec3( "dirLight.core.ambient" , 0.1f * dirLightColor );
	multipleLightsShaderProgram.setVec3( "dirLight.core.diffuse" , 0.5f * dirLightColor );
	multipleLightsShaderProgram.setVec3( "dirLight.core.specular" , 1.0f * dirLightColor );
	multipleLightsShaderProgram.setVec3( "dirLight.direction" , dirLightDir );

	glm::vec3 pointLightColor = glm::vec3( 1.0f , 0.0f , 0.0f );
	multipleLightsShaderProgram.setVec3( "pointLight.core.ambient" , 0.1f * pointLightColor );
	multipleLightsShaderProgram.setVec3( "pointLight.core.diffuse" , 0.5f * pointLightColor );
	multipleLightsShaderProgram.setVec3( "pointLight.core.specular" , 1.0f * pointLightColor );
	multipleLightsShaderProgram.setVec3( "pointLight.position" , pointLightPos );
	multipleLightsShaderProgram.setFloat( "pointLight.c" , 1.0f );
	multipleLightsShaderProgram.setFloat( "pointLight.l" , 0.14f );
	multipleLightsShaderProgram.setFloat( "pointLight.q" , 0.007f );

	multipleLightsShaderProgram.setVec3( "spotLight.core.ambient" , glm::vec3( 0.1f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.core.diffuse" , glm::vec3( 0.5f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.core.specular" , glm::vec3( 1.0f ) );
	multipleLightsShaderProgram.setVec3( "spotLight.position" , spotLightPos );
	multipleLightsShaderProgram.setVec3( "spotLight.direction" , spotLightDir );
	multipleLightsShaderProgram.setFloat( "spotLight.cutoff" , glm::cos( glm::radians( 12.5f ) ) );
	multipleLightsShaderProgram.setFloat( "spotLight.outerCutoff" , glm::cos( glm::radians( 25.0f ) ) );
	multipleLightsShaderProgram.setFloat( "spotLight.c" , 1.0f );
	multipleLightsShaderProgram.setFloat( "spotLight.l" , 0.14f );
	multipleLightsShaderProgram.setFloat( "spotLight.q" , 0.007f );

	// Common to all light types
//	castersShaderProgram.setVec3( "light.ambient" , glm::vec3( 0.1f ) );
//	castersShaderProgram.setVec3( "light.diffuse" , glm::vec3( 0.5f ) );
//	castersShaderProgram.setVec3( "light.specular" , glm::vec3( 1.0f ) );

	// Directional light only
	//lightsShaderProgram.setVec3( "light.direction" , lightDir );

	// Point and Spot lights only
//	castersShaderProgram.setVec3( "light.position" , lightPos );
//	castersShaderProgram.setVec3( "light.direction" , lightDir );
//	castersShaderProgram.setFloat( "light.c" , 1.0f );
//	castersShaderProgram.setFloat( "light.l" , 0.14f );
//	castersShaderProgram.setFloat( "light.q" , 0.007f );
//
//	castersShaderProgram.setFloat( "light.cutoff" , glm::cos( glm::radians( 12.5f ) ) );
//	castersShaderProgram.setFloat( "light.outerCutoff" , glm::cos( glm::radians( 25.0f ) ) );

	//Skybox shader settings
	std::vector<std::string> skyboxFaces = {
		utilities::getPath( "skybox/default/right.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/left.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/top.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/bottom.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/front.jpg" ).c_str() ,
		utilities::getPath( "skybox/default/back.jpg" ).c_str()
	};

	unsigned int skyboxTexture = cubemaps::loadCubemap( skyboxFaces );
	skyboxShaderProgram.setInt( "cubemap" , 0 );
	reflectionShaderProgram.setInt( "skybox" , 0 );

	//bool sceneViewActive = false;

	while( !glfwWindowShouldClose( window ) ){ //Check if GLFW has been instructed to close

		//Update the shaders projection matrices
		// This should be made in the framebuffer size callback to optimize
		lampShaderProgram.setMatrix4fv( "projection" , projection );
		skyboxShaderProgram.setMatrix4fv( "projection" , projection );
		reflectionShaderProgram.setMatrix4fv( "projection" , projection );

		// Start the Dear ImGui frame
		ImGui_ImplOpenGL3_NewFrame();
		ImGui_ImplGlfw_NewFrame();
		ImGui::NewFrame();

		// Process key inputs
		//incalls::process_prolonged_inputs( window );

		// bind to framebuffer and draw scene
		// -------------------------------------------------------------
		glBindFramebuffer(GL_FRAMEBUFFER, framebuffer);
		glEnable( GL_DEPTH_TEST );

		glClearColor( clearColor.x * 0.5,
					  clearColor.y * 0.5,
					  clearColor.z * 0.5,
					  1.0f ); //Set clear color
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );	//Clear color and depth buffer

		//REFLECION TEST
		//
		//
		model = glm::mat4( 1.0f );
		model = glm::translate( model , glm::vec3( 3.0f , 0.0f , 0.0f ) );

		reflectionShaderProgram.setMatrix4fv( "model" , model );
		reflectionShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
		reflectionShaderProgram.setMatrix3fv( "normalMatrix" ,
											  glm::mat3( glm::transpose( glm::inverse( model ) ) ) );
		reflectionShaderProgram.setVec3( "cameraPos" , cam.getPosition() );
		reflectionShaderProgram.use();
		glBindVertexArray( cubeVAO );
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_CUBE_MAP , skyboxTexture );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );


		//MODEL
		//
//		modelShaderProgram.setMatrix4fv( "projection" , projection );
//		modelShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
//		castersShaderProgram.setMatrix4fv( "projection" , projection );
//		castersShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
		multipleLightsShaderProgram.setMatrix4fv( "projection" , projection );
		multipleLightsShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );


//		modelShaderProgram.use();
//		castersShaderProgram.use();
		multipleLightsShaderProgram.use();

		//model = glm::scale( glm::mat4( 1.0f ) , glm::vec3( 0.5f , 0.5f , 0.5f ) );
		model = glm::translate( glm::mat4( 1.0f ) , glm::vec3( 0.0f , 0.0f , 0.0f ) );
		//model = glm::rotate( glm::mat4( 1.0f ) , glm::radians( -90.0f ) , glm::vec3( 0.0f , 1.0f , 0.0f ) );
//		modelShaderProgram.setMatrix4fv( "model" , model );
//		modelShaderProgram.setMatrix3fv( "normalMatrix" ,
//										 glm::mat3( glm::transpose( glm::inverse( model ) ) ) );
//		castersShaderProgram.setMatrix4fv( "model" , model );
//		castersShaderProgram.setMatrix3fv( "normalMatrix" ,
//										 glm::mat3( glm::transpose( glm::inverse( model ) ) ) );
		multipleLightsShaderProgram.setMatrix4fv( "model" , model );
		multipleLightsShaderProgram.setMatrix3fv( "normalMatrix" ,
												 glm::mat3( glm::transpose( glm::inverse( model ) ) ) );

//		modelShaderProgram.setVec3( "ViewPos" , cam.getPosition() );
		multipleLightsShaderProgram.setVec3( "ViewPos" , cam.getPosition() );

		//nanosuitModel.draw( modelShaderProgram );
//		nanosuitModel.draw( castersShaderProgram );
		wraithModel.draw( multipleLightsShaderProgram );

//		model = glm::translate( glm::mat4( 1.0f ) , glm::vec3( 0.0f , 0.0f , 3.0f ) );
//		lightsShaderProgram.setMatrix4fv( "model" , model );
//		nanosuitModel.draw( lightsShaderProgram );

		//LAMPs
		//
		//Spot light
		model = glm::translate( glm::mat4( 1.0f ) , spotLightPos );
		model = glm::scale( model , glm::vec3( 0.2f ) );

		lampShaderProgram.use();
		lampShaderProgram.setMatrix4fv( "model" , model );
		lampShaderProgram.setMatrix4fv( "view" , cam.getViewMatrix() );
		lampShaderProgram.setVec3( "color" , glm::vec3( 1.0f ) );
		glBindVertexArray( cubeVAO );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );

		//Point light
		model = glm::translate( glm::mat4( 1.0f ) , pointLightPos );
		model = glm::scale( model , glm::vec3( 0.1f ) );

		lampShaderProgram.use();
		lampShaderProgram.setMatrix4fv( "model" , model );
		lampShaderProgram.setVec3( "color" , pointLightColor );
		glBindVertexArray( cubeVAO );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );

		//Directional light
//		model = glm::mat4( 1.0f );
//		model = glm::translate( model , glm::vec3( 0.0f , 3.0f , 3.0f ) );
//		model = glm::rotate( model ,
//							 -glm::acos( glm::dot( dirLightDir , glm::vec3( -1.0f , 0.0f , 0.0f ) ) ),
//							 glm::cross( dirLightDir , glm::vec3( -1.0f , 0.0f , 0.0f  ) ) );
//		model = glm::scale( model , glm::vec3( 3.0f ) );

		for( int i = 0 ; i < 3 ; i++ ){
			model = glm::mat4( 1.0f )
				  * glm::translate( glm::mat4( 1.0f ) , 7.0f * -dirLightDir + 0.05f * i * glm::vec3( 0.0f , 1.0f , 0.0f ) )
				  * glm::rotate( glm::mat4( 1.0f ) ,
								-glm::acos( glm::dot( dirLightDir , glm::vec3( -1.0f , 0.0f , 0.0f ) ) ),
								 glm::cross( dirLightDir , glm::vec3( -1.0f , 0.0f , 0.0f  ) ) )
				  * glm::scale( glm::mat4( 1.0f ) , glm::vec3( 3.0f ) );
			lampShaderProgram.setMatrix4fv( "model" , model );
			lampShaderProgram.setVec3( "color" , dirLightColor );
			glBindVertexArray( lineVAO );
			glDrawArrays( GL_LINES , 0 , 2 );
		}

		// SKYBOX: drawn as last, optimization purposes
		//
		//model = glm::mat4( 1.0f );
		glDepthMask( GL_FALSE );
		skyboxShaderProgram.use();
		// Removes translation components from view matrix for the skybox

		glDepthFunc( GL_LEQUAL );
		skyboxShaderProgram.setMatrix4fv( "view" , glm::mat4( glm::mat3( cam.getViewMatrix() ) ) );
		glBindVertexArray( skyboxVAO );
		glBindTexture( GL_TEXTURE_CUBE_MAP , skyboxTexture );
		glDrawArrays( GL_TRIANGLES , 0 , 36 );
		glDepthMask( GL_TRUE );

		// Reset
		glBindVertexArray(0);
		glDepthFunc( GL_LESS );

		// IMGUI settings
		ImGui::SetNextWindowPos( ImVec2( incalls::screenWidth / 4.0 , 0 ) );
		ImGui::SetNextWindowSize(ImVec2( incalls::sceneViewWidth ,
										 incalls::sceneViewHeight ) );
		//ImGui::SetNextWindowSizeConstraints(ImVec2(600,600), ImVec2(600,600));
		ImGui::Begin("Scene Window");
		ImVec2 pos = ImGui::GetCursorScreenPos();

		// Inputs for the scene view
		glfwPollEvents(); // Calling pollEvents() sets every callback
						  // in the context of the "Scene Window"

//		if( ImGui::IsWindowFocused() ){ // Process prolonged inputs only if scene view is focused
//			incalls::process_prolonged_inputs( window );
//			sceneViewActive = true;
//		}else{
//			sceneViewActive = false;
//		}

		if( incalls::sceneViewActive )
			incalls::process_prolonged_inputs( window );
		//--------------------------------------------------------

		ImGui::GetWindowDrawList()->AddImage(
			(ImTextureID)framebuffer,
			pos,
			ImVec2( pos.x + incalls::sceneViewWidth ,
				    pos.y + incalls::sceneViewHeight ),
		    ImVec2(0, 1), ImVec2(1, 0)
		);
		ImGui::End();

		// now bind back to default framebuffer
		glBindFramebuffer( GL_FRAMEBUFFER , 0 );

		ImGui::SetNextWindowSize(ImVec2( incalls::screenWidth - incalls::sceneViewWidth ,
										 incalls::screenHeight ) );

		//ImGui::ShowDemoWindow();
		ImGuiWindowFlags controlsWindowFlags = ImGuiWindowFlags_NoMove; // Make the controls window not draggable
		if( incalls::sceneViewActive )
			controlsWindowFlags |= ImGuiWindowFlags_NoInputs; //Disable inputs on the controls window if the scene view is active

		ImGui::Begin( "Controls Window" , NULL , controlsWindowFlags );

		ImGui::Text( "Controls" );
		// TODO : define controls

		ImGui::End();

		//Rendering
		ImGui::Render();
//		int display_w , display_h;
//		glfwMakeContextCurrent( window );
//		glfwGetFramebufferSize( window , &display_w , &display_h );
		//glClearColor( 0.0f , 0.0f , 1.0f , 1.0f );
		glClear(GL_COLOR_BUFFER_BIT);
		ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

		/** DOUBLE BUFFERING **/
		glfwSwapBuffers( window ); // Swap the color buffer (buffer containing the color for each pixel)
								   // used to draw in this iteration (back buffer) with the rendered one (front buffer)
		//glfwPollEvents(); //Check if events are triggered (keyboard input, mouse input, ...)
	}

//	glDeleteVertexArrays( 1 , &VAO );
	glDeleteVertexArrays( 1 , &cubeVAO );
	glDeleteVertexArrays( 1 , &skyboxVAO );
	glDeleteVertexArrays( 1 , &lineVAO );
//	glDeleteVertexArrays( 1 , &normalVAO );
	glDeleteBuffers( 1 , &cubeVBO );
	glDeleteBuffers( 1 , &lineVBO );
	glDeleteBuffers( 1 , &skyboxVBO );
	delete line;

	glDeleteFramebuffers( 1 , &framebuffer );
	glDeleteTextures( 1 , &textureColorbuffer );
	glDeleteRenderbuffers( 1 , &renderbuffer );

	glfwTerminate();

}
